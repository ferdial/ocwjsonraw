# Purpose

The purpose of this repo is to track the development of exporting courses in json using collective.jsonify

# How to use this repo

This repository contains to main directories:

- Jsonfiy source code
- The export result for 21H.343J_spring_2016 course

The ideal workflow would be:

- Commit the original version of jsonify 
- Run the code against 21H.343J_spring_2016 and submit the original output.

Then for every change in jsonify
- Somoeone from OCW or Sapient opens an issue and explains what needs to be achieved
- Feel free to go back and fourth and understand the issue
- Make a commit with related code changes that is relevant to that issue
- Run the export for 21H.343J_spring_2016 and commit the output changes in this repo.
