{
    "text": "\n\n<h2 class=\"subhead\">Course Meeting Times</h2>\n<p>Lectures: 2 sessions / week, 1.5 hours / session</p>\n<p>Sight-singing Laboratory: 1 session / week, 1 hour / session</p>\n<p>Piano Lab: 1 session / week, 1 hour / session</p>\n<h2 class=\"subhead\">Description</h2>\n<p>In this subject we will study the basic harmonic, melodic, and formal practices of western music, principally the classical music of central Europe during the eighteenth century. Topics will include diatonic harmony, simple counterpoint in two parts, and tones of figuration. The coursework will combine composition, listening, analysis, and work in sight-singing and keyboard musicianship.</p>\n<h2 class=\"subhead\">Required Texts for Lecture</h2>\n<p><a href=\"http://www.amazon.com/exec/obidos/ASIN/0155062425/ref=nosim/mitopencourse-20\"><img alt=\"Buy at Amazon\" src=\"/images/a_logo_17.gif\" align=\"absMiddle\" border=\"0\" /></a> Aldwell, Edward, and Carl Schachter. <em>Harmony and Voice Leading</em>. 3rd ed. Orlando, FL: Harcourt Brace, 2002. ISBN: 9780155062429.</p>\n<p>Music manuscript paper: Please obtain a plentiful supply of music manuscript paper. (See the \"Music manuscript paper\" list of requirements for appropriate manuscript paper, located in the <a href=\"/courses/music-and-theater-arts/21m-301-harmony-and-counterpoint-i-spring-2005/study-materials\">study materials</a> section.)</p>\n<h2 class=\"subhead\">Required* Sight-singing Lab</h2>\n<p>Vocal models underlie virtually all the contrapuntal bases of our classical music (yes, even in purely instrumental idioms); hence, practice in singing classical music is one of the most efficient ways for you to internalize the classical norms of voice-leading. The lab meets for an hour at the end of each week, beginning after lecture 4. Bring the required text: Various Composers. <em>Five Centuries of Choral Music for Mixed Voices.</em> Vol. 1. New York, NY: G. Schirmer, 1963.</p>\n<p>*Current members of the MIT Concert Choir or MIT Chamber Chorus are exempt from this requirement.</p>\n<h2 class=\"subhead\">Required Piano Lab</h2>\n<p>Even in our modern age of computer-assisted sound manipulation, the traditional musical keyboard still provides the most efficient way for an individual to perform or improvise contrapuntal polyphony in real time, and thus receive instantaneous aural feedback.</p>\n<p>Piano lab consists of a weekly session with a private piano instructor, doing keyboard exercises tied to this class but based on your individual skills. You're requested to work in the lab for one hour each week, during which the instructor will work with you for 15 minutes. Once you've negotiated a piano lab time, please be sure to attend regularly. If you won't be able to attend a session, please notify the instructor as far in advance as possible.</p>\n<h2 class=\"subhead\">Daily Assignments</h2>\n<p>Expect to complete one assignment for each class meeting. Each late submission is penalized one letter grade for each calendar day it is late; no assignment will be accepted more than three days late. If you must miss a class meeting, leave your work at my office door by 9:00 a.m. the day it is due, or have a friend bring it to class.</p>\n<h2 class=\"subhead\">Quizzes</h2>\n<p>During nearly every week, I will give a timed quiz (typically of five minutes' duration). These quizzes will measure your mastery of the most basic vocabulary (scales, intervals, triads) as well as some of the more important syntactic concepts of harmony and voice-leading.</p>\n<h2 class=\"subhead\">Midterm Exam</h2>\n<p>The midterm exam will cover material from Aldwell and Schachter (chapters 1-10 only), as well as material distributed in class.</p>\n<h2 class=\"subhead\">Final Project</h2>\n<p>Toward the end of the semester, you will compose a melody that you will harmonize in classical style, creating a four-voice texture that adheres strictly to the principles of eighteenth-century harmony and voice-leading.</p>\n<p>For the most part, the grading of homework assignments and the final harmonization project is strictly objective. At my discretion, I may occasionally award a bonus of two to five points for compositional artistry in the final project and in some of the homework assignments.</p>\n<h2 class=\"subhead\">Subject Requirements and Grade Weights</h2>\n<div class=\"maintabletemplate\">\n<table class=\"tablewidth50\" summary=\"See table caption for summary.\">\n<caption class=\"invisible\">Grading</caption>\n<thead>\n<tr>\n<th scope=\"col\">ACTIVITIES</th>\n<th scope=\"col\">PERCENTAGES</th>\n</tr>\n</thead>\n<tbody>\n<tr class=\"row\">\n<td>Sight-singing Lab (incl. Handel Bass)</td>\n<td>10%</td>\n</tr>\n<tr class=\"alt-row\">\n<td>Piano Lab (incl. Bach Reduction)</td>\n<td>10%</td>\n</tr>\n<tr class=\"row\">\n<td>Daily Preparation and Participation</td>\n<td>10%</td>\n</tr>\n<tr class=\"alt-row\">\n<td>Daily Assignments</td>\n<td>20%</td>\n</tr>\n<tr class=\"row\">\n<td>Weekly Quizzes</td>\n<td>15%</td>\n</tr>\n<tr class=\"alt-row\">\n<td>Midterm Exam</td>\n<td>15%</td>\n</tr>\n<tr class=\"row\">\n<td>Final Project</td>\n<td>20%</td>\n</tr>\n</tbody>\n</table>\n</div>\n<br />\n<p>A failure to complete any one of these components will result in failure as your semester grade. That is, to pass this subject, you must attend the sight-singing and piano labs, and satisfactorily perform the Bach reduction and the Handel bass line for me. The labs are graded by their respective instructors; I will grade all other components.</p>", 
    "_content_type_license": "text/plain", 
    "_uid": "b667bb36dff4cfd1ff9054961488e2ac", 
    "title": "Syllabus", 
    "_ac_local_roles": {
        "admin": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {}, 
    "constrainTypesMode": 0, 
    "technical_location": "https://ocw.mit.edu/courses/music-and-theater-arts/21m-301-harmony-and-counterpoint-i-spring-2005/syllabus", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "4a16a746d8d94c67b1cd15a83a556ab8", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2010/05/14 13:04:28.864 GMT-4", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "None", 
    "parent_module": "4a16a746d8d94c67b1cd15a83a556ab8", 
    "is_mathml_document": false, 
    "bottomtext": "", 
    "short_page_title": "Syllabus", 
    "section_identifier": null, 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "admin", 
                "time": "2010/05/14 13:04:28.870 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "admin", 
                "comments": "", 
                "time": "2010/05/16 11:45:08.392 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "Syllabus", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [
        {}
    ], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "4a16a746d8d94c67b1cd15a83a556ab8"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2010.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": null, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/music-and-theater-arts/21m-301-harmony-and-counterpoint-i-spring-2005/syllabus", 
    "is_image_gallery": false, 
    "id": "syllabus", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "Syllabus section contains the course description, and information about required texts for lecture, required sight-singing lab, required piano lab, daily assignments, quizzes, midterm exam, final project, and subject requirements and grade weights.", 
    "course_identifier": "https://ocw.mit.edu/courses/music-and-theater-arts/21m-301-harmony-and-counterpoint-i-spring-2005", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "admin"
    ], 
    "modification_date": "2016/09/24 08:09:44.040 GMT-4", 
    "always_publish": false, 
    "_content_type_description": "text/plain", 
    "_owner": "admin", 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "Syllabus", 
            "string"
        ]
    ], 
    "_id": "syllabus", 
    "courselist_features": [], 
    "_content_type_short_name_tlp": "text/plain", 
    "_path": "/Plone/courses/music-and-theater-arts/21m-301-harmony-and-counterpoint-i-spring-2005/syllabus"
}