{
    "text": "<div class=\"navigation pagination\">\n<ul>\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/6267326555dff200dae2644697a179db\">&lt;<span>More on Finite Element Methods</span></a></li>\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/6267326555dff200dae2644697a179db\">2.10.1<span>Gaussian Quadrature</span></a></li>\n<li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/365c70a74666ed1cd1408aeb96bff4a6\">2.10.2<span>Boundary Conditions for Finite Elements</span></a></li>\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/c782bcc9abb3f6bcc638027dfffdc386\">&gt;<span>The Finite Element Method for Two-Dimensional Diffusion</span></a></li>\n</ul>\n</div>\n<!--?xml version='1.0' encoding='utf-8'?-->\n<h2 class=\"subhead\">2.10.2 Boundary Conditions for Finite Elements</h2>\n<p id=\"taglist\"><a id=\"bcLaplace\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO219\" title=\"MO2.19:  Explain how Dirichlet and Neumann boundary conditions are implemented for Laplace's equation, discretized by the finite element method. \">Measurable Outcome 2.19</a>, <a id=\"sysStiff\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO220\" title=\"MO2.20:  Describe how the finite element method discretization results in a system of discrete equations and, for linear problems, gives rise to the stiffness matrix. Describe the meaning of the entries (rows and columns) of the stiffness matrix and of the right-hand side vector for linear problems. \">Measurable Outcome 2.20</a></p>\n<p>Boundary conditions generally fall into one of three types:</p>\n<ul class=\"itemize\">\n<li>\n<p>Set \\(\\tilde{T}\\) at the boundary (known as a Dirichlet boundary condition). For heat transfer problems, this type of boundary condition occurs when the temperature is known at some portion of the boundary.</p>\n</li>\n<li>\n<p>Set \\(\\tilde{T}_{x}\\) at the boundary (known as a Neumann boundary condition). For heat transfer problems, this type of boundary condition occurs when the heat transfer rate is known at the boundary. For example, an adiabatic boundary would require that \\(\\tilde{T}_{x}=0\\).</p>\n</li>\n<li>\n<p>Set \\(\\alpha _0\\tilde{T} + \\alpha _1 \\tilde{T}_{x}\\) at the boundary (known as a Robin boundary condition) where \\(\\alpha _0\\) and \\(\\alpha _1\\) do not depend on the temperature. For heat transfer problems, this type of boundary condition occurs when modeling convection (we will see this in the convective boundary condition example below).</p>\n</li>\n</ul>\n<h2 class=\"subhead\">Convection Boundary Condition for Heat Transfer</h2>\n<p>Consider the flow of air over a solid object with the velocity and temperature of the external air being \\(U_{ext}\\) and \\(T_{ext}\\), respectively. Aligning the \\(x\\)-direction into the surface of the object, then the heat transfer rate at the surface is</p>\n<table id=\"equ:qwall\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[q_{wall} = -k T_ x, \\label{equ:qwall}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.246)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(T(x)\\) is the temperature inside the solid and \\(k\\) is the thermal conductivity of the solid. A common approach to modeling the heat transfer into the solid as a result of the airflow is based on specifying a heat transfer coefficient for the airflow, \\(h_{ext}\\), which is related to the heat transfer rate by</p>\n<table id=\"equ:hdef\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[h_{ext} \\equiv \\frac{q_{wall}}{T_{ext}-T_{wall}}. \\label{equ:hdef}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.247)</td>\n</tr>\n</tbody>\n</table>\n<p>Note that \\(h_{ext}\\) is generally a function of the external velocity and other flow properties. Combining Equations&nbsp;(<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$q\\_\\{wall\\}\\ \\=\\ \\-k\\ T\\_\\ x\\,\\ \\\\label\\{equ\\:qwall\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.246)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.246)');\">2.246</a>) and (<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$h\\_\\{ext\\}\\ \\\\equiv\\ \\\\frac\\{q\\_\\{wall\\}\\}\\{T\\_\\{ext\\}\\-T\\_\\{wall\\}\\}\\.\\ \\\\label\\{equ\\:hdef\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.247)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.247)');\">2.247</a>) gives the following boundary condition at the surface of the solid,</p>\n<table id=\"equ:convectbc\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[-k T_ x = h_{ext}\\left(T_{ext}-T_{wall}\\right), \\label{equ:convectbc}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.248)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(T_{wall}\\) is the temperature of the solid at its surface. This equation can be re-arranged into the Robin boundary condition form,</p>\n<table id=\"a0000000384\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[-k T_ x + h_{ext} T_{wall} = h_{ext}T_{ext}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.249)</td>\n</tr>\n</tbody>\n</table>\n<h2 class=\"subhead\">Implementation of Dirichlet Boundary Conditions</h2>\n<p>To demonstrate the implementation of a Dirichlet boundary condition, suppose that the value of the temperature is known at \\(x=-L/2\\), specifically,</p>\n<table id=\"a0000000385\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[T(-L/2) = T_{left}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.250)</td>\n</tr>\n</tbody>\n</table>\n<p>This condition is set by forcing the corresponding nodal degree of freedom to be the desired value. At \\(x=-L/2\\), the corresponding nodal degree of freedom would be \\(a_1\\) (the value of the temperature at the first node), thus, the boundary condition is implemented as</p>\n<table id=\"a0000000386\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[a_1 = T_{left}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.251)</td>\n</tr>\n</tbody>\n</table>\n<h2 class=\"subhead\">Implementation of Neumann Boundary Conditions</h2>\n<p>To demonstrate the implementation of a Neumann boundary condition, suppose that the heat transfer rate is known at \\(x=L/2\\), specifically,</p>\n<table id=\"a0000000387\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[-k T_ x(L/2) = q_{right}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.252)</td>\n</tr>\n</tbody>\n</table>\n<p>This condition is enforced through the weighted residual for the last node, \\(i=N+1\\). Specifically,</p>\n<table id=\"a0000000388\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_{N+1} = \\left[\\phi _{N+1}\\,  k \\tilde{T}_ x\\right]^{L/2}_{-L/2} - \\int _{-L/2}^{L/2} {\\phi _{N+1}}_ x\\,  k \\tilde{T}_ x\\, dx + \\int _{-L/2}^{L/2} \\phi _{N+1} f\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.253)</td>\n</tr>\n</tbody>\n</table>\n<p>Because \\(\\phi _{N+1}(x)\\) is zero except in the last element, this weighted residual reduces to</p>\n<table id=\"a0000000389\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_{N+1} = \\left.\\left(\\phi _{N+1} k \\tilde{T}_ x\\right)\\right|_{x=x_{N+1}} - \\int _{x_{N}}^{x_{N+1}} {\\phi _{N+1}}_ x\\,  k \\tilde{T}_ x\\, dx + \\int _{x_{N}}^{x_{N+1}} \\phi _{N+1} f\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.254)</td>\n</tr>\n</tbody>\n</table>\n<p>The two integral terms are calculated in the standard manner. The first term is where the Neumann boundary condition is set through substitution of \\(-k \\tilde{T}_ x = q_{right}\\). Specifically, the weighted residual becomes</p>\n<table id=\"a0000000390\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_{N+1} = -\\phi _{N+1}(x_{N+1})q_{right} - \\int _{x_{N}}^{x_{N+1}} {\\phi _{N+1}}_ x\\,  k \\tilde{T}_ x\\, dx + \\int _{x_{N}}^{x_{N+1}} \\phi _{N+1} f\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.255)</td>\n</tr>\n</tbody>\n</table>\n<p>Note that the boundary term, \\(-\\phi _{N+1}(x_{N+1})q_{right}\\) does not depend on the temperature and thus this boundary condition does not impact the stiffness matrix.</p>\n<h2 class=\"subhead\">Implementation of Robin Boundary Conditions</h2>\n<p>To demonstrate the implementation of a Robin boundary condition, suppose that a convective heat transfer boundary condition were to be set at \\(x=L/2\\), specifically,</p>\n<table id=\"a0000000391\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[-k T_ x(L/2) = h_{ext}\\left[T_{ext}-T(L/2)\\right].\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.256)</td>\n</tr>\n</tbody>\n</table>\n<p>Following the basic process outlined in the Neumann boundary condition, the weighted residual for \\(i=N+1\\) is</p>\n<table id=\"a0000000392\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_{N+1} = \\left.\\left(\\phi _{N+1} k \\tilde{T}_ x\\right)\\right|_{x=x_{N+1}} - \\int _{x_{N}}^{x_{N+1}} {\\phi _{N+1}}_ x\\,  k \\tilde{T}_ x\\, dx + \\int _{x_{N}}^{x_{N+1}} \\phi _{N+1} f\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.257)</td>\n</tr>\n</tbody>\n</table>\n<p>Substituting \\(-k \\tilde{T}_ x = h_{ext}\\left[T_{ext}-\\tilde{T}(x_{N+1})\\right]\\) in the boundary term gives</p>\n<table id=\"a0000000393\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_{N+1} = -\\phi _{N+1}(x_{N+1})h_{ext}\\left[T_{ext}-\\tilde{T}(x_{N+1})\\right] - \\int _{x_{N}}^{x_{N+1}} {\\phi _{N+1}}_ x\\,  k \\tilde{T}_ x\\, dx + \\int _{x_{N}}^{x_{N+1}} \\phi _{N+1} f\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.258)</td>\n</tr>\n</tbody>\n</table>\n<p>As opposed to the Neumann boundary condition, the Robin boundary condition implementation does introduce a new dependence on the solution, specifically on \\(\\tilde{T}(x_{N+1})\\). This will cause a change in the stiffness matrix. Furthermore, the \\(T_{ext}\\) term will alter the right-hand side vector in the FEM numerical implementation.</p>\n<h2 class=\"subhead\">Boundary Condition Implementation Details</h2>\n<p>In class, we will discuss the details of the implementation of the boundary conditions into a computer program using the following MATLAB<sup>\u00ae</sup> script.</p>\n<pre class=\"edx\">\r\n\r\n% FEM solver for k d2T/dx2 + f = 0 where f = 50 exp(x)\r\n%\r\n% Thermal conductivity is set to one, k=1.\r\n%\r\n% BC's:\r\n%\r\n% At x=-1:   T(-1) = 100   (Dirichlet)\r\n%\r\n% At x=1, two options exist:\r\n%\r\n%            Specified heat transfer: dT/dX = qright\r\n%\r\n%            Convection: -k dT/dx = hext * (Text - T(1))\r\n%\r\n% The choice of which bc to use is made through RightBC.\r\n% If RightBC = 0, heat transfer rate is specified.  Otherwise,\r\n% a convection BC is applied.\r\n%\r\n% Gaussian quadrature is used in evaluating the forcing integral.\r\n%\r\n% Note: the finite element degrees of freedom are\r\n%       stored in the vector, T.\r\n\r\nclear all;\r\n\r\n% Number of elements\r\nnElem = 5;\r\nx = linspace(-1,1,nElem+1);\r\n\r\n% Set RightBC info\r\nRightBC = 1;\r\nif (RightBC == 0),\r\n  qright = 0;\r\nelse,\r\n  hext = 10;\r\n  Text = 100;\r\nend\r\n\r\n% Set quadrature rule\r\nNq = 2;\r\nif (Nq == 1),\r\n  alphaq(1) = 2.0; xiq(1) = 0.0;\r\nelseif (Nq == 2),\r\n  alphaq(1) = 1.0; xiq(1) = -1/sqrt(3);\r\n  alphaq(2) = 1.0; xiq(2) =  1/sqrt(3);\r\nelse\r\n  fprintf('Error: Unknown quadrature rule (Nq = %i)\\n',Nq);\r\n  return;\r\nend\r\n\r\n% Zero stiffness matrix\r\nK = zeros(nElem+1, nElem+1);\r\nF = zeros(nElem+1, 1);\r\n\r\n% Loop over all elements and calculate stiffness and residuals\r\nfor elem = 1:nElem,\r\n\r\n  n1 = elem;\r\n  n2 = elem+1;\r\n\r\n  x1 = x(n1);\r\n  x2 = x(n2);\r\n\r\n  dx = x2 - x1;\r\n\r\n  % Add contribution to n1 weighted residual due to n1 function\r\n  K(n1, n1) = K(n1, n1) - (1/dx);\r\n\r\n  % Add contribution to n1 weighted residual due to n2 function\r\n  K(n1, n2) = K(n1, n2) + (1/dx);\r\n\r\n  % Add contribution to n2 weighted residual due to n1 function\r\n  K(n2, n1) = K(n2, n1) + (1/dx);\r\n\r\n  % Add contribution to n2 weighted residual due to n2 function\r\n  K(n2, n2) = K(n2, n2) - (1/dx);\r\n\r\n  % Evaluate forcing term using quadrature\r\n  for nn = 1:Nq,\r\n\r\n    % Get xi location of quadrature point\r\n    xi = xiq(nn);\r\n\r\n    % Calculate x location of quadrature point\r\n    xq = x1 + 0.5*(1+xi)*dx;\r\n\r\n    % Calculate f\r\n    f = 50*exp(xq);\r\n\r\n    % Calculate phi1 and phi2\r\n    phi1 = 0.5*(1-xi);\r\n    phi2 = 0.5*(1+xi);\r\n\r\n    % Add forcing term to n1 weighted residual\r\n    F(n1) = F(n1) - alphaq(nn)*0.5*phi1*f*dx;\r\n\r\n    % Add forcing term to n2 weighted residual\r\n    F(n2) = F(n2) - alphaq(nn)*0.5*phi2*f*dx;\r\n\r\n  end\r\n\r\nend\r\n\r\n\r\n% Set Dirichlet conditions at x=-1\r\nn1 = 1;\r\nK(n1,:)    = zeros(size(1,nElem+1));\r\nK(n1, n1)  = 1.0;\r\nF(n1)      = 100.0;\r\n\r\n\r\n% Set boundary condition at x=1\r\nn1 = nElem+1;\r\nif (RightBC == 0), % Specify heat transfer rate (Neumann)\r\n\r\n  F(n1)     = F(n1) + qright;\r\n\r\nelse, % Convective\r\n\r\n  K(n1,n1)  = K(n1,n1) + hext;\r\n  F(n1)     = F(n1) + hext*Text;\r\n\r\nend\r\n\r\n\r\n% Solve for solution\r\nT = K\\F;\r\n\r\n\r\n% Plot solution\r\nplot(x,T,'*-');\r\nxlabel('x');\r\nylabel('T');\r\n\r\n</pre>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/more-on-finite-element-methods');\">Back<span>More on Finite Element Methods</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion');\">Continue<span>The Finite Element Method for Two-Dimensional Diffusion</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "365c70a74666ed1cd1408aeb96bff4a6", 
    "title": "2.10 More on Finite Element Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "2f262139b40f526166c951230f32cd54", 
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "365c70a74666ed1cd1408aeb96bff4a6", 
            "6267326555dff200dae2644697a179db", 
            "c782bcc9abb3f6bcc638027dfffdc386"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/more-on-finite-element-methods/1690r-boundary-conditions-for-finite-elements", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "6267326555dff200dae2644697a179db", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:14.246 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:09.029 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.10.2 Boundary Conditions for Finite Elements", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:50:14.295 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:48:16.784 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/01/13 14:32:23.808 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/01/13 14:33:55.103 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/05/27 11:00:37.994 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/05/27 11:01:40.525 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/07/20 13:59:28.911 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 14:01:54.676 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/11 15:05:14.215 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 15:06:33.928 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 6, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "6267326555dff200dae2644697a179db", 
            "365c70a74666ed1cd1408aeb96bff4a6", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "c782bcc9abb3f6bcc638027dfffdc386"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/more-on-finite-element-methods/1690r-boundary-conditions-for-finite-elements", 
    "is_image_gallery": false, 
    "id": "1690r-boundary-conditions-for-finite-elements", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:20.485 US/Eastern", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.10 More on Finite Element Methods", 
            "string"
        ]
    ], 
    "_id": "1690r-boundary-conditions-for-finite-elements", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/more-on-finite-element-methods/1690r-boundary-conditions-for-finite-elements"
}