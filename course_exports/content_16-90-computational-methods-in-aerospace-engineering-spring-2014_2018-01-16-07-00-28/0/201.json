{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/bb1256bc6917d853f4c636ce5943967f\"><<span>Importance Sampling</span></a></li><li id=\"flp_btn_1\" class=\"button_selected\"><a href=\"./resolveuid/6f317b0d95c1d32cf178c9fdbae632d2\">3.6.1<span>Design Optimization</span></a></li><li id=\"flp_btn_2\"><a href=\"./resolveuid/1d39506a8ae7400e107dfe048008e5c2\">3.6.2<span>Gradient Based Optimization</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/da1cf6178af153b14d4a177f24979948\">3.6.3<span>Unconstrained Gradient-Based Optimization Methods</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/c0e755e748fd33db478288b736e0381c\">3.6.4<span>Finite Difference Methods</span></a></li><li id=\"flp_btn_5\"><a href=\"./resolveuid/3cde12b3c306b87c973f5af561f4875f\">3.6.5<span>The 1d Search</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/1d39506a8ae7400e107dfe048008e5c2\">><span>Gradient Based Optimization</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">3.6.1 Design Optimization</h2>\n<p id=\"taglist\">\n<a id=\"optimizationtechniques\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO317\" title=\"MO3.17:  Describe the steepest descent, conjugate gradient, and the Newton method for optimization of multivariate functions, and apply these optimization techniques to simple unconstrained design problems. \">Measurable Outcome 3.17</a>\n</p>\n<p>\n</p>\n<p>\nEngineers are often interested in solving design problems where the best possible design is selected from all possible alternatives. A design problem can be written as an optimization problem, with a set of design variables, objective functions and constraints, defined below. </p>\n<p><b class=\"bfseries\">Design variables:</b> Design vector \\(x\\) contains \\(n\\) variables that form the design space. During design space exploration or optimization we change the entries of \\(x\\) in some rational fashion to achieve a desired effect. e.g.&#xA0;, design variables might be wing span, number of engines, airfoil geometries, etc. </p>\n<p><b class=\"bfseries\">Objective Functions:</b> The objective can be a vector \\(J\\) of \\(z\\) system responses or characteristics we are trying to maximize or minimize, \\(J = [ J_1 J_2 \\ldots J_ z ]^ T\\). In many cases the objective is a scalar function, but for real systems often we attempt multi-objective optimization. Here we will consider only a scalar objective, \\(J\\). </p>\n<p><b class=\"bfseries\">Constraints:</b> Constraints act as boundaries of the design space and typically occur due to finiteness of resources or technological limitations of some design variables. Often, but not always, optimal designs lie at the intersection of several active constraints. </p>\n<p>\nWe will write our optimization problem as follows: </p>\n<table id=\"a0000000481\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\min _ x J(x) \\\\ s.t. c(x) = 0 \\\\ g(x) \\leq 0 \\\\ x_{i}^{l} \\leq x_ i \\leq x_{i}^{u}, i = 1, \\ldots , n.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.66)</td>\n</tr>\n</table>\n<p>\nHere, \\(x \\in \\mathbb {R}^ n\\) is the vector of \\(n\\) design variables, \\(J(x):\\mathbb {R}^ n \\to \\mathbb {R}\\) is the scalar objective function, \\(c(x):\\mathbb {R}^ n \\to \\mathbb {R}^{m_1}\\) is a vector that defines the \\(m_1\\) equality constraints, and \\(g(x):\\mathbb {R}^ n \\to \\mathbb {R}^{m_2}\\) is a vector that defines the \\(m_2\\) inequality constraints. \\(x_{i}^{l} \\in \\mathbb {R}^ n\\) and \\(x_{i}^{u} \\in \\mathbb {R}^ n\\) define lower and upper bounds, respectively, for the \\(i\\)th design variable \\(x_ i\\). </p>\n<p>\nThe resulting optimization problem is typically solved using numerical optimization methods. In this section, we will discuss some basic approaches to solving optimization problems. </p>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/variance-reduction-techniques-for-the-monte-carlo-method/1690r-importance-sampling');\">Back<span>Importance Sampling</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization/1690r-gradient-based-optimization');\">Continue<span>Gradient Based Optimization</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "6f317b0d95c1d32cf178c9fdbae632d2", 
    "title": "3.6 Introduction to Design Optimization", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "c0e755e748fd33db478288b736e0381c", 
            "487c3b15ab67d7c95cffa6b147049d0c", 
            "1d39506a8ae7400e107dfe048008e5c2", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "6f317b0d95c1d32cf178c9fdbae632d2"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "487c3b15ab67d7c95cffa6b147049d0c", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:32.019 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:57.017 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "3.6 Introduction to Design Optimization", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:50:32.129 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:48:59.764 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 5, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "bb1256bc6917d853f4c636ce5943967f", 
            "6f317b0d95c1d32cf178c9fdbae632d2", 
            "1d39506a8ae7400e107dfe048008e5c2", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "da1cf6178af153b14d4a177f24979948", 
            "c0e755e748fd33db478288b736e0381c", 
            "3cde12b3c306b87c973f5af561f4875f"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization", 
    "is_image_gallery": false, 
    "id": "introduction-to-design-optimization", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:41.866 GMT-4", 
    "always_publish": false, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "3.6 Introduction to Design Optimization", 
            "string"
        ]
    ], 
    "_id": "introduction-to-design-optimization", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization"
}