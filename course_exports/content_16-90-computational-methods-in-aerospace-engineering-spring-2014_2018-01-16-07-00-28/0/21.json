{
    "text": "<div class=\"navigation pagination\">\r\n<ul>\r\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/f239c31a53e628b3a06a8e6fff5ed57c\">&lt;<span>Discretizing ODEs</span></a></li>\r\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/f239c31a53e628b3a06a8e6fff5ed57c\">1.2.1<span>First-Order ODEs</span></a></li>\r\n<li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/18e3e2fd45b738b5b9bb4940200d150a\">1.2.2<span>An Example of First Order ODE</span></a></li>\r\n<li id=\"flp_btn_3\"><a href=\"./resolveuid/0baef83edfb4b31266ed4f9fe5c876ba\">1.2.3<span>Discretization</span></a></li>\r\n<li id=\"flp_btn_4\"><a href=\"./resolveuid/9b1b577d12e2e60d75d3f8fb6ed609d5\">1.2.4<span>The Forward Euler Method</span></a></li>\r\n<li id=\"flp_btn_5\"><a href=\"./resolveuid/ae250ef953d178da8810f88b0aaa6408\">1.2.5<span>The Midpoint Method</span></a></li>\r\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/0baef83edfb4b31266ed4f9fe5c876ba\">&gt;<span>Discretization</span></a></li>\r\n</ul>\r\n</div>\r\n<!--?xml version='1.0' encoding='utf-8'?-->\r\n<h2 class=\"subhead\">1.2.2 An example of first order ODE</h2>\r\n<p id=\"taglist\"><a id=\"firstorderodes\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO11\" title=\"MO1.1:  Define a first-order ODE. \">Measurable Outcome 1.1</a>, <a id=\"nonlinear\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO13\" title=\"MO1.3:  Distinguish nonlinear ODEs from linear ODEs. \">Measurable Outcome 1.3</a>, <a id=\"linearize\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO14\" title=\"MO1.4:  Approximate the behavior of a nonlinear equation with a linear one. \">Measurable Outcome 1.4</a></p>\r\n<p>Ordinary differential equations (ODEs) occur throughout science and engineering.</p>\r\n<h2 class=\"subhead\">Free Fall Example</h2>\r\n<p>A model for the velocity \\(u(t)\\) of a spherical object falling freely through the atmosphere can be derived by applying Newton's Law. Specifically,</p>\r\n<table id=\"equ:freefall\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[m_ p u_ t = m_ p g - D(u) \\label{equ:freefall}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.4)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>where \\(m_ p\\) is the mass of the particle, \\(g\\) is the gravity, \\(D\\) is the aerodynamic drag acting on the particle, and \\(u_ t\\) denotes the derivative of \\(u(t)\\) with respect to time \\(t\\). For low speeds, this drag can be modeled as (recall Unified fluids),</p>\r\n<table id=\"&lt;plasTeX.TeXFragment object at 0x1047ec7a0&gt;\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr id=\"a0000000004\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  D\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\frac{1}{2}\\rho _ g \\pi a^2 u^2 C_ D(Re) \\label{equ:freefall_ drag}\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.5)</td>\r\n</tr>\r\n<tr id=\"&lt;plasTeX.TeXFragment object at 0x1047eca10&gt;\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle Re\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\frac{2 \\rho _ g u a}{\\mu _ g} \\label{equ:freefall_ Re}\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.6)</td>\r\n</tr>\r\n<tr id=\"&lt;plasTeX.TeXFragment object at 0x1047ecd50&gt;\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle C_ D\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\frac{24}{Re} + \\frac{6}{1 + \\sqrt {Re}} + 0.4 \\label{equ:freefall_ CD}\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.7)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>where \\(\\rho _ g\\) and \\(\\mu _ g\\) are the density and dynamic viscosity of the atmosphere, \\(a\\) is the sphere radius, \\(Re\\) is the Reynolds number for the sphere, and \\(C_ D\\) is the drag coefficient. To complete the problem specification, an initial condition is required on the velocity. For example, at time \\(t=0\\), let \\(u(0) = u_0\\). By solving Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML-full&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$m\\_\\ p\\ u\\_\\ t\\ \\=\\ m\\_\\ p\\ g\\ \\-\\ D\\(u\\)\\ \\\\label\\{equ\\:freefall\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.4)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.4)');\">1.4</a> with this initial condition, the velocity at any time \\(u(t)\\) can be found.</p>\r\n<h2 class=\"subhead\">General ODEs</h2>\r\n<p>A general ODE is typically written in the form,</p>\r\n<table id=\"&lt;plasTeX.TeXFragment object at 0x104807120&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u_ t(t) = f(u(t),t), \\label{equ:ODE_ nonlinear}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.8)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>where \\(f(u(t),t)\\) is the forcing function that results in the evolution of \\(u(t)\\) in time. When \\(f(u(t),t)\\) depends on \\(u(t)\\) in a nonlinear manner, then the ODE is called a nonlinear ODE. Note that in these notes, where clarity permits, we will omit the arguments of functions.</p>\r\n<p>For the example defined by Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML-full&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$m\\_\\ p\\ u\\_\\ t\\ \\=\\ m\\_\\ p\\ g\\ \\-\\ D\\(u\\)\\ \\\\label\\{equ\\:freefall\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.4)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.4)');\">1.4</a>, the forcing function is,</p>\r\n<table id=\"a0000000005\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[f(u,t) = g - \\frac{1}{m_ p} D(u)\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.9)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>From the definition of \\(D(u)\\), \\(f(u,t)\\) is nonlinear in \\(u\\). Note also that in this example, \\(f\\) does not depend on \\(t\\) directly, rather \\(f(u(t),t) = f(u(t))\\).</p>\r\n<h2 class=\"subhead\">Linear ODEs</h2>\r\n<p>Although the use of numerical integration is most important for nonlinear ODE's (since analytic solutions rarely exist), the study of numerical methods applied to linear ODE's is often quite helpful in understanding the behavior for nonlinear problems. The general form for a single, linear ODE, is</p>\r\n<table id=\"&lt;plasTeX.TeXFragment object at 0x10480b460&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u_ t = \\lambda (t) u(t) + g(t), \\label{equ:ODE_ linear}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.10)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>where \\(\\lambda (t)\\) is independent of \\(u\\). When \\(\\lambda (t)\\) is a constant, the ODE is referred to as a linear ODE with constant coefficients.</p>\r\n<p>In many situations, the linear ODE is derived by linearizing a nonlinear ODE about a constant state. Specifically, define the dependent state \\(u(t)\\) as a sum of \\(u_0\\) and a perturbation, \\(\\tilde{u}(t)\\),</p>\r\n<table id=\"equ:perturbation\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u(t) = u_0 + \\tilde{u}(t). \\label{equ:perturbation}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.11)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>A linearized equation for the evolution of \\(\\tilde{u}(t)\\) can be derived by substitution of this into Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML-full&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$u\\_\\ t\\(t\\)\\ \\=\\ f\\(u\\(t\\)\\,t\\)\\,\\ \\\\label\\{equ\\:ODE\\_\\ nonlinear\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.8)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.8)');\">1.8</a>:</p>\r\n<table id=\"a0000000006\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr id=\"a0000000007\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  u_ t\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  f(u(t),t),\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.12)</td>\r\n</tr>\r\n<tr id=\"a0000000008\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle \\tilde{u}_ t\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  f(u_0 + \\tilde{u}(t),t)\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.13)</td>\r\n</tr>\r\n<tr id=\"a0000000009\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle \\tilde{u}_ t\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  f(u_0, 0) + \\left.\\frac{\\partial f}{\\partial u}\\right|_{u_0,0}\\tilde{u}(t) + \\left.\\frac{\\partial f}{\\partial t}\\right|_{u_0,0}t + O(t^2, \\tilde{u} t, \\tilde{u}^2).\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.14)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Note that the last equation is obtained using <a href=\"http://crosslinks.mit.edu/topic/taylor-series/\">Taylor series</a>. Thus, when \\(t\\) and \\(\\tilde{u}\\) are small, the perturbation satisfies the linear equation,</p>\r\n<table id=\"&lt;plasTeX.TeXFragment object at 0x10484da10&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\tilde{u}_ t \\approx f(u_0, 0) + \\left.\\frac{\\partial f}{\\partial u}\\right|_{u_0,0}\\tilde{u}(t) + \\left.\\frac{\\partial f}{\\partial t}\\right|_{u_0,0}t \\label{equ:ODE_ linearized}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.15)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Comparing Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML-full&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$u\\_\\ t\\ \\=\\ \\\\lambda\\ \\(t\\)\\ u\\(t\\)\\ \\+\\ g\\(t\\)\\,\\ \\\\label\\{equ\\:ODE\\_\\ linear\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.10)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.10)');\">1.10</a> to <a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML-full&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\tilde\\{u\\}\\_\\ t\\ \\\\approx\\ f\\(u\\_0\\,\\ 0\\)\\ \\+\\ \\\\left\\.\\\\frac\\{\\\\partial\\ f\\}\\{\\\\partial\\ u\\}\\\\right\\|\\_\\{u\\_0\\,0\\}\\\\tilde\\{u\\}\\(t\\)\\ \\+\\ \\\\left\\.\\\\frac\\{\\\\partial\\ f\\}\\{\\\\partial\\ t\\}\\\\right\\|\\_\\{u\\_0\\,0\\}t\\ \\\\label\\{equ\\:ODE\\_\\ linearized\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.15)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.15)');\">1.15</a>, we see that in this example,</p>\r\n<table id=\"a0000000010\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr id=\"a0000000011\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\lambda (t)\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\left.\\frac{\\partial f}{\\partial u}\\right|_{u_0,0},\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.16)</td>\r\n</tr>\r\n<tr id=\"a0000000012\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle g(t)\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  f(u_0, 0) + \\left.\\frac{\\partial f}{\\partial t}\\right|_{u_0,0}t\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.17)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>For the falling sphere problem, a linear ODE can be derived by linearizing about the initial velocity \\(u_0\\). As shown above, this requires the calculation of \\({\\partial f}/{\\partial u}\\) and \\({\\partial f}/{\\partial t}\\). For the sphere,</p>\r\n<table id=\"a0000000013\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{\\partial f}{\\partial u} = \\frac{\\partial }{\\partial u}\\left[ g - \\frac{1}{m_ p} D(u(t))\\right] = -\\frac{1}{m_ p}\\frac{\\partial D}{\\partial u}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.18)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>The value of \\({\\partial D}/{\\partial u}\\) is</p>\r\n<table id=\"a0000000014\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr id=\"a0000000015\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\frac{\\partial D}{\\partial u}\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\frac{\\partial }{\\partial u}\\left[ \\frac{1}{2}\\rho _ g \\pi a^2 u^2 C_ D(Re) \\right],\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.19)</td>\r\n</tr>\r\n<tr id=\"a0000000016\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\rho _ g \\pi a^2 u C_ D(Re) + \\frac{1}{2}\\rho _ g \\pi a^2 u^2 \\frac{\\partial C_ D}{\\partial Re}\\frac{\\partial Re}{\\partial u},\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.20)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>and \\({\\partial C_ D}/{\\partial Re}\\) and \\({\\partial Re}/{\\partial u}\\) can be found from their definitions. Also, since \\(f\\) does not directly depend on \\(t\\) for this problem, \\({\\partial f}/{\\partial t} = 0.\\)</p>\r\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes');\">Back<span>Discretizing ODEs</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes/1690r-discretization');\">Continue<span>Discretization</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "18e3e2fd45b738b5b9bb4940200d150a", 
    "title": "1.2 Discretizing ODEs", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "0baef83edfb4b31266ed4f9fe5c876ba", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "9b1b577d12e2e60d75d3f8fb6ed609d5", 
            "ae250ef953d178da8810f88b0aaa6408", 
            "18e3e2fd45b738b5b9bb4940200d150a", 
            "f239c31a53e628b3a06a8e6fff5ed57c"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes/1690r-an-example-of-first-order-ode", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "f239c31a53e628b3a06a8e6fff5ed57c", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:48:36.266 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:40:53.395 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.2.2 An Example of First Order ODE", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:48:36.309 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:40:54.723 US/Eastern"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2016/02/16 07:51:09.203 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2016/02/16 07:51:20.876 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/04/28 14:23:36.448 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2016/05/25 06:14:44.941 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:02:15.885 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:03:28.694 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 0, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "18e3e2fd45b738b5b9bb4940200d150a", 
            "f239c31a53e628b3a06a8e6fff5ed57c", 
            "0baef83edfb4b31266ed4f9fe5c876ba", 
            "9b1b577d12e2e60d75d3f8fb6ed609d5", 
            "ae250ef953d178da8810f88b0aaa6408"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes/1690r-an-example-of-first-order-ode", 
    "is_image_gallery": false, 
    "id": "1690r-an-example-of-first-order-ode", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/27 10:27:58.697 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.2 Discretizing ODEs", 
            "string"
        ]
    ], 
    "_id": "1690r-an-example-of-first-order-ode", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes/1690r-an-example-of-first-order-ode"
}