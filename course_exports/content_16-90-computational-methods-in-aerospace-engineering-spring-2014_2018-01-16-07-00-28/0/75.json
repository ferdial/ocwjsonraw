{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/c5e7e539a82c8e620c8ae738a18f6f10\"><<span>Runge-Kutta Methods</span></a></li><li id=\"flp_btn_1\"><a href=\"./resolveuid/c5e7e539a82c8e620c8ae738a18f6f10\">1.9.1<span>Two-Stage Runge-Kutta Methods</span></a></li><li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/d5200dfdd11c9d138626a6b253c602ff\">1.9.2<span>Four-Stage Runge-Kutta Method</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/1f5a4263b1f3f52202d6af00f58ae206\">1.9.3<span>Stability Regions</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/1f5a4263b1f3f52202d6af00f58ae206\">><span>Stability Regions</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">1.9.2 Four-stage Runge-Kutta Method</h2>\n<p id=\"taglist\">\n<a id=\"rkmulti\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO116\" title=\"MO1.16:  Describe the form of the Runge-Kutta family of multi-stage methods. \">Measurable Outcome 1.16</a>, <a id=\"mstepvmstage\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO117\" title=\"MO1.17:  Explain the relative computational costs of multi-step versus multi-stage methods. \">Measurable Outcome 1.17</a>, <a id=\"odeinteg\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO119\" title=\"MO1.19:  Recommend an appropriate ODE integration method based on the features of the problem being solved. \">Measurable Outcome 1.19</a>\n</p>\n<p>\nThe most popular form of a four-stage Runge-Kutta method is: </p>\n<table id=\"a0000000128\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000129\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  a\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  {\\Delta t}f(v^ n, t^ n)\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.145)</td>\n</tr>\n<tr id=\"a0000000130\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle b\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  {\\Delta t}f(v^ n+a/2, t^ n + {\\Delta t}/2)\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.146)</td>\n</tr>\n<tr id=\"a0000000131\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle c\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  {\\Delta t}f(v^ n+b/2, t^ n + {\\Delta t}/2)\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.147)</td>\n</tr>\n<tr id=\"a0000000132\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle d\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  {\\Delta t}f(v^ n+c, t^ n + {\\Delta t})\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.148)</td>\n</tr>\n<tr id=\"a0000000133\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle v^{n+1}\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  v^ n + \\frac{1}{6}(a + 2b + 2c + d)\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.149)</td>\n</tr>\n</table>\n<p>\n Note that this method requires four evaluations of \\(f\\) per iteration. This method is fourth-order accurate, \\(p=4\\). </p>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods');\">Back<span>Runge-Kutta Methods</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-stability-regions');\">Continue<span>Stability Regions</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "d5200dfdd11c9d138626a6b253c602ff", 
    "title": "1.9 Runge-Kutta Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "1f5a4263b1f3f52202d6af00f58ae206", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "c5e7e539a82c8e620c8ae738a18f6f10"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-four-stage-runge-kutta-method", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "c5e7e539a82c8e620c8ae738a18f6f10", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:17.973 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:43:30.565 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.9.2 Four-Stage Runge-Kutta Method", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:49:18.019 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:43:32.303 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 0, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-four-stage-runge-kutta-method", 
    "is_image_gallery": false, 
    "id": "1690r-four-stage-runge-kutta-method", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:24.417 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.9 Runge-Kutta Methods", 
            "string"
        ]
    ], 
    "_id": "1690r-four-stage-runge-kutta-method", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-four-stage-runge-kutta-method"
}