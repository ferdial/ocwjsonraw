{
    "text": "<div class=\"navigation pagination\"><ul> <li id=\"top_bck_btn\"><a href=\"./resolveuid/75e73977a306f5532134b4e0a24fdb81\">&lt;<span>Adams-Moulton Methods</span></a></li> <li id=\"flp_btn_1\"><a href=\"./resolveuid/67717326dffb74445162101fd9a9ec91\">1.8.1<span>Adams-Bashforth Methods</span></a></li> <li id=\"flp_btn_2\"><a href=\"./resolveuid/75e73977a306f5532134b4e0a24fdb81\">1.8.2<span>Adams-Moulton Methods</span></a></li> <li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/998bd38300b68dbd2d38c251f8262e37\">1.8.3<span>Backwards Differentiation Methods</span></a></li> <li id=\"flp_btn_4\"><a href=\"./resolveuid/f02cecfe1cd48c17eeff25be1aaa895d\">1.8.4<span>Backwards Differentiation Excercise</span></a></li> <li id=\"top_continue_btn\"><a href=\"./resolveuid/f02cecfe1cd48c17eeff25be1aaa895d\">&gt;<span>Backwards Differentiation Excercise</span></a></li> </ul></div> <!--?xml version='1.0' encoding='utf-8'?--> <h2 class=\"subhead\">1.8.3 Backwards Differentiation Methods</h2> <p id=\"taglist\"><a id=\"stiffonmethod\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO112\" title=\"MO1.12:  Describe how stiffness impacts the choice of numerical method for solving the equations. \">Measurable Outcome 1.12</a>, <a id=\"multistep\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO115\" title=\"MO1.15:  Describe multi-step methods, including the Adams-Bashforth, Adams-Moulton, and backwards differentiation families. \">Measurable Outcome 1.15</a>, <a id=\"mstepvmstage\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO117\" title=\"MO1.17:  Explain the relative computational costs of multi-step versus multi-stage methods. \">Measurable Outcome 1.17</a>, <a id=\"stabilityboundry\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO118\" title=\"MO1.18:  Determine the stability boundary for a multi-step or multi-stage method applied to a linear system of ODEs. \">Measurable Outcome 1.18</a>, <a id=\"odeinteg\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO119\" title=\"MO1.19:  Recommend an appropriate ODE integration method based on the features of the problem being solved. \">Measurable Outcome 1.19</a></p> <p>Backwards differentiation methods are some of the best multi-step methods for stiff problems. The backwards differentiation formulae are of the form,</p> <table id=\"equ:bd\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[v^{n+1} + \\sum _{i=1}^ s \\alpha _ i v^{n+1-i} = {\\Delta t}\\beta _0 f^{n+1}. \\label{equ:bd}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.138)</td> </tr> </tbody> </table> <p>The coefficients for the first through fourth order methods are given in the table below.</p> <div id=\"tab:bd\" class=\"table\"><center style=\"border-style:hidden\"> <table cellspacing=\"0\" class=\"tabular\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td style=\"text-align:center;border-style:hidden\"><p>\\(p\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\alpha _1\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\alpha _2\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\alpha _3\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\alpha _4\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\beta _0\\)</p></td> </tr> <tr> <td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\"><p>1</p></td> <td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\"><p>-1</p></td> <td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">&nbsp;</td> <td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">&nbsp;</td> <td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">&nbsp;</td> <td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\"><p>1</p></td> </tr> <tr> <td style=\"text-align:center;border-style:hidden\"><p>2</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(-\\frac{4}{3}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{1}{3}\\)</p></td> <td style=\"text-align:center;border-style:hidden\">&nbsp;</td> <td style=\"text-align:center;border-style:hidden\">&nbsp;</td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{2}{3}\\)</p></td> </tr> <tr> <td style=\"text-align:center;border-style:hidden\"><p>3</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(-\\frac{18}{11}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{9}{11}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(-\\frac{2}{11}\\)</p></td> <td style=\"text-align:center;border-style:hidden\">&nbsp;</td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{6}{11}\\)</p></td> </tr> <tr> <td style=\"text-align:center;border-style:hidden\"><p>4</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(-\\frac{48}{25}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{36}{25}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(-\\frac{16}{25}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{3}{25}\\)</p></td> <td style=\"text-align:center;border-style:hidden\"><p>\\(\\frac{12}{25}\\)</p></td> </tr> </tbody> </table> </center></div> <div class=\"caption\"><b>Table 4</b>: <span>Coefficients for backward differentiation methods</span></div> <p>&nbsp;</p> <p>The stability boundary for these methods are shown below. As can be seen, all of these methods are stable everywhere on the negative real axis, and are mostly stable in the left-half plane in general. Thus, backwards differentiation work well for stiff problems in which stong damping is present.</p> <div id=\"fig:bd_stab\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/3dabc32c84284e1f722abfd3244e215f\" alt=\"This figure shows the overlapping backwards differentiation stability regions for p=1 through p=4 method.\" width=\"402px\" height=\"391px\" /> </center></div> <div class=\"caption\"><b>Figure 1.21</b>: <span>Backwards differentiation stability regions for \\(p=1\\) through \\(p=4\\) method. Note: interior of curves is unstable region.</span></div> <h2 class=\"subhead\">MATLAB<sup>&reg;</sup>'s ODE Integrators</h2> <p>MATLAB has a a set of tools for integration of ODE's. We will briefly look at two of them: <b class=\"bfseries\">ode45</b> and <b class=\"bfseries\">ode15s</b>. <b class=\"bfseries\">ode45</b> is designed to solve problems that are not stiff while <b class=\"bfseries\">ode15s</b> is intended for stiff problems. <b class=\"bfseries\">ode45</b> is based on a four and five-stage Runge-Kutta integration (discussed in Section&nbsp;<a href=\"./resolveuid/c5e7e539a82c8e620c8ae738a18f6f10\">1.9</a>), while <b class=\"bfseries\">ode15s</b> is based on a range of highly stable implicit integration formulas (one option when using <b class=\"bfseries\">ode15s</b> is to use the backwards differentiation formulas). As a short illustration on how these MATLAB ODE integrators are implemented, the following script solves the one-dimensional diffusion problem from Section&nbsp;<a href=\"/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/stiffness-and-implicit-methods/\">1.7.1</a> using either <b class=\"bfseries\">ode45</b> or <b class=\"bfseries\">ode15s</b>. The specific problem we consider here is a bar which is initially at a temperature \\(T_{init} = 400 K\\) and at \\(t=0\\), the temperature at the left and right ends is suddenly raised to \\(800 K\\) and \\(1000 K\\), respectively.</p> <pre class=\"edx\">\r\n% MATLAB script: dif1d_main.m\r\n%\r\n% This code solve the one-dimensional heat diffusion equation\r\n% for the problem of a bar which is initially at T=Tinit and\r\n% suddenly the temperatures at the left and right change to\r\n% Tleft and Tright.\r\n%\r\n% Upon discretization in space by a finite difference method,\r\n% the result is a system of ODE's of the form,\r\n%\r\n% u_t = Au + b\r\n%\r\n% The code calculates A and b.  Then, uses one of MATLAB's\r\n% ODE integrators, either ode45 (which is based on a Runge-Kutta\r\n% method and is not designed for stiff problems) or ode15s (which\r\n% is based on an implicit method and is designed for stiff problems).\r\n%\r\n\r\nclear all; close all;\r\n\r\nsflag = input('Use stiff integrator? (1=yes, [default=no]): ');\r\n\r\n% Set non-dimensional thermal coefficient\r\nk   = 1.0; % this is really k/(rho*cp)\r\n\r\n% Set length of bar\r\nL = 1.0; % non-dimensional\r\n\r\n% Set initial temperature\r\nTinit = 400;\r\n\r\n% Set left and right temperatures for t&gt;0\r\nTleft = 800;\r\nTright = 1000;\r\n\r\n% Set up grid size\r\nNx = input(['Enter number of divisions in x-direction: [default=' ...\r\n            '51]']);\r\nif (isempty(Nx)),\r\n  Nx = 51;\r\nend\r\n\r\nh  = L/Nx;\r\nx  = linspace(0,L,Nx+1);\r\n\r\n% Calculate number of iterations (Nmax) needed to iterate to t=Tmax\r\nTmax = 0.5;\r\n\r\n% Initialize a sparse matrix to hold stiffness &amp; identity matrix\r\nA = spalloc(Nx-1,Nx-1,3*(Nx-1));\r\nI = speye(Nx-1);\r\n\r\n% Calculate stiffness matrix\r\n\r\nfor ii = 1:Nx-1,\r\n\r\n  if (ii &gt; 1),\r\n    A(ii,ii-1) = k/h^2;\r\n  end\r\n\r\n  if (ii &lt; Nx-1),\r\n    A(ii,ii+1) = k/h^2;\r\n  end\r\n\r\n  A(ii,ii) = -2*k/h^2;\r\n\r\nend\r\n\r\n% Set forcing vector\r\nb = zeros(Nx-1,1);\r\nb(1)    = k*Tleft/h^2;\r\nb(Nx-1) = k*Tright/h^2;\r\n\r\n% Set initial vector\r\nv0 = Tinit*ones(1,Nx-1);\r\n\r\nif (sflag == 1),\r\n\r\n  % Call ODE15s\r\n  options = odeset('Jacobian',A);\r\n  [t,v] = ode15s(@dif1d_fun,[0 Tmax],v0,options,A,b);\r\n\r\nelse\r\n\r\n  % Call ODE45\r\n  [t,v] = ode45(@dif1d_fun,[0 Tmax],v0,[],A,b);\r\n\r\nend\r\n\r\n% Get midpoint value of T and plot vs. time\r\nTmid = v(:,floor(Nx/2));\r\nplot(t,Tmid);\r\nxlabel('t');\r\nylabel('T at midpoint');\r\n</pre> <p>&nbsp;</p> <p>As can be seen, this script pre-computes the linear system \\(A\\) and the column vector \\(b\\) since the forcing function for the one-dimensional diffusion problem can be written as the linear function, \\(f = Av + b\\). Then, when calling either ODE integrator, the function which returns \\(f\\) is the first argument in the call and is named, <b class=\"bfseries\">dif1d_fun</b>. This function is given below:</p> <pre class=\"edx\">\r\n% MATLAB function: dif1d_fun.m\r\n%\r\n% This routine returns the forcing term for\r\n% a one-dimensional heat diffusion problem\r\n% that has been discretized by finite differences.\r\n% Note that the matrix A and the vector b are pre-computed\r\n% in the main driver routine, dif1d_main.m, and passed\r\n% to this function.  Then, this function simply returns\r\n% f(v) = A*v + b.  So, in reality, this function is\r\n% not specific to 1-d diffusion.\r\n\r\nfunction [f] = dif1d_fun(t, v, A, b)\r\n\r\nf = A*v + b;\r\n</pre> <p>&nbsp;</p> <p>As can be seen from <b class=\"bfseries\">dif1d_fun</b>, \\(A\\) and \\(b\\) have been passed into the function and thus the calculation of \\(f\\) simply requires the multiplication of \\(v\\) by \\(A\\) and the addition of \\(b\\).</p> <p>The major difference between the implementation of the ODE integrators in MATLAB and our discussions is that MATLAB's implementations are adaptive. Specifically, MATLAB's integrators estimate the error at each iteration and then adjust the timestep to either improve the accuracy (i.e. by decreasing the timestep) or efficiency (i.e. by increasing the timestep).</p> <p>The results for the stiff integrator, <b class=\"bfseries\">ode15s</b> are shown in Figure&nbsp;<a href=\"./resolveuid/68c3f1b46bc70f77f3f9b4ae7782a553\" onclick=\"window.open(this.href,'16.90r','width=247,height=195','toolbar=1'); return false;\">1.22</a>.</p> <div id=\"fig:dif1d_ode15s\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/68c3f1b46bc70f77f3f9b4ae7782a553\" alt=\"The graph shows the rapidly increasing temperature evolution using MATLAB's ode15s integrator.\" width=\"495px\" height=\"390px\" /> </center></div> <div class=\"caption\"><b>Figure 1.22</b>: <span>Temperature evolution at the middle of bar with suddenly raised end temperatures using MATLAB's <b class=\"bfseries\">ode15s</b> integrator</span></div> <div id=\"fig:dif1d_ode45\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/96daf9d794ec9a99cd50ed8e56ce1c30\" alt=\"The graph shows the rapidly increasing temperature evolution using MATLAB's ode45 integrator.\" width=\"495px\" height=\"390px\" /> </center></div> <div class=\"caption\"><b>Figure 1.23</b>: <span>Temperature evolution at the middle of bar with suddenly raised end temperatures using MATLAB's <b class=\"bfseries\">ode45</b> integrator</span></div> <div id=\"fig:dif1d_ode45_zoom\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/6215e00ace11f28c4567f14a88a3275a\" alt=\"The graph shows a zoomed in version of the rapidly increasing temperature evolution using MATLAB's ode45 integrator, which more easily display the small scale oscillations.\" width=\"498px\" height=\"390px\" /> </center></div> <div class=\"caption\"><b>Figure 1.24</b>: <span>Temperature evolution at the middle of bar with suddenly raised end temperatures using MATLAB's <b class=\"bfseries\">ode45</b> integrator (zoomed version).</span></div> <p>&nbsp;</p> <p>These results look as expected (note: in integrating from \\(t=0\\) to \\(t=0.5\\), a total of 64 timesteps were taken).</p> <p>The results for the non-stiff integrator are shown in Figure&nbsp;<a href=\"./resolveuid/96daf9d794ec9a99cd50ed8e56ce1c30\" onclick=\"window.open(this.href,'16.90r','width=247,height=195','toolbar=1'); return false;\">1.23</a> and in a zoomed view in Figure&nbsp;<a href=\"./resolveuid/6215e00ace11f28c4567f14a88a3275a\" onclick=\"window.open(this.href,'16.90r','width=247,height=195','toolbar=1'); return false;\">1.24</a>. The presence of small scale oscillations can be clearly observed in the <b class=\"bfseries\">ode45</b> results. These oscillations are a result of the large negative eigenvalues which require small \\({\\Delta t}\\) to maintain stability. Since the <b class=\"bfseries\">ode45</b> method is adaptive, the timestep automatically decreases to maintain stability, but the oscillatory results clearly show that the stability is barely achieved. Also, as a measure of the relative inefficiency of the <b class=\"bfseries\">ode45</b> integrator for this stiff problem, note that 6273 timesteps were required to integrate from \\(t=0\\) to \\(t=0.5\\).</p> <p>One final concern regarding the efficiency of the stiff integrator <b class=\"bfseries\">ode15s</b>. In order for this method to work in an efficient manner for large systems of equations such as in this example, it is very important that the Jacobian matrix, \\({\\partial f}/{\\partial u}\\) be provided to MATLAB. If this is not done, then <b class=\"bfseries\">ode15s</b> will construct an approximation to this derivative matrix using finite differences and for large systems, this will become a significant cost. In the script <b class=\"bfseries\">dif1d_main</b>, the Jacobian is communicated to the <b class=\"bfseries\">ode15s</b> integrator using the <b class=\"bfseries\">odeset</b> routine. Note: <b class=\"bfseries\">ode45</b> is an explicit method and does not need the Jacobian so it is not provided in that case.</p> <div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods/1690r-adams-moulton-methods');\">Back<span>Adams-Moulton Methods</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods/1690r-backwards-differentiation-excercise');\">Continue<span>Backwards Differentiation Excercise</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "998bd38300b68dbd2d38c251f8262e37", 
    "title": "1.8 Multi-Step Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "f02cecfe1cd48c17eeff25be1aaa895d", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "998bd38300b68dbd2d38c251f8262e37", 
            "75e73977a306f5532134b4e0a24fdb81", 
            "67717326dffb74445162101fd9a9ec91"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods/1690r-backwards-differentiation-methods", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "67717326dffb74445162101fd9a9ec91", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:15.185 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:43:08.333 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.8.3 Backwards Differentiation Methods", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:49:15.230 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:43:08.408 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2015/12/07 09:29:40.042 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/12/08 07:49:07.426 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:38:40.756 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:46:20.926 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/11 14:00:28.725 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 14:02:45.544 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "75e73977a306f5532134b4e0a24fdb81", 
            "3dabc32c84284e1f722abfd3244e215f", 
            "c5e7e539a82c8e620c8ae738a18f6f10", 
            "96daf9d794ec9a99cd50ed8e56ce1c30", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "998bd38300b68dbd2d38c251f8262e37", 
            "6215e00ace11f28c4567f14a88a3275a", 
            "67717326dffb74445162101fd9a9ec91", 
            "f02cecfe1cd48c17eeff25be1aaa895d", 
            "68c3f1b46bc70f77f3f9b4ae7782a553", 
            "935324e31ab2cb5790590ba1f034fcd5"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods/1690r-backwards-differentiation-methods", 
    "is_image_gallery": false, 
    "id": "1690r-backwards-differentiation-methods", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/27 10:31:23.266 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.8 Multi-Step Methods", 
            "string"
        ]
    ], 
    "_id": "1690r-backwards-differentiation-methods", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods/1690r-backwards-differentiation-methods"
}