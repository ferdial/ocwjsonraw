{
    "text": "<div class=\"navigation pagination\">\r\n<ul>\r\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/58553753c9ccc012f030d87844ae8db5\">&lt;<span>Definition of Multi-Step Methods</span></a></li>\r\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/a3fbfbbde140393baed5af945a9316f9\">1.3.1<span>Errors</span></a></li>\r\n<li id=\"flp_btn_2\"><a href=\"./resolveuid/6121a3cad8f1c65278216a952ea8ff2c\">1.3.2<span>Local Truncation Error</span></a></li>\r\n<li id=\"flp_btn_3\"><a href=\"./resolveuid/09523d4baafc8f4ce9b7d3d67ad30e7b\">1.3.3<span>Local Order of Accuracy</span></a></li>\r\n<li id=\"flp_btn_4\"><a href=\"./resolveuid/58553753c9ccc012f030d87844ae8db5\">1.3.4<span>Definition of Multi-Step Methods</span></a></li>\r\n<li id=\"flp_btn_5\" class=\"button_selected\"><a href=\"./resolveuid/ff3b4491f1d1d23e2d3a939de701c5c2\">1.3.5<span>Example of Most Accurate Multi-Step Method</span></a></li>\r\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/99ee39b779f49afb0849103c798f1c50\">&gt;<span>Convergence</span></a></li>\r\n</ul>\r\n</div>\r\n<div class=\"self_assessment\">\r\n<h2 class=\"subhead\">1.3.5 Example of Most Accurate Multi-Step Method</h2>\r\n<p id=\"taglist\"><a id=\"discretize\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO15\" title=\"MO1.5:  Discretize a univariate function and its derivative, assess the truncation error using Taylor series analysis. \">Measurable Outcome 1.5</a>, <a id=\"femid\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO16\" title=\"MO1.6:  Describe the Forward Euler methods and the Midpoint methods. \">Measurable Outcome 1.6</a>, <a id=\"consistent\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO18\" title=\"MO1.8:  Assess whether a numerical method is consistent, and calculate its local order of accuracy. \">Measurable Outcome 1.8</a>, <a id=\"multistep\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO115\" title=\"MO1.15:  Describe multi-step methods, including the Adams-Bashforth, Adams-Moulton, and backwards differentiation families. \">Measurable Outcome 1.15</a></p>\r\n<text> </text>\r\n<p>&nbsp;</p>\r\n<p>In this example, we will derive the most accurate multi-step method of the following form:</p>\r\n<table id=\"a0000000049\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[v^{n+1} + \\alpha _1 v^ n + \\alpha _2 v^{n-1} = {\\Delta t}\\left[ \\beta _1 f^ n + \\beta _2 f^{n-1} \\right]\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.56)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>The local truncation error for this method is,</p>\r\n<table id=\"a0000000050\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\tau = - \\alpha _1 u^ n - \\alpha _2 u^{n-1} + {\\Delta t}\\left[ \\beta _1 f^ n + \\beta _2 f^{n-1} \\right] - u^{n+1}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.57)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Substitution of \\(f^ n = u_ t^ n\\) and \\(f^{n-1} = u_ t^{n-1}\\) gives,</p>\r\n<table id=\"a0000000051\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\tau = - \\alpha _1 u^ n - \\alpha _2 u^{n-1} + {\\Delta t}\\left[ \\beta _1 u_ t^ n + \\beta _2 u_ t^{n-1} \\right] - u^{n+1}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.58)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Then, Taylor series about \\(t=t^ n\\) are substituted for \\(u^{n-1}\\), \\(u_ t^{n-1}\\), and \\(u^{n+1}\\) to give,</p>\r\n<table id=\"a0000000052\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr id=\"a0000000053\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\tau\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  - \\alpha _1 u^ n - \\alpha _2\\left[ u^ n - {\\Delta t}u^ n_ t + \\frac{1}{2}{\\Delta t}^2 u^ n_{tt} - \\frac{1}{6}{\\Delta t}^3 u^ n_{ttt} + \\frac{1}{24}{\\Delta t}^4 u^ n_{tttt} + O({\\Delta t}^5) \\right]\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.59)</td>\r\n</tr>\r\n<tr id=\"a0000000054\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  + {\\Delta t}\\beta _1 u_ t^ n + {\\Delta t}\\beta _2 \\left[ u^ n_ t - {\\Delta t}u^ n_{tt} + \\frac{1}{2}{\\Delta t}^2 u^ n_{ttt} - \\frac{1}{6}{\\Delta t}^3 u^ n_{tttt} + O({\\Delta t}^4) \\right]\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.60)</td>\r\n</tr>\r\n<tr id=\"a0000000055\">\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">&nbsp;</td>\r\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  - \\left[ u^ n + {\\Delta t}u^ n_ t + \\frac{1}{2}{\\Delta t}^2 u^ n_{tt} + \\frac{1}{6}{\\Delta t}^3 u^ n_{ttt} + \\frac{1}{24}{\\Delta t}^4 u^ n_{tttt} + O({\\Delta t}^5) \\right]\\)</td>\r\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\r\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.61)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Next, collect the terms in powers of \\({\\Delta t}\\), which gives the following coefficients:</p>\r\n<table id=\"a0000000056\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tbody>\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\begin{array}{r@{:\\: \\: }cccccccccc} u^ n &amp;  - &amp;  \\alpha _1 &amp;  - &amp;  \\alpha _2 &amp; &amp; &amp; &amp; &amp;  - &amp;  1 \\\\[0.1in] {\\Delta t}u_ t^ n &amp; &amp; &amp; &amp;  \\alpha _2 &amp;  + &amp;  \\beta _1 &amp;  + &amp;  \\beta _2 &amp;  - &amp;  1 \\\\[0.1in] {\\Delta t}^2 u_{tt}^ n &amp; &amp; &amp;  - &amp;  \\frac{\\alpha _2}{2} &amp; &amp; &amp;  - &amp;  \\beta _2 &amp;  - &amp;  \\frac{1}{2} \\\\[0.1in] {\\Delta t}^3 u_{ttt}^ n &amp; &amp; &amp; &amp;  \\frac{\\alpha _2}{6} &amp; &amp; &amp;  + &amp;  \\frac{\\beta _2}{2} &amp;  - &amp;  \\frac{1}{6} \\\\[0.1in] {\\Delta t}^4 u_{tttt}^ n &amp; &amp; &amp;  -&amp;  \\frac{\\alpha _2}{24} &amp; &amp; &amp;  - &amp;  \\frac{\\beta _2}{6} &amp;  - &amp;  \\frac{1}{24} \\end{array}\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.62)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>To find the most accurate multi-step method of the given form, we solve for the values of \\(\\alpha _1\\), \\(\\alpha _2\\), \\(\\beta _1\\), and \\(\\beta _2\\) that result in the coefficients of the first four terms being identically zero.</p>\r\n<p><b class=\"bfseries\">Exercise 1</b> Use MATLAB<sup>&reg;</sup>'s backslash command in the form</p>\r\n<pre>\r\n x= A\\b </pre>\r\n<p>&nbsp;</p>\r\n<div id=\"Q1_div\" class=\"problem_question\">\r\n<p>Which of the following most closely matches your solution for \\([\\alpha _1, \\alpha _2, \\beta _1, \\beta _2]^ T\\)?</p>\r\n<fieldset><legend class=\"visually-hidden\">Exercise 1</legend><div class=\"choice\"><label id=\"Q1_input_1_label\"><span id=\"Q1_input_1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_1\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\r\n<text> \\([4,-5,2,4]\\)</text>\r\n</span><span id=\"Q1_input_1_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q1_input_2_label\"><span id=\"Q1_input_2_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_2\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\r\n<text> \\([1,0,-1,5]\\)</text>\r\n</span><span id=\"Q1_input_2_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q1_input_3_label\"><span id=\"Q1_input_3_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_3\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\r\n<text> \\([4,2,-4,1]\\)</text>\r\n</span><span id=\"Q1_input_3_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q1_input_4_label\"><span id=\"Q1_input_4_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_4\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"true\" /><span class=\"choice\">\r\n<text> \\([4,-5,4,2]\\)</text>\r\n</span><span id=\"Q1_input_4_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div></fieldset>\r\n</div>\r\n<p>&nbsp;</p>\r\n\r\n<div id=\"S1_div\" class=\"problem_solution\" tabindex=\"-1\">&nbsp;</div>\r\n\r\n<div id=\"Q2_div\" class=\"problem_question\">\r\n<p><b class=\"bfseries\">Exercise 2</b> What is the order of accuracy of the scheme you found in the exercise above?</p>\r\n<fieldset><legend class=\"visually-hidden\">Exercise 2</legend><div class=\"choice\"><label id=\"Q2_input_1_label\"><span id=\"Q2_input_1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q2_input_1\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\r\n<text> p=1</text>\r\n</span><span id=\"Q2_input_1_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q2_input_2_label\"><span id=\"Q2_input_2_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q2_input_2\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\r\n<text> p=2</text>\r\n</span><span id=\"Q2_input_2_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q2_input_3_label\"><span id=\"Q2_input_3_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q2_input_3\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"true\" /><span class=\"choice\">\r\n<text> p=3</text>\r\n</span><span id=\"Q2_input_3_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q2_input_4_label\"><span id=\"Q2_input_4_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q2_input_4\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\r\n<text> p=4</text>\r\n</span><span id=\"Q2_input_4_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div></fieldset>\r\n<div class=\"action\"><button id=\"Q1_button\" onclick=\"checkAnswer({1: 'multiple_choice', 2: 'multiple_choice'})\" class=\"problem_mo_button\">Check</button><button id=\"Q1_button_show\" onclick=\"showHideSolution({1: 'multiple_choice', 2: 'multiple_choice'}, 1, [1, 2])\" class=\"problem_mo_button\">Show Answer</button></div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<div id=\"S2_div\" class=\"problem_solution\" tabindex=\"-1\"><font color=\"blue\">Answer: </font> <font color=\"blue\">Note, with these values, the leading error term is \\(-\\frac{1}{6}{\\Delta t}^4 u_{tttt}^ n\\). Thus, the scheme is third order accurate (\\(p=3\\)). </font></div>\r\n<p>&nbsp;</p>\r\n</div>\r\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy/1690r-definition-of-multi-step-methods');\">Back<span>Definition of Multi-Step Methods</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence');\">Continue<span>Convergence</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "ff3b4491f1d1d23e2d3a939de701c5c2", 
    "title": "1.3 Order of Accuracy", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "ff3b4491f1d1d23e2d3a939de701c5c2", 
            "6121a3cad8f1c65278216a952ea8ff2c", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "58553753c9ccc012f030d87844ae8db5", 
            "a3fbfbbde140393baed5af945a9316f9", 
            "99ee39b779f49afb0849103c798f1c50"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy/1690r-example-of-most-accurate-multi-step-method", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "a3fbfbbde140393baed5af945a9316f9", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:48:57.429 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:41:38.548 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.3.5 Example of Most Accurate Multi-Step Method", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:48:57.471 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:41:38.666 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/07/20 11:10:28.271 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/07/20 11:12:24.536 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:21:33.005 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:24:36.988 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/11 13:23:05.899 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 13:24:22.806 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/11 13:25:27.704 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 13:26:17.777 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/12 10:41:08.725 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/12 10:43:54.448 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 3, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "a3fbfbbde140393baed5af945a9316f9", 
            "58553753c9ccc012f030d87844ae8db5", 
            "6121a3cad8f1c65278216a952ea8ff2c", 
            "ff3b4491f1d1d23e2d3a939de701c5c2", 
            "99ee39b779f49afb0849103c798f1c50", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "09523d4baafc8f4ce9b7d3d67ad30e7b"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy/1690r-example-of-most-accurate-multi-step-method", 
    "is_image_gallery": false, 
    "id": "1690r-example-of-most-accurate-multi-step-method", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/28 10:45:05.689 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.3 Order of Accuracy", 
            "string"
        ]
    ], 
    "_id": "1690r-example-of-most-accurate-multi-step-method", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy/1690r-example-of-most-accurate-multi-step-method"
}