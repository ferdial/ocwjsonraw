{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/487c3b15ab67d7c95cffa6b147049d0c\"><<span>Probabilistic Methods and Optimization</span></a></li><li id=\"flp_btn_1\" class=\"button_selected\"><a href=\"./resolveuid/07ccb107020a5395c3177b0998f3ed49\">3.1.1<span>Measurable Outcomes</span></a></li><li id=\"flp_btn_2\"><a href=\"./resolveuid/3fd6bb8dfeaf382ce1503c80bd79737e\">3.1.2<span>Pre-requisite Material</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/3fd6bb8dfeaf382ce1503c80bd79737e\">><span>Pre-requisite Material</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">3.1.1 Measurable outcomes</h2>\n<p>\nThe objectives of this module are to introduce you, the student, to basic numerical methods used in probabilistic analysis and optimization, and to show you how these techniques are useful in an engineering design setting. </p>\n<p>\nSpecifically, students successfully completing this module will be able to: </p>\n<ul class=\"enumerate\">\n<li>\n<p>Measurable Outcome 3.1: \n Define random variables and how they can be used in mathematical modeling. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.2: \n Define events and outcomes, list the axioms of probability. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.3: \n Describe the process of Monte Carlo sampling from uniform distributions. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.4: \n Describe how to generalize Monte Carlo sampling from uniform distributions to arbitrary univariate distributions. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.5: \n Use Monte Carlo simulation to propagate uncertainty through an ODE or PDE model. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.6: \n Describe what an estimator is. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.7: \n Define the bias and variance of an estimator. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.8: \n State unbiased estimators for mean and variance of a random variable, and for the probability of particular events. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.9: \n Describe the typical convergence rate of Monte Carlo methods. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.10: \n Define the standard error and sampling distribution of an estimator. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.11: \n Give standard errors for sample estimators of mean, variance, and event probability. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.12: \n Obtain confidence intervals for sample estimates of the mean, variance, and event probability. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.13: \n Describe how to apply design of experiments methods, including parameter study, one-at-a-time, Latin hypercube sampling, and orthogonal arrays. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.14: \n Describe the Response Surface Method. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.15: \n Describe the construction of a response surface through Taylor series, design of experiments with least-squares regression, and random sampling with least-squares regression. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.16: \n Describe the R2-metric, its use in measuring the quality of a response surface, and its potential problems. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.17: \n Describe the steepest descent, conjugate gradient, and the Newton method for optimization of multivariate functions, and apply these optimization techniques to simple unconstrained design problems. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.18: \n Describe methods to estimate gradients. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.19: \n Use finite difference approximations to estimate gradients. </p>\n</li>\n<li>\n<p>Measurable Outcome 3.20: \n Interpret sensitivity information and explain its relevance to aerospace design examples. </p>\n</li>\n</ul>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization');\">Back<span>Probabilistic Methods and Optimization</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/overview4/1690r-pre-requisite-material4');\">Continue<span>Pre-requisite Material</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "07ccb107020a5395c3177b0998f3ed49", 
    "title": "3.1 Overview", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "487c3b15ab67d7c95cffa6b147049d0c", 
            "3fd6bb8dfeaf382ce1503c80bd79737e", 
            "07ccb107020a5395c3177b0998f3ed49"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/overview4", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "487c3b15ab67d7c95cffa6b147049d0c", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:17.498 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:08.725 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": false, 
    "bottomtext": "", 
    "short_page_title": "3.1 Overview", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:50:17.552 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:48:09.062 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 0, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "487c3b15ab67d7c95cffa6b147049d0c", 
            "07ccb107020a5395c3177b0998f3ed49", 
            "3fd6bb8dfeaf382ce1503c80bd79737e"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/overview4", 
    "is_image_gallery": false, 
    "id": "overview4", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:39.642 GMT-4", 
    "always_publish": false, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "3.1 Overview", 
            "string"
        ]
    ], 
    "_id": "overview4", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/overview4"
}