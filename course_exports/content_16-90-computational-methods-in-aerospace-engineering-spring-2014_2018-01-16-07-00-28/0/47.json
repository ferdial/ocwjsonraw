{
    "text": "<div class=\"navigation pagination\"><ul> <li id=\"top_bck_btn\"><a href=\"./resolveuid/e8dbfb2204cb6fc4431e0b32e5ea65da\">&lt;<span>Linear Constant Coefficient Systems</span></a></li> <li id=\"flp_btn_1\"><a href=\"./resolveuid/36e637ced6ffe05d36060d537611ad2e\">1.6.1<span>Nonlinear Systems</span></a></li> <li id=\"flp_btn_2\"><a href=\"./resolveuid/e8dbfb2204cb6fc4431e0b32e5ea65da\">1.6.2<span>Linear Constant Coefficient Systems</span></a></li> <li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/04ce95ca3b3acc385d8381923a3dd6fe\">1.6.3<span>Eigenvalue Stability for a Linear ODE</span></a></li> <li id=\"flp_btn_4\"><a href=\"./resolveuid/64bbf17403266a46283d2d2450cf7589\">1.6.4<span>Imaginary Eigenvalues</span></a></li> <li id=\"top_continue_btn\"><a href=\"./resolveuid/64bbf17403266a46283d2d2450cf7589\">&gt;<span>Imaginary Eigenvalues</span></a></li> </ul></div> <!--?xml version='1.0' encoding='utf-8'?--> <h2 class=\"subhead\">1.6.3 Eigenvalue Stability for a Linear ODE</h2> <p id=\"taglist\"><a id=\"analyticalvalidate\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO12\" title=\"MO1.2:  Use analytical solutions to validate numerical solutions of ODE. \">Measurable Outcome 1.2</a>, <a id=\"nonlinear\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO13\" title=\"MO1.3:  Distinguish nonlinear ODEs from linear ODEs. \">Measurable Outcome 1.3</a>, <a id=\"femid\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO16\" title=\"MO1.6:  Describe the Forward Euler methods and the Midpoint methods. \">Measurable Outcome 1.6</a>, <a id=\"zeroeigenstability\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO19\" title=\"MO1.9:  Assess whether a numerical method is zero stable, and whether a numerical method is eigenvalue stable. \">Measurable Outcome 1.9</a>, <a id=\"stabilityboundry\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO118\" title=\"MO1.18:  Determine the stability boundary for a multi-step or multi-stage method applied to a linear system of ODEs. \">Measurable Outcome 1.18</a></p> <p>As we have seen, while numerical methods can be convergent, they can still exhibit instabilities as \\(n\\) increases for finite \\({\\Delta t}\\). For example, when applying the midpoint method to either the ice particle problem in Section&nbsp;<a href=\"/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes/1690r-the-forward-euler-method/\">1.2.4</a> or the simpler model problem in Example&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$f\\ \\=\\ \\-u\\^2\\ \\\\label\\{equ\\:ga\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.66)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.66)');\">1.66</a>, instabilities were seen in both cases as \\(n\\) increased. Similarly, for the nonlinear pendulum problem in Example&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\theta\\ \\_\\{tt\\}\\ \\+\\ \\\\frac\\{g\\}\\{L\\}\\\\sin\\ \\\\theta\\ \\=\\ 0\\.\\ \\\\label\\{equ\\:nonpendulum\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.86)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.86)');\">1.86</a>, the forward Euler method had a growing amplitude again indicating an instability. The key to understanding these results is to analyze the stability for finite \\({\\Delta t}\\). This analysis is different than the stability analysis we performed in Section&nbsp;<a href=\"/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/zero-stability-and-the-dahlquist-equivalence-theorem/1690r-stability/\">1.5.2</a> since that analysis was for the limit of \\({\\Delta t}\\rightarrow 0\\).</p> <p>Suppose we are interested in solving the linear ODE,</p> <table id=\"a0000000085\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u_ t = \\lambda u.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.99)</td> </tr> </tbody> </table> <p>Consider the Forward Euler method applied to this problem,</p> <table id=\"&lt;plasTeX.TeXFragment object at 0x1048da460&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[v^{n+1} = v^ n + \\lambda {\\Delta t}v^ n. \\label{equ:fe_ lin}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.100)</td> </tr> </tbody> </table> <p>Similar to the zero stability analysis, we will assume that the solution has the following form,</p> <table id=\"equ:gdef\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[v^{n} = g^ n v^0, \\label{equ:gdef}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.101)</td> </tr> </tbody> </table> <p>where \\(g\\) is the amplification factor (and the superscript \\(n\\) acting on \\(g\\) is again raising to a power). As in the zero stability analysis, we wish to determine under what conditions \\(|g| &gt; 1\\) since this would mean that \\(v^ n\\) will grow unbounded as \\(n \\rightarrow \\infty\\). Substituting Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$v\\^\\{n\\}\\ \\=\\ g\\^\\ n\\ v\\^0\\,\\ \\\\label\\{equ\\:gdef\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.101)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.101)');\">1.101</a> into Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$v\\^\\{n\\+1\\}\\ \\=\\ v\\^\\ n\\ \\+\\ \\\\lambda\\ \\{\\\\Delta\\ t\\}v\\^\\ n\\.\\ \\\\label\\{equ\\:fe\\_\\ lin\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.100)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.100)');\">1.100</a> gives,</p> <table id=\"a0000000086\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[g^{n+1} = (1 + \\lambda {\\Delta t})g^ n.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.102)</td> </tr> </tbody> </table> <p>Thus, the only non-zero root of this equation gives,</p> <table id=\"a0000000087\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[g = 1 + \\lambda {\\Delta t},\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.103)</td> </tr> </tbody> </table> <p>which is the amplification factor for the forward Euler method. Now, we must determine what values of \\(\\lambda {\\Delta t}\\) lead to instability (or stability). A simple way to do this for multi-step methods is to solve for the stability boundary for which \\(|g| = 1\\). To do this, let \\(g = e^{i\\theta }\\) (since \\(|e^{i\\theta }| = 1\\)) where \\(\\theta = [0,2\\pi ]\\). Making this substitution into the amplification factor,</p> <table id=\"a0000000088\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[e^{i\\theta } = 1 + \\lambda {\\Delta t}\\quad \\Rightarrow \\quad \\lambda {\\Delta t}= e^{i\\theta } - 1.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.104)</td> </tr> </tbody> </table> <p>Thus, the stability boundary for the forward Euler method lies on a circle of radius one centered at -1 along the real axis and is shown in Figure&nbsp;<a href=\"./resolveuid/ffa99b42f4380a3320c9f53791a67819\" onclick=\"window.open(this.href,'16.90r','width=198,height=196','toolbar=1'); return false;\">1.10</a>.</p> <div id=\"fig:fe_stab\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/ffa99b42f4380a3320c9f53791a67819\" alt=\"This graph demonstrates the stability boundary for the forward Euler method, which lies on a circle of radius one centered at -1 along the real axis.\" width=\"397px\" height=\"392px\" /> </center></div> <div class=\"caption\"><b>Figure 1.10</b>: <span>Forward Euler stability region</span></div> <p>For a given problem, i.e. with a given \\(\\lambda\\), the timestep must be chosen so that the algorithm remains stable for \\(n \\rightarrow \\infty\\). Let's consider some examples.</p> <h2 class=\"subhead\">Example</h2> <p>Let's return to the previous example, \\(u_ t = -u^2\\) with \\(u(0) = 1\\). To determine the timestep restrictions, we must estimate the eigenvalue for this problem. Linearizing this problem about a known state gives the eigenvalue as \\(\\lambda = {\\partial f}/{\\partial u} = -2u\\). Since the solution will decay from the initial condition (since \\(u_ t &lt; 0\\) because \\(-u^2 &lt; 0\\)), the largest magnitude of the eigenvalue occurs at the initial condition when \\(u(0) = 1\\) and thus, \\(\\lambda = -2\\). Since this eigenvalue is a negative real number, the maximum \\({\\Delta t}\\) will occur at the maximum extent of the stability region along the negative real axis. Since this occurs when \\(\\lambda {\\Delta t}= -2\\), this implies that \\({\\Delta t}&lt; 1\\). To test the validity of this analysis, the forward Euler method was run for \\({\\Delta t}= 0.9\\) and \\({\\Delta t}= 1.1\\). The results are shown in Figure&nbsp;<a href=\"./resolveuid/9f9bf44b75d4cda7ee127e6196a405a1\" onclick=\"window.open(this.href,'16.90r','width=247,height=195','toolbar=1'); return false;\">1.11</a> which are stable for \\({\\Delta t}= 0.9\\) but are unstable for \\({\\Delta t}= 1.1\\).</p> <div id=\"fig:ga_fe_stab\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/9f9bf44b75d4cda7ee127e6196a405a1\" alt=\"This figure shows two line graphs of forward Euler solutions used to determine the timestep restrictions, one that shows stability for &Delta;t=0.9 and the other that is unstable for &Delta;t=1.1.\" width=\"495px\" height=\"390px\" /> </center></div> <div class=\"caption\"><b>Figure 1.11</b>: <span>Forward Euler solution for \\(u_ t = -u^2\\) with \\(u(0) = 1\\) with \\({\\Delta t}= 0.9\\) and \\(1.1\\).</span></div> <h2 class=\"subhead\">Pendulum Example</h2> <p>Next, let's consider the application of the forward Euler method to the pendulum problem. For this case, the linearization produces a matrix,</p> <table id=\"a0000000089\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{\\partial f}{\\partial u} = \\left(\\begin{array}{cc} 0 &amp;  -\\frac{g}{L}\\cos \\theta \\\\ 1 &amp;  0 \\end{array}\\right)\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.105)</td> </tr> </tbody> </table> <p>The eigenvalues can be found from the roots of the determinant of \\({\\partial f}/{\\partial u} - \\lambda I\\):</p> <table id=\"a0000000090\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr id=\"a0000000091\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\det \\left(\\frac{\\partial f}{\\partial u} - \\lambda I\\right)\\)</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\det \\left(\\begin{array}{cc} -\\lambda &amp;  -\\frac{g}{L}\\cos \\theta \\\\ 1 &amp;  -\\lambda \\end{array}\\right)\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.106)</td> </tr> <tr id=\"a0000000092\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\lambda ^2 + \\frac{g}{L}\\cos \\theta = 0\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.107)</td> </tr> <tr id=\"a0000000093\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  \\Rightarrow\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\lambda = \\pm i \\sqrt {\\frac{g}{L}\\cos \\theta }\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.108)</td> </tr> </tbody> </table> <p>Thus, we see that the eigenvalues will always be imaginary for this problem. As a result, since the forward Euler stability region does not contain any part of the imaginary axis (except the origin), no finite timestep exists which will be stable. This explains why the amplitude increases for the pendulum simulations in Figure&nbsp;<a href=\"./resolveuid/dc33ef954ba00d2fd80b8e3cff99322b\" onclick=\"window.open(this.href,'16.90r','width=251,height=195','toolbar=1'); return false;\">1.8</a>.</p> <div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-linear-constant-coefficient-systems');\">Back<span>Linear Constant Coefficient Systems</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-imaginary-eigenvalues');\">Continue<span>Imaginary Eigenvalues</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "04ce95ca3b3acc385d8381923a3dd6fe", 
    "title": "1.6 Systems of ODE's and Eigenvalue Stability", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "04ce95ca3b3acc385d8381923a3dd6fe", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "64bbf17403266a46283d2d2450cf7589", 
            "36e637ced6ffe05d36060d537611ad2e"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-eigenvalue-stability-for-a-linear-ode", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "36e637ced6ffe05d36060d537611ad2e", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:06.673 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:42:58.702 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.6.3 Eigenvalue Stability for a Linear ODE", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:49:06.714 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:43:00.777 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2015/12/07 10:26:02.180 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/12/08 07:48:44.242 US/Eastern"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/12/08 08:32:00.473 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/12/08 08:32:03.686 US/Eastern"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/12/08 08:37:40.980 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/12/08 08:37:57.826 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/01/13 13:37:19.300 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/01/14 06:43:18.674 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/04/28 13:38:46.396 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2016/05/25 06:14:12.210 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 3, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "9b1b577d12e2e60d75d3f8fb6ed609d5", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "36e637ced6ffe05d36060d537611ad2e", 
            "04ce95ca3b3acc385d8381923a3dd6fe", 
            "ffa99b42f4380a3320c9f53791a67819", 
            "9f9bf44b75d4cda7ee127e6196a405a1", 
            "69a13333afb590eed339c7fba31529fd", 
            "e8dbfb2204cb6fc4431e0b32e5ea65da", 
            "dc33ef954ba00d2fd80b8e3cff99322b", 
            "64bbf17403266a46283d2d2450cf7589"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-eigenvalue-stability-for-a-linear-ode", 
    "is_image_gallery": false, 
    "id": "1690r-eigenvalue-stability-for-a-linear-ode", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:13.634 US/Eastern", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.6 Systems of ODE's and Eigenvalue Stability", 
            "string"
        ]
    ], 
    "_id": "1690r-eigenvalue-stability-for-a-linear-ode", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-eigenvalue-stability-for-a-linear-ode"
}