{
    "text": "<div class=\"navigation pagination\">\n<ul>\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/16176028e8695612f636568d503833fc\">&lt;<span>Reference Element and Linear Elements</span></a></li>\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/c782bcc9abb3f6bcc638027dfffdc386\">2.11.1<span>Overview</span></a></li>\n<li id=\"flp_btn_2\"><a href=\"./resolveuid/16176028e8695612f636568d503833fc\">2.11.2<span>Reference Element and Linear Elements</span></a></li>\n<li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/6075ecc5d133cbcb277c06977e6970ea\">2.11.3<span>Differentiation using the Reference Element</span></a></li>\n<li id=\"flp_btn_4\"><a href=\"./resolveuid/d29ce1ed362660b8be9bf2741bbc6ee9\">2.11.4<span>Construction of the Stiffness Matrix</span></a></li>\n<li id=\"flp_btn_5\"><a href=\"./resolveuid/cd2d971fe847d266f0b1555caab639d4\">2.11.5<span>Integration in the Reference Element</span></a></li>\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/d29ce1ed362660b8be9bf2741bbc6ee9\">&gt;<span>Construction of the Stiffness Matrix</span></a></li>\n</ul>\n</div>\n<!--?xml version='1.0' encoding='utf-8'?-->\n<h2 class=\"subhead\">2.11.3 Differentiation using the Reference Element</h2>\n<p id=\"taglist\"><a id=\"integRef\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO217\" title=\"MO2.17:  Describe how integrals are performed using a reference element. \">Measurable Outcome 2.17</a></p>\n<p>To find the derivative of \\(\\tilde{T}\\) with respect to \\(x\\) (or similarly \\(y\\)) within an element, we differentiate the three nodal basis functions within the element:</p>\n<table id=\"a0000000400\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr id=\"a0000000401\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\tilde{T}_ x\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\frac{\\partial }{\\partial x}\\left(\\sum _{j=1}^{3} a_ j\\phi _ j\\right),\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.271)</td>\n</tr>\n<tr id=\"a0000000402\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\sum _{j=1}^{3} a_ j\\frac{\\partial \\phi _ j}{\\partial x}.\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.272)</td>\n</tr>\n</tbody>\n</table>\n<p>To find the \\(x\\)-derivatives of each of the \\(\\phi _ j\\)'s, the chain rule is applied:</p>\n<table id=\"a0000000403\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{\\partial \\phi _ j}{\\partial x} = \\frac{\\partial \\phi _ j}{\\partial \\xi _1}\\frac{\\partial \\xi _1}{\\partial x} + \\frac{\\partial \\phi _ j}{\\partial \\xi _2}\\frac{\\partial \\xi _2}{\\partial x}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.273)</td>\n</tr>\n</tbody>\n</table>\n<p>Similarly, to find the derivatives with respect to \\(y\\):</p>\n<table id=\"a0000000404\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{\\partial \\phi _ j}{\\partial y} = \\frac{\\partial \\phi _ j}{\\partial \\xi _1}\\frac{\\partial \\xi _1}{\\partial y} + \\frac{\\partial \\phi _ j}{\\partial \\xi _2}\\frac{\\partial \\xi _2}{\\partial y}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.274)</td>\n</tr>\n</tbody>\n</table>\n<p>The calculation of the derivatives of \\(\\phi _ j\\) with respect to the \\(\\xi\\) variables gives:</p>\n<table id=\"a0000000405\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr id=\"a0000000406\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\frac{\\partial \\phi _1}{\\partial \\xi _1}\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  -1, \\qquad \\frac{\\partial \\phi _1}{\\partial \\xi _2} = -1,\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.275)</td>\n</tr>\n<tr id=\"a0000000407\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle \\frac{\\partial \\phi _2}{\\partial \\xi _1}\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  1, \\qquad \\frac{\\partial \\phi _2}{\\partial \\xi _2} = 0,\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.276)</td>\n</tr>\n<tr id=\"a0000000408\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle \\frac{\\partial \\phi _3}{\\partial \\xi _1}\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  0, \\qquad \\frac{\\partial \\phi _3}{\\partial \\xi _2} = 1.\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.277)</td>\n</tr>\n</tbody>\n</table>\n<p>The only remaining terms are the calculation of \\(\\frac{\\partial \\xi _1}{\\partial x}\\), \\(\\frac{\\partial \\xi _2}{\\partial x}\\), etc. which can be found by differentiating Equation&nbsp;(<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\left\\(\\\\begin\\{array\\}\\{c\\}\\ \\\\xi\\ \\_1\\ \\\\\\\\\\ \\\\xi\\ \\_2\\ \\\\end\\{array\\}\\\\right\\)\\ \\=\\ \\\\left\\(\\\\begin\\{array\\}\\{cc\\}\\ x\\_2\\-x\\_1\\ \\&amp;amp\\;\\ \\ x\\_3\\-x\\_1\\ \\\\\\\\\\ y\\_2\\-y\\_1\\ \\&amp;amp\\;\\ \\ y\\_3\\-y\\_1\\\\end\\{array\\}\\\\right\\)\\^\\{\\-1\\}\\ \\\\left\\(\\\\begin\\{array\\}\\{c\\}\\ x\\-x\\_1\\ \\\\\\\\\\ y\\-y\\_1\\ \\\\end\\{array\\}\\\\right\\)\\ \\\\label\\{equ\\:x\\_\\ to\\_\\ xi\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.270)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.270)');\">2.270</a>),</p>\n<table id=\"a0000000409\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr id=\"a0000000410\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  \\frac{\\partial \\vec{\\xi }}{\\partial \\vec{x}}\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\left(\\begin{array}{cc} x_2-x_1 &amp;  x_3-x_1 \\\\ y_2-y_1 &amp;  y_3-y_1\\end{array}\\right)^{-1},\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.278)</td>\n</tr>\n<tr id=\"a0000000411\">\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">&nbsp;</td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\frac{1}{J} \\left(\\begin{array}{cc} y_3-y_1 &amp;  -(x_3-x_1) \\\\ -(y_2-y_1) &amp;  x_2-x_1\\end{array}\\right),\\)</td>\n<td style=\"width:40%;border-style:hidden\">&nbsp;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.279)</td>\n</tr>\n</tbody>\n</table>\n<p>where</p>\n<table id=\"a0000000412\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[J = (x_2-x_1)(y_3-y_1) - (x_3-x_1)(y_2-y_1).\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.280)</td>\n</tr>\n</tbody>\n</table>\n<p>Note that the Jacobian, \\(J\\), is equal to twice the area of the triangular element.</p>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion/1690r-reference-element-and-linear-elements');\">Back<span>Reference Element and Linear Elements</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion/1690r-construction-of-the-stiffness-matrix');\">Continue<span>Construction of the Stiffness Matrix</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "6075ecc5d133cbcb277c06977e6970ea", 
    "title": "2.11 The Finite Element Method for Two-Dimensional Diffusion", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "d29ce1ed362660b8be9bf2741bbc6ee9", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "16176028e8695612f636568d503833fc", 
            "6075ecc5d133cbcb277c06977e6970ea", 
            "c782bcc9abb3f6bcc638027dfffdc386"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion/1690r-differentiation-using-the-reference-element", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "c782bcc9abb3f6bcc638027dfffdc386", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:15.892 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:17.524 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.11.3 Differentiation using the Reference Element", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:50:15.939 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:48:18.764 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:36:23.216 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:37:04.717 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 1, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "16176028e8695612f636568d503833fc", 
            "c782bcc9abb3f6bcc638027dfffdc386", 
            "cd2d971fe847d266f0b1555caab639d4", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "6075ecc5d133cbcb277c06977e6970ea", 
            "d29ce1ed362660b8be9bf2741bbc6ee9"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion/1690r-differentiation-using-the-reference-element", 
    "is_image_gallery": false, 
    "id": "1690r-differentiation-using-the-reference-element", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:21.311 US/Eastern", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.11 The Finite Element Method for Two-Dimensional Diffusion", 
            "string"
        ]
    ], 
    "_id": "1690r-differentiation-using-the-reference-element", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion/1690r-differentiation-using-the-reference-element"
}