{
    "text": "<div class=\"navigation pagination\">\n<ul>\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/767b5c964bd2394b92daca9fa25f2e1e\">&lt;<span>Finite Volume Method Applied to 1-D Convection</span></a></li>\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/3d8df8b822917094b5a69893808a75cc\">2.5.1<span>Finite Volume Method in 1-D</span></a></li>\n<li id=\"flp_btn_2\"><a href=\"./resolveuid/767b5c964bd2394b92daca9fa25f2e1e\">2.5.2<span>Finite Volume Method Applied to 1-D Convection</span></a></li>\n<li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/d4283096140199d20c85a833f3518826\">2.5.3<span>Finite Volume Method in 2-D</span></a></li>\n<li id=\"flp_btn_4\"><a href=\"./resolveuid/a9d8dcc7e873f01e6dc36f07884b2f23\">2.5.4<span>Finite Volume Method for 2-D Convection on a Rectangular Mesh</span></a></li>\n<li id=\"flp_btn_5\"><a href=\"./resolveuid/eaeacad2dafd93833c0e0227dade769c\">2.5.5<span>Finite Volume Method for Nonlinear Systems</span></a></li>\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/a9d8dcc7e873f01e6dc36f07884b2f23\">&gt;<span>Finite Volume Method for 2-D Convection on a Rectangular Mesh</span></a></li>\n</ul>\n</div>\n<!--?xml version='1.0' encoding='utf-8'?-->\n<h2 class=\"subhead\">2.5.3 Finite Volume Method in 2-D</h2>\n<p id=\"taglist\"><a id=\"conservation\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO21\" title=\"MO2.1:  Identify whether a PDE is in the form of a conservation law, describe the characteristic of a conservation law and how the solution behaves along the characteristic. \">Measurable Outcome 2.1</a>, <a id=\"implement\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO23\" title=\"MO2.3:  Implement a finite difference or finite volume discretization to solve a representative PDE (or set of PDEs) from an engineering application. \">Measurable Outcome 2.3</a>, <a id=\"godunov\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO24\" title=\"MO2.4:  Describe finite volume discretization of two-dimensional convection on an unstructured mesh. \">Measurable Outcome 2.4</a></p>\n<p>The finite volume discretization can be extended to two-dimensional problems. Suppose the physical domain is divided into a set of triangular control volumes, as shown in Figure&nbsp;<a href=\"./resolveuid/46b2c91affe18bdabc3c9637e97f1477\" onclick=\"window.open(this.href,'16.90r','width=88,height=85','toolbar=1'); return false;\">2.15</a>.</p>\n<div id=\"fig:triangle_cv\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/46b2c91affe18bdabc3c9637e97f1477\" alt=\"This schematic shows a finite volume discretization extended over two dimensions by dividing the physical domain into a set of four triangles.\" width=\"177px\" height=\"171px\" /> </center></div>\n<div class=\"caption\"><b>Figure 2.15</b>: <span>Triangular mesh and notation for finite volume method.</span></div>\n<p>\n</p>\n<p>Application of Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\frac\\{\\{\\\\rm\\ d\\}\\}\\{\\{\\\\rm\\ d\\}t\\}\\\\int\\ \\_\\{\\\\Omega\\ \\}\\ U\\\\\\,\\ dA\\ \\+\\ \\\\int\\ \\_\\{\\\\delta\\ \\\\Omega\\ \\}\\ \\\\vec\\{F\\}\\(U\\)\\ \\\\cdot\\ \\\\vec\\{n\\}\\\\\\,\\ ds\\ \\=\\ \\\\int\\ \\_\\{\\\\Omega\\ \\}\\ S\\(U\\,t\\)\\\\\\,\\ dA\\,\\ \\\\label\\{equ\\:conservation\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.1)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.1)');\">2.1</a> to control volume A gives,</p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x103468188&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{{\\rm d}}{{\\rm d}t}\\int _{\\Omega _ A} U\\, dA + \\int _{\\delta \\Omega _ A} H(U,\\vec{n})\\, ds = \\int _{\\Omega _ A} S(U,t)\\, dA, \\label{equ:2d_ cv_ temp}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.109)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(\\Omega _ A\\) is the interior and \\(\\delta \\Omega _ A\\) is the boundary of control volume A. \\(H(U,\\vec{n})\\) is the flux normal to the face,</p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x103468598&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[H(U,\\vec{n}) \\equiv \\left[F(U)\\vec{i} + G(U)\\vec{j}\\right]\\cdot \\vec{n}. \\label{equ:H_ def}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.110)</td>\n</tr>\n</tbody>\n</table>\n<p>As in the one-dimensional case, we define the cell average,</p>\n<table id=\"a0000000247\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[U_ A \\equiv \\frac{1}{A_ A} \\int _{\\Omega _ A} U\\, dA,\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.111)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(A_ A\\) is the area of control volume \\(A\\). Thus, Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\frac\\{\\{\\\\rm\\ d\\}\\}\\{\\{\\\\rm\\ d\\}t\\}\\\\int\\ \\_\\{\\\\Omega\\ \\_\\ A\\}\\ U\\\\\\,\\ dA\\ \\+\\ \\\\int\\ \\_\\{\\\\delta\\ \\\\Omega\\ \\_\\ A\\}\\ H\\(U\\,\\\\vec\\{n\\}\\)\\\\\\,\\ ds\\ \\=\\ \\\\int\\ \\_\\{\\\\Omega\\ \\_\\ A\\}\\ S\\(U\\,t\\)\\\\\\,\\ dA\\,\\ \\\\label\\{equ\\:2d\\_\\ cv\\_\\ temp\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.109)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.109)');\">2.109</a> becomes,</p>\n<table id=\"a0000000248\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[A_ A\\frac{{\\rm d}U_ A}{{\\rm d}t} + \\int _{\\delta \\Omega _ A} H(U,\\vec{n})\\, ds = \\int _{\\Omega _ A} S(U,t)\\, dA.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.112)</td>\n</tr>\n</tbody>\n</table>\n<p>In the case of convection, we again assume \\(S=0\\). Also, we expand the surface integral into the contributions for the three edges,</p>\n<table id=\"a0000000249\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[A_ A\\frac{{\\rm d}U_ A}{{\\rm d}t} + \\int _{1}^{2} H(U,\\vec{n}_{AB} )\\, ds + \\int _{2}^{3} H(U,\\vec{n}_{AC} )\\, ds + \\int _{3}^{1} H(U,\\vec{n}_{AD} )\\, ds = 0,\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.113)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(\\vec{n}_{AB}\\) is the unit normal pointing from cell A to cell B, and similarly for \\(\\vec{n}_{AC}\\) and \\(\\vec{n}_{AD}\\).</p>\n<p>As in one-dimensional case, we assume that the solution everywhere in the control volume is equal to the cell average value. Finally, the flux at each interface is determined by the &lsquo;upwind' value using the velocity component normal to the face. For example, at the interface between cell A and B,</p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x103471258&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[H(U,\\vec{n}_{AB}) \\approx \\hat{H}(U_ A, U_ B, \\vec{n}_{AB}) \\equiv \\frac{1}{2}\\vec{u}_{AB}\\cdot \\vec{n}_{AB}\\left(U_ B + U_ A\\right) - \\frac{1}{2}|\\vec{u}_{AB}\\cdot \\vec{n}_{AB}|\\left(U_ B - U_ A\\right), \\label{equ:upwindflux_ convection2d}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.114)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(\\vec{u}_{AB}\\) is the velocity between the control volumes. Thus, when \\(\\vec{u}_{AB}\\cdot \\vec{n}_{AB} &gt; 0\\), the flux is determined by the state from cell A, i.e. \\(U_ A\\). Likewise, when \\(\\vec{u}_{AB}\\cdot \\vec{n}_{AB} &lt; 0\\), the flux is determined by the state from cell B, i.e. \\(U_ B\\). The velocity, \\(\\vec{u}_{AB}\\) is usually approximated as the velocity at the midpoint of the edge (note: \\(\\vec{u}\\) can be a function of \\(\\vec{x}\\) in two-dimensions even though the velocity is assumed to be divergence free, i.e. \\({\\partial u}/{\\partial x} + {\\partial v}/{\\partial y} = 0\\)). We use the notation \\(\\hat{H}\\) to indicate that the flux is an approximation to the true flux when \\(\\vec{u}\\) is not constant. Thus, the finite volume algorithm prior to time discretization would be given by,</p>\n<table id=\"a0000000250\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[A_ A\\frac{{\\rm d}U_ A}{{\\rm d}t} + \\hat{H}(U_ A,U_ B,\\vec{n}_{AB} )\\Delta s_{AB} + \\hat{H}(U_ A,U_ C,\\vec{n}_{AC} )\\Delta s_{AC} + \\hat{H}(U_ A,U_ D,\\vec{n}_{AD} )\\Delta s_{AD} = 0.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.115)</td>\n</tr>\n</tbody>\n</table>\n<p>The final step is to integrate in time. As in the one-dimensional case, we might use a forward Euler algorithm which would result in the final fully discrete finite volume method,</p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x103492188&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[A_ A\\frac{U_ A^{n+1} - U_ A^ n}{{\\Delta t}} + \\hat{H}(U_ A^ n,U_ B^ n,\\vec{n}_{AB} )\\Delta s_{AB} + \\hat{H}(U_ A^ n,U_ C^ n,\\vec{n}_{AC} )\\Delta s_{AC} + \\hat{H}(U_ A^ n,U_ D^ n,\\vec{n}_{AD} )\\Delta s_{AD} = 0. \\label{equ:conservation_2d_ FVM}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.116)</td>\n</tr>\n</tbody>\n</table>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-applied-to-1-d-convection');\">Back<span>Finite Volume Method Applied to 1-D Convection</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-for-2-d-convection-on-a-rectangular-mesh');\">Continue<span>Finite Volume Method for 2-D Convection on a Rectangular Mesh</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "d4283096140199d20c85a833f3518826", 
    "title": "2.5 Introduction to Finite Volume Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "d4283096140199d20c85a833f3518826", 
            "eaeacad2dafd93833c0e0227dade769c", 
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "767b5c964bd2394b92daca9fa25f2e1e", 
            "a9d8dcc7e873f01e6dc36f07884b2f23", 
            "3d8df8b822917094b5a69893808a75cc"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-in-2-d", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "3d8df8b822917094b5a69893808a75cc", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:36.930 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:46:02.980 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.5.3 Finite Volume Method in 2-D", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:49:36.979 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:46:03.104 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:16:20.445 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/01/13 14:17:17.263 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:01:28.510 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:02:49.338 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 3, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "767b5c964bd2394b92daca9fa25f2e1e", 
            "3d8df8b822917094b5a69893808a75cc", 
            "a9d8dcc7e873f01e6dc36f07884b2f23", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "eaeacad2dafd93833c0e0227dade769c", 
            "d4283096140199d20c85a833f3518826", 
            "46b2c91affe18bdabc3c9637e97f1477"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-in-2-d", 
    "is_image_gallery": false, 
    "id": "1690r-finite-volume-method-in-2-d", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:18.032 US/Eastern", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.5 Introduction to Finite Volume Methods", 
            "string"
        ]
    ], 
    "_id": "1690r-finite-volume-method-in-2-d", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-in-2-d"
}