{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/84564160240cf3bee329df42818d2eaa\"><<span>Apply Newton-Rhapson</span></a></li><li id=\"flp_btn_1\" class=\"button_selected\"><a href=\"./resolveuid/67717326dffb74445162101fd9a9ec91\">1.8.1<span>Adams-Bashforth Methods</span></a></li><li id=\"flp_btn_2\"><a href=\"./resolveuid/75e73977a306f5532134b4e0a24fdb81\">1.8.2<span>Adams-Moulton Methods</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/998bd38300b68dbd2d38c251f8262e37\">1.8.3<span>Backwards Differentiation Methods</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/f02cecfe1cd48c17eeff25be1aaa895d\">1.8.4<span>Backwards Differentiation Excercise</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/75e73977a306f5532134b4e0a24fdb81\">><span>Adams-Moulton Methods</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\r\n<h2 class=\"subhead\">1.8.1 Adams-Bashforth Methods</h2>\r\n<p id=\"taglist\">\r\n<a id=\"multistep\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO115\" title=\"MO1.15:  Describe multi-step methods, including the Adams-Bashforth, Adams-Moulton, and backwards differentiation families. \">Measurable Outcome 1.15</a>, <a id=\"mstepvmstage\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO117\" title=\"MO1.17:  Explain the relative computational costs of multi-step versus multi-stage methods. \">Measurable Outcome 1.17</a>, <a id=\"stabilityboundry\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO118\" title=\"MO1.18:  Determine the stability boundary for a multi-step or multi-stage method applied to a linear system of ODEs. \">Measurable Outcome 1.18</a>, <a id=\"odeinteg\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO119\" title=\"MO1.19:  Recommend an appropriate ODE integration method based on the features of the problem being solved. \">Measurable Outcome 1.19</a>\r\n</p>\r\n<p>\r\nAdams-Bashforth methods are explicit methods of the form, </p>\r\n<table id=\"a0000000118\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\r\n<tr>\r\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[v^{n+1} - v^{n} = {\\Delta t}\\sum _{i=1}^ s \\beta _ i f^{n+1-i}.\\]</td>\r\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.136)</td>\r\n</tr>\r\n</table>\r\n<p>\r\n Thus, the basic time derivative approximation remains the same for all \\(p\\) (i.e. \\(du/dt\\) is approximated by \\((v^{n+1} - v^ n)/Dt\\)) and the higher-order accuracy is achieved by using more values of \\(f\\). </p>\r\n<div id=\"tab:ab_app\" class=\"table\">\r\n<center style=\"border-style:hidden\">\r\n<table cellspacing=\"0\" class=\"tabular\" style=\"table-layout:auto;border-style:hidden\">\r\n<tr>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(p\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\beta _1\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\beta _2\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\beta _3\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\beta _4\\) </p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">\r\n<p>\r\n1 </p>\r\n</td>\r\n<td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">\r\n<p>\r\n 1 </p>\r\n</td>\r\n<td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">&#xA0;</td>\r\n<td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">&#xA0;</td>\r\n<td style=\"border-top-style:solid; border-top-color:black; border-top-width:1px; text-align:center;border-style:hidden\">&#xA0;</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n 2 </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\frac{3}{2}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(-\\frac{1}{2}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">&#xA0;</td>\r\n<td style=\"text-align:center;border-style:hidden\">&#xA0;</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n 3 </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\frac{23}{12}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(-\\frac{16}{12}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\frac{5}{12}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">&#xA0;</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n 4 </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\frac{55}{24}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(-\\frac{59}{24}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(\\frac{37}{24}\\) </p>\r\n</td>\r\n<td style=\"text-align:center;border-style:hidden\">\r\n<p>\r\n \\(-\\frac{9}{24}\\) </p>\r\n</td>\r\n</tr>\r\n</table>\r\n</center></div><div class=\"caption\"><b>Table 2</b>: <span>Coefficients for Adams-Bashforth methods (these methods are explicit so \\(\\beta _0 = 0\\)). Note: the \\(p=1\\) method is the forward Euler method.</span></div>\r\n<p>\r\n</p>\r\n<p>\r\nThe coefficients for the first through fourth order methods are given in the table above. The first-order Adams-Bashforth is forward Euler. </p>\r\n<div id=\"fig:ab_stab\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\">\r\n<center style=\"border-style:hidden\">\r\n<img src=\"./resolveuid/994a70be6287f90b9807211940376999\" alt=\"The graph shows the shrinking, but all overlapping, Adams-Bashforth stability regions for p=1 through p=4 methods.\" width=\"495px\" height=\"393px\" />\r\n</center></div><div class=\"caption\"><b>Figure 1.19</b>: <span>Adams-Bashforth stability regions for \\(p=1\\) through \\(p=4\\) methods. Note: interior of contours is stable region.</span></div>\r\n<p>\r\n</p>\r\n<p>\r\nThe stability boundary for these methods are shown in Figure&#xA0;<a href=\"./resolveuid/994a70be6287f90b9807211940376999\" onclick=\"window.open(this.href,'16.90r','width=248,height=195','toolbar=1'); return false;\">1.19</a>. As the order of accuracy increases, the stability regions become smaller. Note, this is the opposite of Runge-Kutta methods for which the size of the stability regions increases with increased accuracy (see Section&#xA0;<a href=\"./resolveuid/c5e7e539a82c8e620c8ae738a18f6f10\">1.9</a>). </p>\r\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/stiffness-and-implicit-methods/1690r-apply-newton-rhapson');\">Back<span>Apply Newton-Rhapson</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods/1690r-adams-moulton-methods');\">Continue<span>Adams-Moulton Methods</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "67717326dffb74445162101fd9a9ec91", 
    "title": "1.8 Multi-Step Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "f02cecfe1cd48c17eeff25be1aaa895d", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "998bd38300b68dbd2d38c251f8262e37", 
            "84564160240cf3bee329df42818d2eaa", 
            "75e73977a306f5532134b4e0a24fdb81", 
            "67717326dffb74445162101fd9a9ec91"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "5cae4847c59aa247c7673c0c6b0abef1", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:13.671 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:43:23.989 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.8 Multi-Step Methods", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:49:13.728 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:43:30.867 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:34:15.936 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:24:59.064 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 7, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "c5e7e539a82c8e620c8ae738a18f6f10", 
            "84564160240cf3bee329df42818d2eaa", 
            "67717326dffb74445162101fd9a9ec91", 
            "994a70be6287f90b9807211940376999", 
            "75e73977a306f5532134b4e0a24fdb81", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "998bd38300b68dbd2d38c251f8262e37", 
            "f02cecfe1cd48c17eeff25be1aaa895d"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods", 
    "is_image_gallery": false, 
    "id": "multi-step-methods", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/27 10:26:50.158 GMT-4", 
    "always_publish": false, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.8 Multi-Step Methods", 
            "string"
        ]
    ], 
    "_id": "multi-step-methods", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/multi-step-methods"
}