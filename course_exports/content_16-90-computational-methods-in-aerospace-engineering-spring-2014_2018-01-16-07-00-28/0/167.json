{
    "text": "<div class=\"navigation pagination\">\n<ul>\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/365c70a74666ed1cd1408aeb96bff4a6\">&lt;<span>Boundary Conditions for Finite Elements</span></a></li>\n<li id=\"flp_btn_1\" class=\"button_selected\"><a href=\"./resolveuid/c782bcc9abb3f6bcc638027dfffdc386\">2.11.1<span>Overview</span></a></li>\n<li id=\"flp_btn_2\"><a href=\"./resolveuid/16176028e8695612f636568d503833fc\">2.11.2<span>Reference Element and Linear Elements</span></a></li>\n<li id=\"flp_btn_3\"><a href=\"./resolveuid/6075ecc5d133cbcb277c06977e6970ea\">2.11.3<span>Differentiation using the Reference Element</span></a></li>\n<li id=\"flp_btn_4\"><a href=\"./resolveuid/d29ce1ed362660b8be9bf2741bbc6ee9\">2.11.4<span>Construction of the Stiffness Matrix</span></a></li>\n<li id=\"flp_btn_5\"><a href=\"./resolveuid/cd2d971fe847d266f0b1555caab639d4\">2.11.5<span>Integration in the Reference Element</span></a></li>\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/16176028e8695612f636568d503833fc\">&gt;<span>Reference Element and Linear Elements</span></a></li>\n</ul>\n</div>\n<!--?xml version='1.0' encoding='utf-8'?-->\n<h2 class=\"subhead\">2.11.1 Overview</h2>\n<p>In this lecture, we will consider the finite element approximation of the two-dimensional diffusion problem,</p>\n<table id=\"equ:dif2d\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\nabla \\cdot \\left(k \\nabla T\\right) + f = 0. \\label{equ:dif2d}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.259)</td>\n</tr>\n</tbody>\n</table>\n<p>As in the previous discussion of the method of weighted residuals and the finite element method, the approximate solution will have the form</p>\n<table id=\"a0000000394\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\tilde{T}(x,y) = \\sum _{j=1}^{N} a_ j \\phi _ j(x,y),\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.260)</td>\n</tr>\n</tbody>\n</table>\n<p>where \\(\\phi _ j(x,y)\\) are the known basis functions and the \\(a_ j\\) are the unknown coefficients to be determined for the specific problem. Following the Galerkin method of weighted residuals, we will weight Equation&nbsp;(<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\nabla\\ \\\\cdot\\ \\\\left\\(k\\ \\\\nabla\\ T\\\\right\\)\\ \\+\\ f\\ \\=\\ 0\\.\\ \\\\label\\{equ\\:dif2d\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.259)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.259)');\">2.259</a>) by one of the basis functions and integrate the diffusion term by parts to give the following weighted residual:</p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x104b36328&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tbody>\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_ i \\equiv \\int _{\\delta \\Omega } \\phi _ i\\,  k\\nabla \\tilde{T}\\cdot \\vec{n}\\,  ds - \\int _{\\Omega } \\nabla \\phi _ i \\cdot \\left(k\\nabla \\tilde{T}\\right)\\, dA + \\int _{\\Omega } \\phi _ i f\\, dA = 0. \\label{equ:mwr_ dif2d}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.261)</td>\n</tr>\n</tbody>\n</table>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/more-on-finite-element-methods/1690r-boundary-conditions-for-finite-elements');\">Back<span>Boundary Conditions for Finite Elements</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion/1690r-reference-element-and-linear-elements');\">Continue<span>Reference Element and Linear Elements</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "c782bcc9abb3f6bcc638027dfffdc386", 
    "title": "2.11 The Finite Element Method for Two-Dimensional Diffusion", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "d29ce1ed362660b8be9bf2741bbc6ee9", 
            "16176028e8695612f636568d503833fc", 
            "6075ecc5d133cbcb277c06977e6970ea", 
            "365c70a74666ed1cd1408aeb96bff4a6", 
            "c782bcc9abb3f6bcc638027dfffdc386"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "125c58ac6a345a7cbba8de2d3160cb8b", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:14.638 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:47:54.430 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.11 The Finite Element Method for Two-Dimensional Diffusion", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:50:14.728 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:47:56.853 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:34:09.775 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:34:57.591 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 10, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "365c70a74666ed1cd1408aeb96bff4a6", 
            "c782bcc9abb3f6bcc638027dfffdc386", 
            "16176028e8695612f636568d503833fc", 
            "6075ecc5d133cbcb277c06977e6970ea", 
            "d29ce1ed362660b8be9bf2741bbc6ee9", 
            "cd2d971fe847d266f0b1555caab639d4"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion", 
    "is_image_gallery": false, 
    "id": "the-finite-element-method-for-two-dimensional-diffusion", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:20.912 US/Eastern", 
    "always_publish": false, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.11 The Finite Element Method for Two-Dimensional Diffusion", 
            "string"
        ]
    ], 
    "_id": "the-finite-element-method-for-two-dimensional-diffusion", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/the-finite-element-method-for-two-dimensional-diffusion"
}