{
    "text": "<div class=\"navigation pagination\"><ul> <li id=\"top_bck_btn\"><a href=\"./resolveuid/d5200dfdd11c9d138626a6b253c602ff\">&lt;<span>Four-Stage Runge-Kutta Method</span></a></li> <li id=\"flp_btn_1\"><a href=\"./resolveuid/c5e7e539a82c8e620c8ae738a18f6f10\">1.9.1<span>Two-Stage Runge-Kutta Methods</span></a></li> <li id=\"flp_btn_2\"><a href=\"./resolveuid/d5200dfdd11c9d138626a6b253c602ff\">1.9.2<span>Four-Stage Runge-Kutta Method</span></a></li> <li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/1f5a4263b1f3f52202d6af00f58ae206\">1.9.3<span>Stability Regions</span></a></li> <li id=\"top_continue_btn\"><a href=\"./resolveuid/125c58ac6a345a7cbba8de2d3160cb8b\">&gt;<span>Numerical Methods for Partial Differential Equations</span></a></li> </ul></div> <div class=\"self_assessment\"><h2 class=\"subhead\">1.9.3 Stability Regions</h2> <p id=\"taglist\"><a id=\"rkmulti\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO116\" title=\"MO1.16:  Describe the form of the Runge-Kutta family of multi-stage methods. \">Measurable Outcome 1.16</a>, <a id=\"stabilityboundry\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO118\" title=\"MO1.18:  Determine the stability boundary for a multi-step or multi-stage method applied to a linear system of ODEs. \">Measurable Outcome 1.18</a>, <a id=\"odeinteg\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO119\" title=\"MO1.19:  Recommend an appropriate ODE integration method based on the features of the problem being solved. \">Measurable Outcome 1.19</a></p> <text> </text> <p>The eigenvalue stability regions for Runge-Kutta methods can be found using essentially the same approach as for multi-step methods. Specifically, we consider a linear problem in which \\(f = \\lambda u\\) where \\(\\lambda\\) is a constant. Then, we determine the amplification factor \\(g = g(\\lambda {\\Delta t})\\). For example, let's look at the modified Euler method,</p> <table id=\"a0000000134\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr id=\"a0000000135\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle  a\\)</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  {\\Delta t}\\lambda v^ n\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.150)</td> </tr> <tr id=\"a0000000136\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle b\\)</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  {\\Delta t}\\lambda \\left(v^ n + {\\Delta t}\\lambda v^ n/2\\right)\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.151)</td> </tr> <tr id=\"a0000000137\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle v^{n+1}\\)</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  v^ n + {\\Delta t}\\lambda \\left(v^ n + {\\Delta t}\\lambda v^ n/2\\right)\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.152)</td> </tr> <tr id=\"a0000000138\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle v^{n+1}\\)</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  \\left[1 + {\\Delta t}\\lambda + \\frac{1}{2}({\\Delta t}\\lambda )^2 \\right]v^ n\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.153)</td> </tr> <tr id=\"a0000000139\"> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\\(\\displaystyle \\Rightarrow g\\)</td> <td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\\(\\displaystyle  =\\)</td> <td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\\(\\displaystyle  1 + \\lambda {\\Delta t}+ \\frac{1}{2}(\\lambda {\\Delta t})^2\\)</td> <td style=\"width:40%;border-style:hidden\">&nbsp;</td> <td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.154)</td> </tr> </tbody> </table> <p>A similar derivation for the four-stage scheme shows that,</p> <table id=\"a0000000140\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[g = 1 + \\lambda {\\Delta t}+ \\frac{1}{2}(\\lambda {\\Delta t})^2 + \\frac{1}{6}(\\lambda {\\Delta t})^3 + \\frac{1}{24}(\\lambda {\\Delta t})^4.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.155)</td> </tr> </tbody> </table> <p>When analyzing multi-step methods, the next step would be to determine the locations in the \\(\\lambda {\\Delta t}\\)-plane of the stability boundary (i.e. where \\(|g|=1\\)). This however is not easy for Runge-Kutta methods and would require the solution of a higher-order polynomial for the roots. Instead, the most common approach is to simply rely on a contour plotter in which the \\(\\lambda {\\Delta t}\\)-plane is discretized into a finite set of points and \\(|g|\\) is evaluated at these points. Then, the \\(|g|=1\\) contour can be plotted. The following is the MATLAB<sup>&reg;</sup>  code which produces the stability region for the second-order Runge-Kutta methods (note: \\(g(\\lambda {\\Delta t})\\) is the same for both second-order methods):</p> <pre class=\"edx\">\r\n% Specify x range and number of points\r\nx0 = -3;\r\nx1 =  3;\r\nNx = 301;\r\n\r\n% Specify y range and number of points\r\ny0 = -3;\r\ny1 =  3;\r\nNy = 301;\r\n\r\n% Construct mesh\r\nxv    = linspace(x0,x1,Nx);\r\nyv    = linspace(y0,y1,Ny);\r\n[x,y] = meshgrid(xv,yv);\r\n\r\n% Calculate z\r\nz = x + i*y;\r\n\r\n% 2nd order Runge-Kutta growth factor\r\ng = 1 + z + 0.5*z.^2;\r\n\r\n% Calculate magnitude of g\r\ngmag = abs(g);\r\n\r\n% Plot contours of gmag\r\ncontour(x,y,gmag,[1 1],'k-');\r\naxis([x0,x1,y0,y1]);\r\naxis('square');\r\nxlabel('Real \\lambda\\Delta t');\r\nylabel('Imag \\lambda\\Delta t');\r\ngrid on;\r\n</pre> <p>The plots of the stability regions for the second and fourth-order Runge-Kutta algorithms is shown in Figure&nbsp;<a href=\"./resolveuid/1d765b08bf022be5cb59a6df42ff01bb\" onclick=\"window.open(this.href,'16.90r','width=196,height=195','toolbar=1'); return false;\">1.25</a>. These stability regions are larger than those of multi-step methods. In particular, the stability regions of the multi-stage schemes grow with increasing accuracy while the stability regions of multi-step methods decrease with increasing accuracy.</p> <div id=\"fig:rkstab\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\"><center style=\"border-style:hidden\"> <img src=\"./resolveuid/1d765b08bf022be5cb59a6df42ff01bb\" alt=\"The graph shows the smaller stability boundary for second-order Runge-Kutta algorithm, which is completely within the larger boundary of fourth-order Runge-Kutta algorithms.\" width=\"393px\" height=\"391px\" /> <div class=\"caption\"><b>Figure 1.25</b>: <span>Stability boundaries for second-order and fourth-order Runge-Kutta algorithms (stable within the boundaries).</span></div> </center></div> <p>&nbsp;</p> <div id=\"Q1_div\" class=\"problem_question\"><p><b class=\"bfseries\">Exercise</b> Consider the Heun method described previously. For a system with real eigenvalues, what is the most negative value of \\(\\lambda {\\Delta t}\\) for which the Heun method will be stable?</p> <fieldset><legend class=\"visually-hidden\">Exercise 1</legend> <div class=\"choice\"><label id=\"Q1_label\"><span id=\"Q1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><span class=\"visually-hidden\">Numerical Response</span><input type=\"text\" id=\"Q1_input\" value=\"\" onkeypress=\"numericTypedOrDropDownSelected(1)\" class=\"problem_text_input\" /><input type=\"hidden\" id=\"Q1_ans\" value=\"-2.0\" /><input type=\"hidden\" id=\"Q1_tolerance\" value=\"1e-1\" /><span id=\"Q1_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div> <p id=\"S1_ans\" tabindex=\"-1\" class=\"problem_answer\">&nbsp;</p> </fieldset> <div class=\"action\"><button id=\"Q1_button\" onclick=\"checkAnswer({1: 'numerical'})\" class=\"problem_mo_button\">Check</button><button id=\"Q1_button_show\" onclick=\"showHideSolution({1: 'numerical'}, 1, [1])\" class=\"problem_mo_button\">Show Answer</button></div></div> <p>&nbsp;</p> <p>&nbsp;</p> <div id=\"S1_div\" class=\"problem_solution\" tabindex=\"-1\"><font color=\"blue\">Answer: </font> <font color=\"blue\">The stability region for the Heun method is the same as the stability region for the modified Euler method. We see that the stability region includes values of \\(\\lambda {\\Delta t}\\) on the interval \\([-2,0]\\) on the real axis. </font></div> <p>&nbsp;</p></div> <div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-four-stage-runge-kutta-method');\">Back<span>Four-Stage Runge-Kutta Method</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations');\">Continue<span>Numerical Methods for Partial Differential Equations</span></button></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "1f5a4263b1f3f52202d6af00f58ae206", 
    "title": "1.9 Runge-Kutta Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "1f5a4263b1f3f52202d6af00f58ae206", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "c5e7e539a82c8e620c8ae738a18f6f10"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-stability-regions", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "c5e7e539a82c8e620c8ae738a18f6f10", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:18.424 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:43:32.685 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.9.3 Stability Regions", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:49:18.475 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:43:45.124 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:47:35.957 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 11:48:49.163 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/11 14:03:32.808 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 14:04:25.456 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 1, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "d5200dfdd11c9d138626a6b253c602ff", 
            "c5e7e539a82c8e620c8ae738a18f6f10", 
            "1d765b08bf022be5cb59a6df42ff01bb", 
            "1f5a4263b1f3f52202d6af00f58ae206", 
            "6018b2cc123ed80f52d919c7a1393c2e"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-stability-regions", 
    "is_image_gallery": false, 
    "id": "1690r-stability-regions", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/28 11:36:44.724 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.9 Runge-Kutta Methods", 
            "string"
        ]
    ], 
    "_id": "1690r-stability-regions", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/runge-kutta-methods/1690r-stability-regions"
}