{
    "text": "<div class=\"navigation pagination\">\n<ul>\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/3d8df8b822917094b5a69893808a75cc\">&lt;<span>Introduction to Finite Volume Methods</span></a></li>\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/3d8df8b822917094b5a69893808a75cc\">2.5.1<span>Finite Volume Method in 1-D</span></a></li>\n<li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/767b5c964bd2394b92daca9fa25f2e1e\">2.5.2<span>Finite Volume Method Applied to 1-D Convection</span></a></li>\n<li id=\"flp_btn_3\"><a href=\"./resolveuid/d4283096140199d20c85a833f3518826\">2.5.3<span>Finite Volume Method in 2-D</span></a></li>\n<li id=\"flp_btn_4\"><a href=\"./resolveuid/a9d8dcc7e873f01e6dc36f07884b2f23\">2.5.4<span>Finite Volume Method for 2-D Convection on a Rectangular Mesh</span></a></li>\n<li id=\"flp_btn_5\"><a href=\"./resolveuid/eaeacad2dafd93833c0e0227dade769c\">2.5.5<span>Finite Volume Method for Nonlinear Systems</span></a></li>\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/d4283096140199d20c85a833f3518826\">&gt;<span>Finite Volume Method in 2-D</span></a></li>\n</ul>\n</div>\n<div class=\"self_assessment\">\n<h2 class=\"subhead\">2.5.2 Finite Volume Method applied to 1-D Convection</h2>\n<p id=\"taglist\"><a id=\"conservation\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO21\" title=\"MO2.1:  Identify whether a PDE is in the form of a conservation law, describe the characteristic of a conservation law and how the solution behaves along the characteristic. \">Measurable Outcome 2.1</a>, <a id=\"analyticpde\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO22\" title=\"MO2.2:  Qualitatively describe the solution to simple PDEs: convection equation, diffusion equation, convection-diffusion equation, Burgers equation. \">Measurable Outcome 2.2</a>, <a id=\"implement\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO23\" title=\"MO2.3:  Implement a finite difference or finite volume discretization to solve a representative PDE (or set of PDEs) from an engineering application. \">Measurable Outcome 2.3</a></p>\n<text> </text>\n<p>The following MATLAB<sup>\u00ae</sup> script solves the one-dimensional convection equation using the finite volume algorithm given by Equation&nbsp;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\Delta\\ x\\_\\ i\\ \\\\frac\\{U\\_\\ i\\^\\{n\\+1\\}\\ \\-\\ U\\_\\ i\\^\\ n\\}\\{\\{\\\\Delta\\ t\\}\\}\\ \\+\\ F\\_\\{i\\+\\\\frac\\{1\\}\\{2\\}\\}\\^\\{n\\}\\ \\-\\ F\\_\\{i\\-\\\\frac\\{1\\}\\{2\\}\\}\\^\\ n\\ \\=\\ 0\\,\\ \\\\label\\{equ\\:conservation\\_1d\\_\\ FVM\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.107)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.107)');\">2.107</a> and <a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$F\\_\\{i\\+\\\\frac\\{1\\}\\{2\\}\\}\\^\\ n\\ \\=\\ \\\\frac\\{1\\}\\{2\\}u\\^\\ n\\\\left\\(U\\_\\{i\\+1\\}\\^\\ n\\ \\+\\ U\\_\\{i\\}\\^\\ n\\\\right\\)\\ \\-\\ \\\\frac\\{1\\}\\{2\\}\\|u\\^\\ n\\|\\\\left\\(U\\_\\{i\\+1\\}\\^\\ n\\ \\-\\ U\\_\\{i\\}\\^\\ n\\\\right\\)\\.\\ \\\\label\\{equ\\:upwindflux\\_\\ convection1d\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.108)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.108)');\">2.108</a>. The problem is assumed to be periodic so that whatever leaves the domain at \\(x = x_ R\\) re-enters it at \\(x=x_ L\\).</p>\n<pre class=\"edx\">\r\n% Script: convect1d.m\r\n\r\nclear all;\r\n\r\n% Set-up grid\r\nxL = -4;\r\nxR =  4;\r\nNx = 40; % number of control volumes\r\nx = linspace(xL,xR,Nx+1);\r\n\r\n% Calculate midpoint values of x in each control volume\r\nxmid = 0.5*(x(1:Nx) + x(2:Nx+1));\r\n\r\n% Calculate cell size in control volumes (assumed equal)\r\ndx = x(2) - x(1);\r\n\r\n% Set velocity\r\nu = 1;\r\n\r\n% Set final time\r\ntfinal = 100;\r\n\r\n% Set timestep\r\nCFL = 0.5;\r\ndt = CFL*dx/abs(u);\r\n\r\n% Set initial condition to U0 = exp(-x^2)\r\n% Note: technically, we should average the initial\r\n% distribution in each cell but I chose to just set\r\n% the value of U in each control volume to the midpoint\r\n% value of U0.\r\n\r\nU = exp(-xmid.^2);\r\nt = 0;\r\n\r\n% Loop until t &gt; tfinal\r\nwhile (t &lt; tfinal),\r\n\r\n  Ubc = [U(Nx), U, U(1)]; % This enforces the periodic bc\r\n\r\n  % Calculate the flux at each interface\r\n  F =   0.5*    u *( Ubc(2:Nx+2) + Ubc(1:Nx+1)) ...\r\n      - 0.5*abs(u)*( Ubc(2:Nx+2) - Ubc(1:Nx+1));\r\n\r\n  % Calculate residual in each cell\r\n  R = F(2:Nx+1) - F(1:Nx);\r\n\r\n  % Forward Euler step\r\n  U = U - (dt/dx)*R;\r\n\r\n  % Increment time\r\n  t = t + dt;\r\n\r\n  % Plot current solution\r\n  stairs(x,[U, U(Nx)]);\r\n  axis([xL, xR, -0.5, 1.5]);\r\n  grid on;\r\n  drawnow;\r\n\r\nend\r\n\r\n% overlay exact solution\r\nU = exp(-xmid.^2);\r\nhold on;\r\nstairs(x,[U, U(Nx)], 'r-');\r\n\r\n</pre>\n<h2 class=\"subhead\">Exercise</h2>\n<div id=\"Q1_div\" class=\"problem_question\">\n<p>Copy the MATLAB code above and determine which of the following timesteps (\\(\\Delta t\\)) is the largest that still remains stable.</p>\n<fieldset><legend class=\"visually-hidden\">Exercise 1</legend>\n<div class=\"choice\"><label id=\"Q1_input_1_label\"><span id=\"Q1_input_1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_1\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\"> <text> 0.04</text> </span><span id=\"Q1_input_1_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div>\n<div class=\"choice\"><label id=\"Q1_input_2_label\"><span id=\"Q1_input_2_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_2\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\"> <text> 0.07</text> </span><span id=\"Q1_input_2_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div>\n<div class=\"choice\"><label id=\"Q1_input_3_label\"><span id=\"Q1_input_3_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_3\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\"> <text> 0.10</text> </span><span id=\"Q1_input_3_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div>\n<div class=\"choice\"><label id=\"Q1_input_4_label\"><span id=\"Q1_input_4_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_4\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"true\" /><span class=\"choice\"> <text> 0.20</text> </span><span id=\"Q1_input_4_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div>\n</fieldset>\n<div class=\"action\"><button id=\"Q1_button\" onclick=\"checkAnswer({1: 'multiple_choice'})\" class=\"problem_mo_button\">Check</button><button id=\"Q1_button_show\" onclick=\"showHideSolution({1: 'multiple_choice'}, 1, [1])\" class=\"problem_mo_button\">Show Answer</button></div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<div id=\"S1_div\" class=\"problem_solution\" tabindex=\"-1\"><font color=\"blue\">Answer: </font> <font color=\"blue\">The CFL condition (to be discussed in a later module) tells us that \\(\\Delta t = 0.20\\) is the largest timestep choice that will remain stable. </font></div>\n<p>&nbsp;</p>\n</div>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods');\">Back<span>Introduction to Finite Volume Methods</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-in-2-d');\">Continue<span>Finite Volume Method in 2-D</span></button></div>\n<p>&nbsp;</p>", 
    "_content_type_license": "text/plain", 
    "_uid": "767b5c964bd2394b92daca9fa25f2e1e", 
    "title": "2.5 Introduction to Finite Volume Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "d4283096140199d20c85a833f3518826", 
            "eaeacad2dafd93833c0e0227dade769c", 
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "767b5c964bd2394b92daca9fa25f2e1e", 
            "a9d8dcc7e873f01e6dc36f07884b2f23", 
            "3d8df8b822917094b5a69893808a75cc"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-applied-to-1-d-convection", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "3d8df8b822917094b5a69893808a75cc", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:36.157 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:45:26.384 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.5.2 Finite Volume Method Applied to 1-D Convection", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:49:36.203 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:45:27.761 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:15:09.526 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/01/13 14:16:04.420 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/05/27 10:49:39.404 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/05/27 10:52:26.018 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/06/06 10:03:24.334 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/06/06 10:05:43.043 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/07/20 12:59:52.183 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:01:15.572 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/11 14:54:10.692 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 14:54:39.483 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "brem", 
                "time": "2016/10/12 11:57:16.786 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/12 12:03:43.512 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "767b5c964bd2394b92daca9fa25f2e1e", 
            "eaeacad2dafd93833c0e0227dade769c", 
            "3d8df8b822917094b5a69893808a75cc", 
            "d4283096140199d20c85a833f3518826", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "a9d8dcc7e873f01e6dc36f07884b2f23"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-applied-to-1-d-convection", 
    "is_image_gallery": false, 
    "id": "1690r-finite-volume-method-applied-to-1-d-convection", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:17.872 US/Eastern", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.5 Introduction to Finite Volume Methods", 
            "string"
        ]
    ], 
    "_id": "1690r-finite-volume-method-applied-to-1-d-convection", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/introduction-to-finite-volume-methods/1690r-finite-volume-method-applied-to-1-d-convection"
}