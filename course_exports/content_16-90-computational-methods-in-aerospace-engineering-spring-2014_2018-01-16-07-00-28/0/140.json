{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/06f65fc270a3d410b331d186ad67852a\"><<span>The Collocation Method</span></a></li><li id=\"flp_btn_1\"><a href=\"./resolveuid/bda1812471a587a7513fcb81480a1e18\">2.8.1<span>Functional Approximation of the Solution</span></a></li><li id=\"flp_btn_2\"><a href=\"./resolveuid/06f65fc270a3d410b331d186ad67852a\">2.8.2<span>The Collocation Method</span></a></li><li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/2bb791a5f1058421b20e147e46034287\">2.8.3<span>The Method of Weighted Residuals</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/b85dba099fd2582c61f1a5573f5c79a5\">2.8.4<span>Galerkin Method with New Basis</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/b85dba099fd2582c61f1a5573f5c79a5\">><span>Galerkin Method with New Basis</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">2.8.3 The Method of Weighted Residuals</h2>\n<p id=\"taglist\">\n<a id=\"weightresid\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO212\" title=\"MO2.12:  Describe how the method of weighted residuals can be used to calculate an approximate solution to a PDE. \">Measurable Outcome 2.12</a>, <a id=\"diffWeightColLS\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO213\" title=\"MO2.13:  Describe the differences between the method of weighted residuals, the collocation method, and the least-squares method for approximating a PDE. \">Measurable Outcome 2.13</a>, <a id=\"Galerkin\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO214\" title=\"MO2.14:  Describe the Galerkin method of weighted residuals. \">Measurable Outcome 2.14</a>\n</p>\n<p>\nWhile the collocation method enforces the residual to be zero at \\(N\\) points, the method of weighted residuals requires \\(N\\) weighted integrals of the residual to be zero. A weighted residual is simply the integral over the domain of the residual multiplied by a weight function, \\(w(x)\\). For example, in the one-dimensional diffusion problem we are considering, a weighted residual is, </p>\n<table id=\"a0000000309\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\int _{-1}^{1} w(x)\\,  R(\\tilde{T},x)\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.174)</td>\n</tr>\n</table>\n<p>\n By choosing \\(N\\) weight functions, \\(w_ i(x)\\) for \\(i=1,\\ldots ,N\\), and setting these \\(N\\) weighted residuals to zero, we obtain \\(N\\) equations which we solve to determine the \\(N\\) unknown values of \\(a_ j\\). </p>\n<p>\nWe define the weighted residual for \\(w_ i(x)\\) to be </p>\n<table id=\"a0000000310\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_ i(\\tilde{T}) \\equiv \\int _{-1}^{1} w_ i(x)\\,  R(\\tilde{T},x)\\,  dx.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.175)</td>\n</tr>\n</table>\n<p>\n The method of weighted residuals requires </p>\n<table id=\"a0000000311\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[R_ i(\\tilde{T}) = 0 \\qquad \\mbox{for } i=1,\\ldots ,N.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.176)</td>\n</tr>\n</table>\n<p>\nIn the method of weighted residuals, the next step is to determine appropriate weight functions. A common approach, known as the Galerkin method, is to set the weight functions equal to the functions used to approximate the solution. That is, </p>\n<table id=\"a0000000312\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[w_ i(x) = \\phi _ i(x). \\quad \\mbox{(Galerkin)}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.177)</td>\n</tr>\n</table>\n<p>\n For the heat diffusion example we have been considering, this would give weight functions as </p>\n<table id=\"a0000000313\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000314\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  w_1(x)\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  (1-x)(1+x),\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.178)</td>\n</tr>\n<tr id=\"a0000000315\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle w_2(x)\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  x(1-x)(1+x).\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.179)</td>\n</tr>\n</table>\n<p>\nNow, we must calculate the weighted residuals. For our example, we have </p>\n<table id=\"a0000000316\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000317\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  R_1(\\tilde{T})\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\int _{-1}^{1} w_1(x)\\,  R(\\tilde{T},x)\\,  dx,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.180)</td>\n</tr>\n<tr id=\"a0000000318\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    &#xA0;\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\int _{-1}^{1} (1-x)(1+x)\\,  \\left(-2 a_1 - 6 a_2 x + 50 e^ x\\right)\\,  dx,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.181)</td>\n</tr>\n<tr id=\"a0000000319\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    &#xA0;\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  -\\frac{8}{3} a_1 + 200 e^{-1}.\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.182)</td>\n</tr>\n</table>\n<p>\n To do this integral, the following results were used: </p>\n<table id=\"a0000000320\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000321\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  \\int _ a^ b (1-x)(1+x)\\, dx\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\left[ x - \\frac{1}{3}x^3 \\right]_ a^ b,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.183)</td>\n</tr>\n<tr id=\"a0000000322\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle \\int _ a^ b x(1-x)(1+x)\\, dx\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\left[ \\frac{1}{2}x^2 - \\frac{1}{4}x^4 \\right]_ a^ b,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.184)</td>\n</tr>\n<tr id=\"a0000000323\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle \\int _ a^ b x^2 e^ x\\, dx\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\left[ x^2 e^ x - 2 x e^ x + 2 e^ x \\right]_ a^ b.\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.185)</td>\n</tr>\n</table>\n<p>\n Similarly, calculating \\(R_2\\): </p>\n<table id=\"a0000000324\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000325\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  R_2(\\tilde{T})\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\int _{-1}^{1} w_2(x)\\,  R(\\tilde{T},x)\\,  dx,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.186)</td>\n</tr>\n<tr id=\"a0000000326\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    &#xA0;\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\int _{-1}^{1} x(1-x)(1+x)\\,  \\left(-2 a_1 - 6 a_2 x + 50 e^ x\\right)\\,  dx,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.187)</td>\n</tr>\n<tr id=\"a0000000327\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    &#xA0;\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  -\\frac{8}{5} a_2 + 100 e^{1} - 700 e^{-1},\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.188)</td>\n</tr>\n</table>\n<p>\n where the following results have been used: </p>\n<table id=\"a0000000328\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000329\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  \\int _ a^ b x^2(1-x)(1+x)\\, dx\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\left[ \\frac{1}{3}x^3 - \\frac{1}{5}x^5 \\right]_ a^ b,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.189)</td>\n</tr>\n<tr id=\"a0000000330\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle \\int _ a^ b x e^ x\\, dx\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\left[ x e^ x - e^ x \\right]_ a^ b,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.190)</td>\n</tr>\n<tr id=\"a0000000331\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle \\int _ a^ b x^3 e^ x\\, dx\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\left[ x^3 e^ x - 3 x^2 e^ x + 6x e^ x - 6e^ x \\right]_ a^ b.\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.191)</td>\n</tr>\n</table>\n<p>\nFinally, we can solve for \\(a_1\\) and \\(a_2\\) by setting the weighted residuals \\(R_1\\) and \\(R_2\\) to zero: </p>\n<table id=\"a0000000332\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000333\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  -\\frac{8}{3} a_1 + 200 e^{-1}\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  0,\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.192)</td>\n</tr>\n<tr id=\"a0000000334\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle -\\frac{8}{5} a_2 + 100 e^{1} - 700 e^{-1}\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  0.\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(2.193)</td>\n</tr>\n</table>\n<p>\n This could be written as a \\(2\\times 2\\) matrix equation and solved, but the equations are decoupled and can be readily solved, </p>\n<table id=\"a0000000335\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\Rightarrow \\left(\\begin{array}{c} a_1 \\\\ a_2 \\end{array}\\right) = \\left(\\begin{array}{r} 75 e^{-1} \\\\ \\frac{125}{2}e^{1} - \\frac{875}{2}e^{-1} \\end{array}\\right) = \\left(\\begin{array}{r} 27.591 \\\\ 8.945 \\end{array}\\right).\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.194)</td>\n</tr>\n</table>\n<p>\nThe results using this method of weighted residuals are shown in the figures below. Comparison with the collocation method results shows that the method of weighted residuals is more accurate in this case. </p>\n<div id=\"fig:T_MWR\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\">\n<center style=\"border-style:hidden\">\n<img src=\"./resolveuid/34c0c17852197294d452bc57cac2fbc7\" alt=\"This graph shows two very similar lines, each with a single peak, that represent the results T and &#126;T for method of weighted residuals.\" width=\"473px\" height=\"390px\" />\n</center></div><div class=\"caption\"><b>Figure 2.30</b>: <span>Comparison of \\(T\\) (solid) and \\(\\tilde{T}\\) (dashed) for method of weighted residuals.</span></div>\n<div id=\"fig:E_MWR\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\">\n<center style=\"border-style:hidden\">\n<img src=\"./resolveuid/6c3a259743d3b8008cd37d2797bec13b\" alt=\"This graph has one line, representing the error T minus &#126;T for method of weighted residuals.  The line begins at the y-axis point zero, then decreases below -0.3, increases above 0.4, then decreases to almost -0.4, and finally increases back to zero.\" width=\"494px\" height=\"390px\" />\n</center></div><div class=\"caption\"><b>Figure 2.31</b>: <span>Error \\(\\tilde{T}-T\\) for method of weighted residuals.</span></div>\n<div id=\"fig:R_MWR\" class=\"figure\" style=\"padding-top:5px;padding-bottom:15px\">\n<center style=\"border-style:hidden\">\n<img src=\"./resolveuid/230e1fbe722a155d36a26dc508775fb4\" alt=\"This graph of the residual R(&#126;T, x) for method of weighted resiuduals has a single line that steadily decreases and then increases.\" width=\"492px\" height=\"390px\" />\n</center></div><div class=\"caption\"><b>Figure 2.32</b>: <span>Residual \\(R(\\tilde{T},x)\\) for method of weighted residuals.</span></div>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/method-of-weighted-residuals/1690r-the-collocation-method');\">Back<span>The Collocation Method</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/method-of-weighted-residuals/1690r-galerkin-method-with-new-basis');\">Continue<span>Galerkin Method with New Basis</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "2bb791a5f1058421b20e147e46034287", 
    "title": "2.8 Method of Weighted Residuals", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "06f65fc270a3d410b331d186ad67852a", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "b85dba099fd2582c61f1a5573f5c79a5", 
            "bda1812471a587a7513fcb81480a1e18", 
            "876be530ac0533845428281b2b3c5b68"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/method-of-weighted-residuals/1690r-the-method-of-weighted-residuals", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "bda1812471a587a7513fcb81480a1e18", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:03.119 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:46:26.960 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.8.3 The Method of Weighted Residuals", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:50:03.164 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:46:29.291 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 13:11:31.063 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/11 13:12:29.212 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 3, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/method-of-weighted-residuals/1690r-the-method-of-weighted-residuals", 
    "is_image_gallery": false, 
    "id": "1690r-the-method-of-weighted-residuals", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:36.745 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.8 Method of Weighted Residuals", 
            "string"
        ]
    ], 
    "_id": "1690r-the-method-of-weighted-residuals", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/method-of-weighted-residuals/1690r-the-method-of-weighted-residuals"
}