{
    "text": "<div class=\"navigation pagination\"><ul> <li id=\"top_bck_btn\"><a href=\"./resolveuid/cf738358ae33664a5d71baadd24f92cb\">&lt;<span>The CFL Condition</span></a></li> <li id=\"flp_btn_1\" class=\"button_selected\"><a href=\"./resolveuid/935b6a618d7d27726ca1df78ee864834\">2.7.1<span>Fourier Analysis of PDEs</span></a></li> <li id=\"flp_btn_2\"><a href=\"./resolveuid/3e2eea01ee64f3f7264f4d9e57b3b622\">2.7.2<span>Matrix Stability for Finite Difference Methods</span></a></li> <li id=\"flp_btn_3\"><a href=\"./resolveuid/c4a73127b3fffeee5cd92e3e6319d356\">2.7.3<span>Circulant Matrices</span></a></li> <li id=\"flp_btn_4\"><a href=\"./resolveuid/cb633bb139250ab57f908bb74bb848bb\">2.7.4<span>Stability Exercises</span></a></li> <li id=\"top_continue_btn\"><a href=\"./resolveuid/3e2eea01ee64f3f7264f4d9e57b3b622\">&gt;<span>Matrix Stability for Finite Difference Methods</span></a></li> </ul></div> <div class=\"self_assessment\"><h2 class=\"subhead\">2.7.1 Fourier Analysis of PDEs</h2> <p id=\"taglist\"><a id=\"analyticpde\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO22\" title=\"MO2.2:  Qualitatively describe the solution to simple PDEs: convection equation, diffusion equation, convection-diffusion equation, Burgers equation. \">Measurable Outcome 2.2</a>, <a id=\"perfEigen\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO211\" title=\"MO2.11:  Perform an eigenvalue stability analysis of a finite difference approximation of a PDE using either Von Neumann analysis or a semi-discrete (method of lines) analysis. \">Measurable Outcome 2.11</a></p> <p>We will develop Fourier analysis in one dimension. The basic ideas extend easily to multiple dimensions. We will consider the convection-diffusion equation,</p> <table id=\"a0000000260\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{\\partial U}{\\partial t} + u\\frac{\\partial U}{\\partial x} = \\mu \\frac{\\partial ^2 U}{\\partial x^2}.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.124)</td> </tr> </tbody> </table> <p>We will assume that the velocity, \\(u\\), and the viscosity, \\(\\mu\\) are constant.</p> <p>The solution is assumed to be periodic over a length \\(L\\). Thus,</p> <table id=\"a0000000261\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[U(x+mL,t) = U(x,t)\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.125)</td> </tr> </tbody> </table> <p>where \\(m\\) is any integer.</p> <p>A Fourier series with periodicity over length \\(L\\) is given by,</p> <table id=\"equ:Fourier\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[U(x,t) = \\sum _{m=-\\infty }^{+\\infty } \\hat{U}_ m(t)e^{i k_ m x} \\qquad \\mbox{where} \\qquad k_ m = \\frac{2\\pi m}{L}. \\label{equ:Fourier}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.126)</td> </tr> </tbody> </table> <p>\\(k_ m\\) is generally called the wavenumber, though \\(m\\) is the number of waves occurring over the length \\(L\\). We note that \\(\\hat{U}_ m(t)\\) is the amplitude of the \\(m\\)-th wavenumber and it is generally complex (since we have used complex exponentials). Substituting the Fourier series into the convection-diffusion equation gives,</p> <table id=\"a0000000262\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{\\partial }{\\partial t}\\left[\\sum _{m=-\\infty }^{+\\infty } \\hat{U}_ m(t)e^{i k_ m x}\\right] + u \\frac{\\partial }{\\partial x}\\left[\\sum _{m=-\\infty }^{+\\infty } \\hat{U}_ m(t)e^{i k_ m x}\\right] = \\mu \\frac{\\partial ^2}{\\partial x^2}\\left[\\sum _{m=-\\infty }^{+\\infty } \\hat{U}_ m(t)e^{i k_ m x}\\right].\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.127)</td> </tr> </tbody> </table> <table id=\"a0000000263\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\sum _{m=-\\infty }^{+\\infty } \\frac{{\\rm d}\\hat{U}_ m}{{\\rm d}t} e^{i k_ m x} + u \\sum _{m=-\\infty }^{+\\infty } i k_ m \\hat{U}_ m e^{i k_ m x} = \\mu \\sum _{m=-\\infty }^{+\\infty } (i k_ m)^2 \\hat{U}_ m e^{i k_ m x}.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.128)</td> </tr> </tbody> </table> <p>Noting that \\(i^2 = -1\\) and collecting terms gives,</p> <table id=\"&lt;plasTeX.TeXFragment object at 0x104ada050&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\sum _{m=-\\infty }^{+\\infty } \\left[\\frac{{\\rm d}\\hat{U}_ m}{{\\rm d}t} + \\left(i u k_ m + \\mu k_ m^2\\right) \\hat{U}_ m\\right] e^{i k_ m x} = 0. \\label{equ:Fourier_ preorth}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.129)</td> </tr> </tbody> </table> <p>The next step is to utilize the orthogonality of the different Fourier modes over the length \\(L\\), specifically,</p> <table id=\"&lt;plasTeX.TeXFragment object at 0x104ada4c8&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\int _0^ L e^{-i k_ n x} e^{i k_ m x} dx = \\left\\{ \\begin{array}{cc} 0 &amp;  \\mbox{if } m \\neq n \\\\ L &amp;  \\mbox{if } m = n \\end{array}\\right. \\label{equ:Fourier_ orth}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.130)</td> </tr> </tbody> </table> <p>By multiplying Equation&nbsp;(<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\sum\\ \\_\\{m\\=\\-\\\\infty\\ \\}\\^\\{\\+\\\\infty\\ \\}\\ \\\\left\\[\\\\frac\\{\\{\\\\rm\\ d\\}\\\\hat\\{U\\}\\_\\ m\\}\\{\\{\\\\rm\\ d\\}t\\}\\ \\+\\ \\\\left\\(i\\ u\\ k\\_\\ m\\ \\+\\ \\\\mu\\ k\\_\\ m\\^2\\\\right\\)\\ \\\\hat\\{U\\}\\_\\ m\\\\right\\]\\ e\\^\\{i\\ k\\_\\ m\\ x\\}\\ \\=\\ 0\\.\\ \\\\label\\{equ\\:Fourier\\_\\ preorth\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.129)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.129)');\">2.129</a>) by \\(e^{-i k_ n x}\\) and integrating from \\(0\\) to \\(L\\), the orthogonality condition gives,</p> <table id=\"&lt;plasTeX.TeXFragment object at 0x104adac18&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\frac{{\\rm d}\\hat{U}_ n}{{\\rm d}t} + \\left(i u k_ n + \\mu k_ n^2\\right) \\hat{U}_ n = 0, \\qquad \\mbox{for any integer value of } n. \\label{equ:Fourier_ condif1d}\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.131)</td> </tr> </tbody> </table> <p>Thus, the evolution of the amplitude for an individual wavenumber is independent of the other wavenumbers. The solution to Equation&nbsp;(<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$\\\\frac\\{\\{\\\\rm\\ d\\}\\\\hat\\{U\\}\\_\\ n\\}\\{\\{\\\\rm\\ d\\}t\\}\\ \\+\\ \\\\left\\(i\\ u\\ k\\_\\ n\\ \\+\\ \\\\mu\\ k\\_\\ n\\^2\\\\right\\)\\ \\\\hat\\{U\\}\\_\\ n\\ \\=\\ 0\\,\\ \\\\qquad\\ \\\\mbox\\{for\\ any\\ integer\\ value\\ of\\ \\}\\ n\\.\\ \\\\label\\{equ\\:Fourier\\_\\ condif1d\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(2.131)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (2.131)');\">2.131</a>),</p> <table id=\"a0000000264\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\"> <tbody> <tr> <td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\hat{U}_ n(t) = \\hat{U}_ n(0) e^{-i u k_ n t} e^{-\\mu k_ n^2 t}.\\]</td> <td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(2.132)</td> </tr> </tbody> </table> <p>The convection term, which results in the complex time dependent behavior, \\(e^{-i u k_ n t}\\), only oscillates and does not change the magnitude of \\(\\hat{U}_ n\\). The diffusion term causes the magnitude to decrease as long as \\(\\mu &gt; 0\\). But, if the diffusion coefficient were negative, then the magnitude would increase unbounded with time. Thus, in the case of the convection-diffusion PDE, as long as \\(\\mu \\geq 0\\), this solution is stable.</p> <h2 class=\"subhead\">Exercise</h2>\n<div id=\"Q1_div\" class=\"problem_question\">\n<p>\r\nFor the convection-diffusion equation analyzed above, how does the rate of damping compare for a wavelength \\(L\\) mode to a wavelength \\(L/10\\) mode? </p><fieldset><legend class=\"visually-hidden\">Exercise 1</legend><div class=\"choice\"><label id=\"Q1_input_1_label\"><span id=\"Q1_input_1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_1\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> 10 times slower</text>\n</span><span id=\"Q1_input_1_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q1_input_2_label\"><span id=\"Q1_input_2_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_2\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> The damping rates are equal</text>\n</span><span id=\"Q1_input_2_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q1_input_3_label\"><span id=\"Q1_input_3_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_3\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> 1000 times slower</text>\n</span><span id=\"Q1_input_3_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div><div class=\"choice\"><label id=\"Q1_input_4_label\"><span id=\"Q1_input_4_aria_status\" tabindex=\"-1\" class=\"visually-hidden\">&amp;nbsp;</span><input type=\"radio\" id=\"Q1_input_4\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"true\" /><span class=\"choice\">\n<text> 100 times slower</text>\n</span><span id=\"Q1_input_4_normal_status\" class=\"nostatus\" aria-hidden=\"true\">&amp;nbsp;</span></label></div></fieldset><div class=\"action\"><button id=\"Q1_button\" onclick=\"checkAnswer({1: 'multiple_choice'})\" class=\"problem_mo_button\">Check</button><button id=\"Q1_button_show\" onclick=\"showHideSolution({1: 'multiple_choice'}, 1, [1])\" class=\"problem_mo_button\">Show Answer</button></div></div>\n<p>\n<div id=\"S1_div\" class=\"problem_solution\" tabindex=\"-1\">\n<font color=\"blue\">Answer: </font>\n<font color=\"blue\">The \\(L_1\\) mode has a damping rate \\(\\mu k_ m^2 = \\mu (2\\pi /L)^2\\) the \\(L/10\\) mode has a damping rate of \\(\\mu k_ m^2 = \\mu (20\\pi /L)^2\\) </font>\n</div></p>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/upwinding-and-the-cfl-condition/1690r-the-cfl-condition');\">Back<span>The CFL Condition</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/eigenvalue-stability-of-finite-difference-methods/1690r-matrix-stability-for-finite-difference-methods');\">Continue<span>Matrix Stability for Finite Difference Methods</span></button></div></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "935b6a618d7d27726ca1df78ee864834", 
    "title": "2.7 Eigenvalue Stability of Finite Difference Methods", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "125c58ac6a345a7cbba8de2d3160cb8b", 
            "c4a73127b3fffeee5cd92e3e6319d356", 
            "cf738358ae33664a5d71baadd24f92cb", 
            "cb633bb139250ab57f908bb74bb848bb", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "3e2eea01ee64f3f7264f4d9e57b3b622", 
            "935b6a618d7d27726ca1df78ee864834"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/eigenvalue-stability-of-finite-difference-methods", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "125c58ac6a345a7cbba8de2d3160cb8b", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:41.555 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:46:04.343 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "2.7 Eigenvalue Stability of Finite Difference Methods", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:49:41.635 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:46:05.046 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/01/13 14:19:12.739 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/01/13 14:20:45.255 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/05/27 10:53:02.915 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/05/27 10:53:48.479 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/06/03 15:59:33.529 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/06/03 16:01:24.366 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "bmcintosh", 
                "time": "2016/07/20 13:37:06.468 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/07/20 13:38:56.797 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 6, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "cf738358ae33664a5d71baadd24f92cb", 
            "935b6a618d7d27726ca1df78ee864834", 
            "3e2eea01ee64f3f7264f4d9e57b3b622", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "c4a73127b3fffeee5cd92e3e6319d356", 
            "cb633bb139250ab57f908bb74bb848bb"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/eigenvalue-stability-of-finite-difference-methods", 
    "is_image_gallery": false, 
    "id": "eigenvalue-stability-of-finite-difference-methods", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 03:50:18.410 US/Eastern", 
    "always_publish": false, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "2.7 Eigenvalue Stability of Finite Difference Methods", 
            "string"
        ]
    ], 
    "_id": "eigenvalue-stability-of-finite-difference-methods", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-methods-for-partial-differential-equations/eigenvalue-stability-of-finite-difference-methods"
}