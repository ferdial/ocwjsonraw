{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/ed041125462b3411083140b32f255066\"><<span>The Error in Estimating Probabilities</span></a></li><li id=\"flp_btn_1\"><a href=\"./resolveuid/74bb46fc55cb5dc91f9b7be6791726ec\">3.4.1<span>The Error in Estimating the Mean</span></a></li><li id=\"flp_btn_2\"><a href=\"./resolveuid/ed041125462b3411083140b32f255066\">3.4.2<span>The Error in Estimating Probabilities</span></a></li><li id=\"flp_btn_3\" class=\"button_selected\"><a href=\"./resolveuid/9fcaf3a2fde9204023cd8a8ace84e37f\">3.4.3<span>The Error in Estimating the Variance</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/22e0637c83225bd8a9d4e80723fbc928\">3.4.4<span>Bootstrapping</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/22e0637c83225bd8a9d4e80723fbc928\">><span>Bootstrapping</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">3.4.3 The Error in Estimating the Variance</h2>\n<p id=\"taglist\">\n<a id=\"unbiasedestimators\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO38\" title=\"MO3.8:  State unbiased estimators for mean and variance of a random variable, and for the probability of particular events. \">Measurable Outcome 3.8</a>, <a id=\"stderrorformulae\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO311\" title=\"MO3.11:  Give standard errors for sample estimators of mean, variance, and event probability. \">Measurable Outcome 3.11</a>, <a id=\"confidenceinterval\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO312\" title=\"MO3.12:  Obtain confidence intervals for sample estimates of the mean, variance, and event probability. \">Measurable Outcome 3.12</a>\n</p>\n<p>\n</p>\n<h2 class=\"subhead\">The Error in Estimating the Variance</h2>\n<p>\nThe variance of \\(y\\) is given the symbol, \\(\\sigma _ y^2\\), and is defined as, </p>\n<table id=\"equ:variance\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\sigma _ y^2 = E[(y-\\mu _ y)^2] \\label{equ:variance}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.52)</td>\n</tr>\n</table>\n<p>\nWe saw in the previous unit that an unbiased estimator of \\(\\sigma _ y^2\\) is \\(s_ y^2\\), that is: </p>\n<table id=\"a0000000469\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[E[s_ y^2] = \\sigma _ y^2.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.53)</td>\n</tr>\n</table>\n<p>\nNote, you should try proving this result. </p>\n<p>\nTo quantify the uncertainty in this estimator, we would like to determine the standard error, </p>\n<table id=\"a0000000470\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\sigma _{s^2_ y} \\equiv \\left\\{ E\\left[(s_ y^2-\\sigma _ y^2)^2\\right]\\right\\} ^{1/2}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.54)</td>\n</tr>\n</table>\n<p>\nUnfortunately, this standard error is not known for general distributions of \\(y\\). However, if \\(y\\) has a normal distribution, then, </p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x104cafc80&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\sigma _{s^2_ y} = \\frac{\\sigma _ y^2}{\\sqrt {N/2}} \\label{equ:se_ s2y_ normal}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.55)</td>\n</tr>\n</table>\n<p>\nUnder the assumption of \\(y\\) being normally distributed, the distribution of \\(s^2_ y\\) is also related to the chi-squared distribution. Specifically, \\((N-1)s^2_ y/\\sigma ^2_ y\\) has a chi-square distribution with \\(N-1\\) degrees of freedom. Note that the requirement that \\(y\\) be normally distributed is much more restrictive than the requirements for the mean error estimates to hold. For the mean error estimates, the standard error, \\(\\sigma _{\\overline{y}} = \\sigma _ y/\\sqrt {N}\\), is exact regardless of the distribution of \\(y\\). The application of the central limit theorem which gives that\\(\\overline{y}\\) is normally distributed only requires that the number of samples is large but does not constrain the distribution of \\(y\\) itself (beyond requiring that \\(f(y)\\) is continuous). </p>\n<h2 class=\"subhead\">Standard Deviation</h2>\n<p>\nTypically, the standard deviation of \\(y\\) is estimated using \\(s_ y\\), i.e.&#xA0;the square root of the variance estimator. This estimate, however, is biased, </p>\n<table id=\"a0000000471\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[E[s_ y] \\neq \\sigma _ y.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.56)</td>\n</tr>\n</table>\n<p>\nThe standard error for this estimate is only known exactly when \\(y\\) is normally distributed. In that case, </p>\n<table id=\"a0000000472\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\sigma _{s_ y} \\equiv \\left\\{ E[(s_ y-\\sigma _ y)^2]\\right\\} ^{1/2} = \\frac{\\sigma _ y}{\\sqrt {2N}}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.57)</td>\n</tr>\n</table>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-the-error-in-estimating-probabilities');\">Back<span>The Error in Estimating Probabilities</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-bootstrapping');\">Continue<span>Bootstrapping</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "9fcaf3a2fde9204023cd8a8ace84e37f", 
    "title": "3.4 Error Estimates for the Monte Carlo Method", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "ed041125462b3411083140b32f255066", 
            "22e0637c83225bd8a9d4e80723fbc928", 
            "487c3b15ab67d7c95cffa6b147049d0c", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "74bb46fc55cb5dc91f9b7be6791726ec"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-the-error-in-estimating-the-variance", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "74bb46fc55cb5dc91f9b7be6791726ec", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:30.155 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:44.136 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "3.4.3 The Error in Estimating the Variance", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:50:30.200 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:48:55.401 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 1, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-the-error-in-estimating-the-variance", 
    "is_image_gallery": false, 
    "id": "1690r-the-error-in-estimating-the-variance", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:41.519 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "3.4 Error Estimates for the Monte Carlo Method", 
            "string"
        ]
    ], 
    "_id": "1690r-the-error-in-estimating-the-variance", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-the-error-in-estimating-the-variance"
}