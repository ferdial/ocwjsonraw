{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/36e637ced6ffe05d36060d537611ad2e\"><<span>Systems of ODE's and Eigenvalue Stability</span></a></li><li id=\"flp_btn_1\"><a href=\"./resolveuid/36e637ced6ffe05d36060d537611ad2e\">1.6.1<span>Nonlinear Systems</span></a></li><li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/e8dbfb2204cb6fc4431e0b32e5ea65da\">1.6.2<span>Linear Constant Coefficient Systems</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/04ce95ca3b3acc385d8381923a3dd6fe\">1.6.3<span>Eigenvalue Stability for a Linear ODE</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/64bbf17403266a46283d2d2450cf7589\">1.6.4<span>Imaginary Eigenvalues</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/04ce95ca3b3acc385d8381923a3dd6fe\">><span>Eigenvalue Stability for a Linear ODE</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">1.6.2 Linear Constant Coefficient Systems</h2>\n<p id=\"taglist\">\n<a id=\"firstorderodes\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO11\" title=\"MO1.1:  Define a first-order ODE. \">Measurable Outcome 1.1</a>, <a id=\"analyticalvalidate\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO12\" title=\"MO1.2:  Use analytical solutions to validate numerical solutions of ODE. \">Measurable Outcome 1.2</a>, <a id=\"nonlinear\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO13\" title=\"MO1.3:  Distinguish nonlinear ODEs from linear ODEs. \">Measurable Outcome 1.3</a>, <a id=\"linearize\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO14\" title=\"MO1.4:  Approximate the behavior of a nonlinear equation with a linear one. \">Measurable Outcome 1.4</a>, <a id=\"zeroeigenstability\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO19\" title=\"MO1.9:  Assess whether a numerical method is zero stable, and whether a numerical method is eigenvalue stable. \">Measurable Outcome 1.9</a>\n</p>\n<p>\nThe analysis of numerical methods applied to linear, constant coefficient systems can provide significant insight into the behavior of numerical methods for nonlinear problems. Consider the following problem, </p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x104959598&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u_ t = A u, \\label{equ:ODElinear_ sys}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.90)</td>\n</tr>\n</table>\n<p>\n where \\(A\\) is a \\(d \\times d\\) matrix. Assuming that a complete set of eigenvectors exists, the matrix \\(A\\) can be decomposed as, </p>\n<table id=\"equ:eigen\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[A = R\\Lambda R^{-1}, \\qquad \\Lambda = \\mbox{diag}(\\lambda _1, \\lambda _2, \\cdots , \\lambda _ d), \\qquad R = \\left(\\begin{array}{c|c|c|c|c}r_1 &amp;  r_2 &amp;  r_3 &amp;  \\cdots &amp;  r_ d \\\\ \\end{array}\\right) \\label{equ:eigen}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.91)</td>\n</tr>\n</table>\n<p>\n The solution to Equation&#xA0;<a href=\"javascript: void(0)\" onclick=\"return newWindow('&lt;html&gt;&lt;head&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.6.1/MathJax.js?config=TeX-MML-AM_SVG&quot;&gt; &lt;/script&gt;&lt;/head&gt;&lt;body&gt;&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot; style=&quot;table-layout:auto;border-style:hidden&quot;&gt;&lt;tr&gt;&lt;td style=&quot;width:80%;vertical-align:middle;text-align:center;border-style:hidden&quot;&gt;\\$\\$u\\_\\ t\\ \\=\\ A\\ u\\,\\ \\\\label\\{equ\\:ODElinear\\_\\ sys\\}\\$\\$&lt;/td&gt;&lt;td style=&quot;width:20%;vertical-align:middle;text-align:left;border-style:hidden&quot;&gt;(1.90)&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;','Equation (1.90)');\">1.90</a> can be derived as follows, </p>\n<table id=\"a0000000078\" cellpadding=\"7\" width=\"100%\" cellspacing=\"0\" class=\"eqnarray\" style=\"table-layout:auto;border-style:hidden\">\n<tr id=\"a0000000079\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle  u_ t\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  A u\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.92)</td>\n</tr>\n<tr id=\"a0000000080\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle u_ t\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  R\\Lambda R^{-1} u\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.93)</td>\n</tr>\n<tr id=\"a0000000081\">\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"vertical-align:middle;                                    text-align:right;border-style:hidden\">\n\t    \\(\\displaystyle R^{-1} u_ t\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:center;border-style:hidden\">\n\t    \\(\\displaystyle  =\\)\n        </td>\n<td style=\"vertical-align:middle;                                    text-align:left;border-style:hidden\">\n\t    \\(\\displaystyle  \\Lambda R^{-1} u\\)\n        </td>\n<td style=\"width:40%;border-style:hidden\">&#xA0;</td>\n<td style=\"width:20%;border-style:hidden\" class=\"eqnnum\">(1.94)</td>\n</tr>\n</table>\n<p>\n Then, defining \\(w = R^{-1} u\\), </p>\n<table id=\"a0000000082\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[w_ t = \\Lambda w.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.95)</td>\n</tr>\n</table>\n<p>\n Since \\(\\Lambda\\) is a diagonal matrix, this system of equations is actually uncoupled from each other, so that each of the eigenmodes has its own independent evolution equation, </p>\n<table id=\"a0000000083\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[(w_ j)_ t = \\lambda _ j w_ j, \\qquad \\mbox{for each } j = 1 \\mbox{ to } d\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.96)</td>\n</tr>\n</table>\n<p>\n Since each of the eigenmodes has a solution \\(w_ j(t) = w_ j(0)\\exp (\\lambda _ j t)\\), then the solution for \\(u(t)\\) can be written as, </p>\n<table id=\"&lt;plasTeX.TeXFragment object at 0x1048e0668&gt;\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u(t) = \\sum _{j=1}^ d w_ j(0) r_ j e^{\\lambda _ j t}. \\label{equ:ODElinear_ sys_ solution}\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.97)</td>\n</tr>\n</table>\n<p>\nNote that the eigenvalues are in general complex, \\(\\lambda _ j = {\\lambda _ j}_ r + i {\\lambda _ j}_ i\\). The imaginary part of the eigenvalues determines the frequency of oscillations, and the real part of the eigenvalues determines the growth or decay rate. Specifically, </p>\n<table id=\"a0000000084\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[e^{\\lambda t} = e^{(\\lambda _ r + i \\lambda _ i)t} = \\left(\\cos \\lambda _ i t + i\\sin \\lambda _ i t\\right) e^{ \\lambda _ r t}.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.98)</td>\n</tr>\n</table>\n<p>\n Thus, when \\(\\lambda _ r &gt; 0\\), the solution will grow unbounded as \\(t \\rightarrow \\infty\\). </p>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability');\">Back<span>Systems of ODE's and Eigenvalue Stability</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-eigenvalue-stability-for-a-linear-ode');\">Continue<span>Eigenvalue Stability for a Linear ODE</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "e8dbfb2204cb6fc4431e0b32e5ea65da", 
    "title": "1.6 Systems of ODE's and Eigenvalue Stability", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "04ce95ca3b3acc385d8381923a3dd6fe", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "64bbf17403266a46283d2d2450cf7589", 
            "3e2eea01ee64f3f7264f4d9e57b3b622", 
            "36e637ced6ffe05d36060d537611ad2e"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-linear-constant-coefficient-systems", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "36e637ced6ffe05d36060d537611ad2e", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:49:06.263 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:43:06.238 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.6.2 Linear Constant Coefficient Systems", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:49:06.308 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:43:06.828 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-linear-constant-coefficient-systems", 
    "is_image_gallery": false, 
    "id": "1690r-linear-constant-coefficient-systems", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2017/11/20 04:39:55.218 US/Eastern", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.6 Systems of ODE's and Eigenvalue Stability", 
            "string"
        ]
    ], 
    "_id": "1690r-linear-constant-coefficient-systems", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/systems-of-odes-and-eigenvalue-stability/1690r-linear-constant-coefficient-systems"
}