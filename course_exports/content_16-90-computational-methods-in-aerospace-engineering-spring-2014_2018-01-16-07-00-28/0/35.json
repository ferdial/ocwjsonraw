{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/99ee39b779f49afb0849103c798f1c50\"><<span>Convergence</span></a></li><li id=\"flp_btn_1\"><a href=\"./resolveuid/99ee39b779f49afb0849103c798f1c50\">1.4.1<span>Types of Errors</span></a></li><li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/c1ab4737a98d26fbdcee7c83dfab47d4\">1.4.2<span>Convergence of Numerical Methods</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/bc0d43fe1169e2be34aacfba7fd50607\">1.4.3<span>Rate of Convergence Global Order of Accuracy</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/bc0d43fe1169e2be34aacfba7fd50607\">><span>Rate of Convergence Global Order of Accuracy</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\n<h2 class=\"subhead\">1.4.2 Convergence of Numerical Methods</h2>\n<p id=\"taglist\">\n<a id=\"convergent\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO17\" title=\"MO1.7:  Assess whether a numerical method converges, and calculate its global order of accuracy. \">Measurable Outcome 1.7</a>\n</p>\n<p>\nAs the timestep is decreased, i.e. \\({\\Delta t}\\rightarrow 0\\), the approximation from a finite difference method should converge to the solution of the ODE. This concept is known as convergence and is stated mathematically as follows: </p>\n<h2 class=\"subhead\">Definition of Convergence</h2>\n<p>\nA finite difference method for solving, </p>\n<table id=\"a0000000057\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[u_ t = f(u,t) \\qquad \\mbox{with} \\qquad u(0) = u_0\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.63)</td>\n</tr>\n</table>\n<p>\n from \\(t=0\\) to \\(T\\) is convergent if </p>\n<table id=\"a0000000058\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\max _{n=[0,T/{\\Delta t}]} |v^ n - u(n{\\Delta t})| \\rightarrow 0 \\qquad \\mbox{as} \\qquad {\\Delta t}\\rightarrow 0.\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(1.64)</td>\n</tr>\n</table>\n<p>\nThis is a mathematically precise definition; let's take a moment to investigate it a little more deeply. The first part of the statement is familiar by now; we are solving a first-order ODE of the form \\(u_ t=f(u,t)\\) with given initial conditions. We assume that a method is defined to produce our numerical solution \\(v^ n\\) for \\(n=1,2,\\ldots ,T / {\\Delta t}\\) (we assign \\(v^0=u^0\\)); e.g., the forward Euler method. </p>\n<p>\nIn words, the convergence statement is as follows: If we shrink the time step smaller and smaller, the largest absolute error between the numerical solution and the exact solution will also get smaller and smaller. For any numerical solution, the absolute error can be measured at each of the time indices \\(n=0,1,\\ldots , T / {\\Delta t}\\): that is given by \\(|v^ n-u^ n|\\). </p>\n<p>\nThus, for any numerical solution, we can also define the maximum absolute error over those time indices: that is written \\(\\max _{n \\in \\{ 0,1,\\ldots ,T/{\\Delta t}\\} }|v^ n-u^ n|\\). This is the worst case error for the numerical solution. If we drive the largest absolute error to zero, it is implied that the error at each time index will also be driven to zero. To summarize, our convergence statement says that in the limit \\({\\Delta t}\\to 0\\), the numerical solution collapses onto the exact solution and the error goes to zero at all time indices. </p>\n<p>\nIt is important to note that in the limit \\({\\Delta t}\\to 0\\), the last time index \\(T/{\\Delta t}\\to \\infty\\) even for finite \\(T\\); the time interval between adjacent numerical solution points \\((t^ n,v^ n)\\) and \\((t^{n+1},v^{n+1})\\) is also shrinking to zero. So not only does our numerical solution approach zero error at the time steps \\(t^0,t^1,\\ldots ,\\) these time steps are getting closer and closer together so that the ordered pairs \\((t^0,v^0)\\);\\((t^1,v^1)\\),... make up the entirety of the exact continuous solution \\(u(t)\\). </p>\n<p>\nWhile convergence is a clear requirement for a good numerical method, the rate at which the method converges is also important. This rate is known as the global order of accuracy and is discussed in the next section. </p>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence');\">Back<span>Convergence</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence/1690r-rate-of-convergence--global-order-of-accuracy-');\">Continue<span>Rate of Convergence Global Order of Accuracy</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "c1ab4737a98d26fbdcee7c83dfab47d4", 
    "title": "1.4 Convergence", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "bc0d43fe1169e2be34aacfba7fd50607", 
            "99ee39b779f49afb0849103c798f1c50", 
            "e726e961fd269620907a1248b173744d", 
            "a3fbfbbde140393baed5af945a9316f9"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence/1690r-convergence-of-numerical-methods", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [], 
    "parent_uid": "99ee39b779f49afb0849103c798f1c50", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:48:59.295 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:41:50.231 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.4.2 Convergence of Numerical Methods", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:48:59.337 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:41:54.300 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 0, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence/1690r-convergence-of-numerical-methods", 
    "is_image_gallery": false, 
    "id": "1690r-convergence-of-numerical-methods", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:20.710 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.4 Convergence", 
            "string"
        ]
    ], 
    "_id": "1690r-convergence-of-numerical-methods", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence/1690r-convergence-of-numerical-methods"
}