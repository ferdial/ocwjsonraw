{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/6f317b0d95c1d32cf178c9fdbae632d2\"><<span>Introduction to Design Optimization</span></a></li><li id=\"flp_btn_1\"><a href=\"./resolveuid/6f317b0d95c1d32cf178c9fdbae632d2\">3.6.1<span>Design Optimization</span></a></li><li id=\"flp_btn_2\" class=\"button_selected\"><a href=\"./resolveuid/1d39506a8ae7400e107dfe048008e5c2\">3.6.2<span>Gradient Based Optimization</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/da1cf6178af153b14d4a177f24979948\">3.6.3<span>Unconstrained Gradient-Based Optimization Methods</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/c0e755e748fd33db478288b736e0381c\">3.6.4<span>Finite Difference Methods</span></a></li><li id=\"flp_btn_5\"><a href=\"./resolveuid/3cde12b3c306b87c973f5af561f4875f\">3.6.5<span>The 1d Search</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/da1cf6178af153b14d4a177f24979948\">><span>Unconstrained Gradient-Based Optimization Methods</span></a></li></div><div class=\"self_assessment\">\n<h2 class=\"subhead\">3.6.2 Gradient Based Optimization</h2>\n<p id=\"taglist\">\n<a id=\"optimizationtechniques\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO317\" title=\"MO3.17:  Describe the steepest descent, conjugate gradient, and the Newton method for optimization of multivariate functions, and apply these optimization techniques to simple unconstrained design problems. \">Measurable Outcome 3.17</a>\n</p>\n<text>\n</text><p>\n</p>\n<p>\r\nMost optimization algorithms are iterative. We begin with an initial guess for our design variables, denoted \\(x^0\\). We then iteratively update this guess until the optimal design is achieved. </p>\n<table id=\"a0000000482\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[x^ q = x^{q-1} + \\alpha ^ q S^ q, \\;  q = 1,2, \\ldots\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.67)</td>\n</tr>\n</table>\n<p>\r\n where </p>\n<p>\r\n\\(q\\)=iteration number </p>\n<p>\r\n\\(x^ q\\) is our guess for \\(x\\) at iteration \\(q\\) </p>\n<p>\r\n\\(S^ q \\in \\mathbb {R}^ n\\) is our vector search direction at iteration \\(q\\) </p>\n<p>\r\n\\(\\alpha ^ q\\) is the scalar step length at iteration \\(q\\) </p>\n<p>\r\n\\(x^0\\) is given initial guess. </p>\n<p>\r\nAt each iteration we have two decisions to make: in which direction to move (i.e., what \\(S^ q\\) to choose, and how far to move along that direction (i.e., how large should \\(\\alpha ^ q\\) be). Optimization algorithms determine the search direction \\(S^ q\\) according to some criteria. Gradient-based algorithms use gradient information to compute the search direction. </p>\n<p>\r\nFor \\(J(x)\\) a scalar objective function that depends on \\(n\\) design variables, the gradient of \\(J\\) with respect to \\(x=[x_1 x_2 \\ldots x_ n]^ T\\) is a vector of length \\(n\\). In general, we need the gradient evaluated at some point \\(x^ k\\): </p>\n<table id=\"a0000000483\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\nabla J(x^ k) = [ \\frac{\\partial J}{\\partial x_1}(x^ k) \\  \\ldots \\frac{\\partial J}{\\partial x_ n}(x^ k) ]\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.68)</td>\n</tr>\n</table>\n<p>\r\nThe second derivative of \\(J\\) with respect to \\(x\\) is a matrix, called the Hessian matrix, of dimension \\(n \\times n\\). In general, we need the Hessian evaluated at some point \\(x^ k\\): </p>\n<table id=\"a0000000484\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\nabla ^2 J(x^ k) = [ \\frac{\\partial ^2 J}{\\partial x_1^2}(x^ k) \\  \\ldots \\frac{\\partial ^2 J}{\\partial x_1 \\partial x_ n}(x^ k) \\\\ \\frac{\\partial ^2 J}{\\partial x_1 \\partial x_ n}(x^ k) \\  \\ldots \\frac{\\partial ^2 J}{\\partial x_ n^2}(x^ k)]\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.69)</td>\n</tr>\n</table>\n<p>\r\nIn other words, the \\((i,j)\\) entry of the Hessian is given by, </p>\n<table id=\"a0000000485\" class=\"equation\" width=\"100%\" cellspacing=\"0\" cellpadding=\"7\" style=\"table-layout:auto;border-style:hidden\">\n<tr>\n<td class=\"equation\" style=\"width:80%;vertical-align:middle;text-align:center;border-style:hidden\">\\[\\nabla ^2 J(x^ k)_{i,j} = \\frac{\\partial ^2 J}{\\partial x_ i \\partial x_ j}(x^ k)\\]</td>\n<td class=\"eqnnum\" style=\"width:20%;vertical-align:middle;text-align:left;border-style:hidden\">(3.70)</td>\n</tr>\n</table>\n<p>\r\nConsider the function \\(J(x)=3x_1 + x_1 x_2 + x_3^{2} + 6x_2^{3} x_3\\). </p>\n<p>\n<div id=\"Q1_div\" class=\"problem_question\">\n<p>\r\nEnter the gradient evaluated at the point \\((x_1,x_2,x_3)=(1,1,1)\\): </p><fieldset><legend class=\"visually-hidden\">Exercise 1</legend><div class=\"choice\"><label id=\"Q1_input_1_label\"><span id=\"Q1_input_1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><input type=\"radio\" id=\"Q1_input_1\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> \\((4, 7, 9)\\)</text>\n</span><span id=\"Q1_input_1_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div><div class=\"choice\"><label id=\"Q1_input_2_label\"><span id=\"Q1_input_2_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><input type=\"radio\" id=\"Q1_input_2\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> \\((5, 19, 3)\\)</text>\n</span><span id=\"Q1_input_2_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div><div class=\"choice\"><label id=\"Q1_input_3_label\"><span id=\"Q1_input_3_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><input type=\"radio\" id=\"Q1_input_3\" onclick=\"optionSelected(1)\" name=\"Q1_input\" class=\"problem_radio_input\" correct=\"true\" /><span class=\"choice\">\n<text> \\((4, 19, 8)\\)</text>\n</span><span id=\"Q1_input_3_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div></fieldset></div></p>\n<p>\n<div id=\"S1_div\" class=\"problem_solution\" tabindex=\"-1\">\n</div></p>\n<p>\n<div id=\"Q2_div\" class=\"problem_question\">\n<p>\r\nEnter the last row of the Hessian evaluated at the point \\((x_1,x_2,x_3)=(1,1,1)\\): </p><fieldset><legend class=\"visually-hidden\">Exercise 2</legend><div class=\"choice\"><label id=\"Q2_input_1_label\"><span id=\"Q2_input_1_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><input type=\"radio\" id=\"Q2_input_1\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> \\((3, 18, 2)\\)</text>\n</span><span id=\"Q2_input_1_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div><div class=\"choice\"><label id=\"Q2_input_2_label\"><span id=\"Q2_input_2_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><input type=\"radio\" id=\"Q2_input_2\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"true\" /><span class=\"choice\">\n<text> \\((0, 18, 2)\\)</text>\n</span><span id=\"Q2_input_2_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div><div class=\"choice\"><label id=\"Q2_input_3_label\"><span id=\"Q2_input_3_aria_status\" tabindex=\"-1\" class=\"visually-hidden\"></span><input type=\"radio\" id=\"Q2_input_3\" onclick=\"optionSelected(2)\" name=\"Q2_input\" class=\"problem_radio_input\" correct=\"false\" /><span class=\"choice\">\n<text> \\((0, 16, 2)\\)</text>\n</span><span id=\"Q2_input_3_normal_status\" class=\"nostatus\" aria-hidden=\"true\"></span></label></div></fieldset><div class=\"action\"><button id=\"Q1_button\" onclick=\"checkAnswer({1: 'multiple_choice', 2: 'multiple_choice'})\" class=\"problem_mo_button\">Check</button><button id=\"Q1_button_show\" onclick=\"showHideSolution({1: 'multiple_choice', 2: 'multiple_choice'}, 1, [1, 2])\" class=\"problem_mo_button\">Show Answer</button></div></div></p>\n<p>\n<div id=\"S2_div\" class=\"problem_solution\" tabindex=\"-1\">\n</div></p>\n</div><div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization');\">Back<span>Introduction to Design Optimization</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization/1690r-unconstrained-gradient-based-optimization-methods');\">Continue<span>Unconstrained Gradient-Based Optimization Methods</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "1d39506a8ae7400e107dfe048008e5c2", 
    "title": "3.6 Introduction to Design Optimization", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "c0e755e748fd33db478288b736e0381c", 
            "487c3b15ab67d7c95cffa6b147049d0c", 
            "1d39506a8ae7400e107dfe048008e5c2", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "6f317b0d95c1d32cf178c9fdbae632d2"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization/1690r-gradient-based-optimization", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "6f317b0d95c1d32cf178c9fdbae632d2", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:32.598 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:59.537 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "3.6.2 Gradient Based Optimization", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:50:32.643 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:49:02.511 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/12 12:04:02.852 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "brem", 
                "comments": "", 
                "time": "2016/10/12 12:05:15.683 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 0, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "c0e755e748fd33db478288b736e0381c", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "3cde12b3c306b87c973f5af561f4875f", 
            "6f317b0d95c1d32cf178c9fdbae632d2", 
            "da1cf6178af153b14d4a177f24979948", 
            "1d39506a8ae7400e107dfe048008e5c2"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization/1690r-gradient-based-optimization", 
    "is_image_gallery": false, 
    "id": "1690r-gradient-based-optimization", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:41.933 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "3.6 Introduction to Design Optimization", 
            "string"
        ]
    ], 
    "_id": "1690r-gradient-based-optimization", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/introduction-to-design-optimization/1690r-gradient-based-optimization"
}