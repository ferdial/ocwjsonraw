{
    "text": "<div class=\"navigation pagination\">\n<ul>\n<li id=\"top_bck_btn\"><a href=\"./resolveuid/9fcaf3a2fde9204023cd8a8ace84e37f\">&lt;<span>The Error in Estimating the Variance</span></a></li>\n<li id=\"flp_btn_1\"><a href=\"./resolveuid/74bb46fc55cb5dc91f9b7be6791726ec\">3.4.1<span>The Error in Estimating the Mean</span></a></li>\n<li id=\"flp_btn_2\"><a href=\"./resolveuid/ed041125462b3411083140b32f255066\">3.4.2<span>The Error in Estimating Probabilities</span></a></li>\n<li id=\"flp_btn_3\"><a href=\"./resolveuid/9fcaf3a2fde9204023cd8a8ace84e37f\">3.4.3<span>The Error in Estimating the Variance</span></a></li>\n<li id=\"flp_btn_4\" class=\"button_selected\"><a href=\"./resolveuid/22e0637c83225bd8a9d4e80723fbc928\">3.4.4<span>Bootstrapping</span></a></li>\n<li id=\"top_continue_btn\"><a href=\"./resolveuid/e719ff5142ac62fbc38b4410308e2feb\">&gt;<span>Variance Reduction Techniques for the Monte Carlo Method</span></a></li>\n</ul>\n</div>\n<!--?xml version='1.0' encoding='utf-8'?-->\n<h2 class=\"subhead\">3.4.4 Bootstrapping</h2>\n<p>The standard errors and confidence intervals for a variety of estimators often rely upon assumptions on the underlying output values, \\(y\\). For example, the standard error for the variance as given in the previous unit requires the assumption that \\(y\\) is distributed normally. Clearly, in most applications that will not be the case. In this situation, an alternative method is required if confidence intervals are desired. One such method is known as bootstrapping.</p>\n<p>As an example, let's consider the Monte Carlo estimation of the 50-percentile value of a population. From a Monte Carlo sample, the 50-percentile value of the population could be estimated as the median of the sample. If we wanted to determine the standard error associated with using the median of sample size \\(N\\) as an estimate for the 50-percentile value, one possibility would be to run multiple Monte Carlo's of sample size \\(N\\) and estimate the variability of the estimator directly. Suppose the a total of \\(M\\) Monte Carlo samples are run (each of size \\(N\\)), then the total number of simulations would be \\(M \\times N\\). Labelling \\(\\theta _ i\\) as the estimator (i.e.&nbsp;the median) from the \\(i\\)-th Monte Carlo sample, the distribution of \\(\\theta\\) can be determined and confidence intervals can be built using the \\(M\\) values of \\(\\theta _ i\\). This, however, could be very expensive since \\(M \\times N\\) simulations are required.</p>\n<p>Bootstrapping avoids the need for \\(M \\times N\\) simulations by resampling from a single Monte Carlo simulation. After a single Monte Carlo simulation, \\(N\\) simulations have been performed and the results of the simulations are all equally likely to have occurred (since they were drawn in a random, independent manner). A bootstrap resampling is performed by drawing with uniform probability from this original Monte Carlo sample. Specifically, if we wanted to generate \\(M\\) bootstrap samples of size \\(N\\):</p>\n<ol class=\"enumerate\">\n<li>\n<p>Perform an initial Monte Carlo sample of size \\(N\\) to produce \\(y_ i\\) with \\(i=1\\) to \\(N\\). From this sample, calculate the desired estimator \\(\\theta _1 = \\theta (y_ i)\\).</p>\n</li>\n<li>\n<p>Resample the values of \\(y_ i\\) assuming uniform probability of the events (i.e.&nbsp;each \\(y_ i\\) has a probability of \\(1/N\\)). Since the same event may occur a different number of times in this resample and in the original sample, this will generate a new sample, \\(\\hat{y}_ i\\). From this resample, calculate the desired estimator, \\(\\theta _ j = \\theta (\\hat{y}_ i)\\).</p>\n</li>\n<li>\n<p>Perform the resampling in Step 2 a total of \\(M-1\\) times. In all, this will produce \\(M\\) values of the estimator, \\(\\theta _1\\) through \\(\\theta _ M\\).</p>\n</li>\n<li>\n<p>From the \\(M\\) values of \\(\\theta _ i\\), confidence intervals can be determined.</p>\n</li>\n</ol>\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-the-error-in-estimating-the-variance');\">Back<span>The Error in Estimating the Variance</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/variance-reduction-techniques-for-the-monte-carlo-method');\">Continue<span>Variance Reduction Techniques for the Monte Carlo Method</span></button></div>\n<p>&nbsp;</p>", 
    "_content_type_license": "text/plain", 
    "_uid": "22e0637c83225bd8a9d4e80723fbc928", 
    "title": "3.4 Error Estimates for the Monte Carlo Method", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "ed041125462b3411083140b32f255066", 
            "22e0637c83225bd8a9d4e80723fbc928", 
            "487c3b15ab67d7c95cffa6b147049d0c", 
            "74bb46fc55cb5dc91f9b7be6791726ec", 
            "e719ff5142ac62fbc38b4410308e2feb"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-bootstrapping", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "74bb46fc55cb5dc91f9b7be6791726ec", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:50:30.540 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:48:38.090 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "3.4.4 Bootstrapping", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "jmartis", 
                "comments": "", 
                "time": "2015/11/06 09:50:30.588 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "pgandhi3", 
                "time": "2015/11/18 04:48:42.364 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/05/27 11:07:30.559 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "bmcintosh", 
                "comments": "", 
                "time": "2016/05/27 11:08:01.325 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "9fcaf3a2fde9204023cd8a8ace84e37f", 
            "74bb46fc55cb5dc91f9b7be6791726ec", 
            "22e0637c83225bd8a9d4e80723fbc928", 
            "ed041125462b3411083140b32f255066", 
            "e719ff5142ac62fbc38b4410308e2feb"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-bootstrapping", 
    "is_image_gallery": false, 
    "id": "1690r-bootstrapping", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/20 06:45:41.597 GMT-4", 
    "always_publish": true, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "3.4 Error Estimates for the Monte Carlo Method", 
            "string"
        ]
    ], 
    "_id": "1690r-bootstrapping", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/probabilistic-methods-and-optimization/error-estimates-for-the-monte-carlo-method/1690r-bootstrapping"
}