{
    "text": "<div class=\"navigation pagination\"><li id=\"top_bck_btn\"><a href=\"./resolveuid/ae250ef953d178da8810f88b0aaa6408\"><<span>The Midpoint Method</span></a></li><li id=\"flp_btn_1\" class=\"button_selected\"><a href=\"./resolveuid/a3fbfbbde140393baed5af945a9316f9\">1.3.1<span>Errors</span></a></li><li id=\"flp_btn_2\"><a href=\"./resolveuid/6121a3cad8f1c65278216a952ea8ff2c\">1.3.2<span>Local Truncation Error</span></a></li><li id=\"flp_btn_3\"><a href=\"./resolveuid/09523d4baafc8f4ce9b7d3d67ad30e7b\">1.3.3<span>Local Order of Accuracy</span></a></li><li id=\"flp_btn_4\"><a href=\"./resolveuid/58553753c9ccc012f030d87844ae8db5\">1.3.4<span>Definition of Multi-Step Methods</span></a></li><li id=\"flp_btn_5\"><a href=\"./resolveuid/ff3b4491f1d1d23e2d3a939de701c5c2\">1.3.5<span>Example of Most Accurate Multi-Step Method</span></a></li><li id=\"top_continue_btn\"><a href=\"./resolveuid/6121a3cad8f1c65278216a952ea8ff2c\">><span>Local Truncation Error</span></a></li></div><?xml version='1.0' encoding='utf-8'?>\r\n<h2 class=\"subhead\">1.3.1 Errors</h2>\r\n<p id=\"taglist\">\r\n<a id=\"discretize\" class=\"mo_link\" href=\"./resolveuid/6018b2cc123ed80f52d919c7a1393c2e/#anchorMO15\" title=\"MO1.5:  Discretize a univariate function and its derivative, assess the truncation error using Taylor series analysis. \">Measurable Outcome 1.5</a>\r\n</p>\r\n<p>\r\n</p>\r\n<p>\r\nThere are two sources of error when we solve an ODE with a numerical method. The first is <em>rounding error</em>, due to the finite precision of floating-point arithmetic. The second is <em>truncation error</em> (sometimes called discretization error), due to the method used. The truncation error would remain even if we were to use exact arithmetic. We will be concerned with truncation error, which is typically the dominant error source for our problems of interest. We will further define two types of truncation error: the <i class=\"itshape\">global error</i> is the difference between the computed solution at \\(t^ n\\) and the true solution of the ODE at \\(t^ n\\), while the <i class=\"itshape\">local error</i> is the error made in one step of the numerical method, i.e.&#xA0;the difference between the computed solution at \\(t^ n\\) and the solution of the ODE passing through the previous point \\((t^{n-1},v^{n-1})\\). We defer the global error to Section&#xA0;<a href=\"/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/convergence/1690r-convergence-of-numerical-methods/\">1.4.2</a> and will investigate the local error here. </p>\r\n<div class=\"navigation progress\"><button id=\"bck_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/discretizing-odes/1690r-the-midpoint-method');\">Back<span>The Midpoint Method</span></button> <button id=\"continue_btn\" type=\"button\" onclick=\"window.location.assign('/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy/1690r-local-truncation-error');\">Continue<span>Local Truncation Error</span></button> </div>", 
    "_content_type_license": "text/plain", 
    "_uid": "a3fbfbbde140393baed5af945a9316f9", 
    "title": "1.3 Order of Accuracy", 
    "_ac_local_roles": {
        "jmartis": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "ff3b4491f1d1d23e2d3a939de701c5c2", 
            "6121a3cad8f1c65278216a952ea8ff2c", 
            "5cae4847c59aa247c7673c0c6b0abef1", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "ae250ef953d178da8810f88b0aaa6408", 
            "58553753c9ccc012f030d87844ae8db5", 
            "a3fbfbbde140393baed5af945a9316f9"
        ]
    }, 
    "constrainTypesMode": -1, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "5cae4847c59aa247c7673c0c6b0abef1", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2015/11/06 09:48:54.758 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2015/11/18 04:41:29.308 US/Eastern", 
    "parent_module": "c20ba6edc9d26448cd5545a1ccb7c3cd", 
    "is_mathml_document": true, 
    "bottomtext": "", 
    "short_page_title": "1.3 Order of Accuracy", 
    "section_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "jmartis", 
                "time": "2015/11/06 09:48:54.922 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "pgandhi3", 
                "comments": "", 
                "time": "2015/11/18 04:41:29.396 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "c20ba6edc9d26448cd5545a1ccb7c3cd"
        ], 
        "isReferencing": [
            "ae250ef953d178da8810f88b0aaa6408", 
            "a3fbfbbde140393baed5af945a9316f9", 
            "6018b2cc123ed80f52d919c7a1393c2e", 
            "6121a3cad8f1c65278216a952ea8ff2c", 
            "09523d4baafc8f4ce9b7d3d67ad30e7b", 
            "58553753c9ccc012f030d87844ae8db5", 
            "ff3b4491f1d1d23e2d3a939de701c5c2", 
            "c1ab4737a98d26fbdcee7c83dfab47d4"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2015.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy", 
    "is_image_gallery": false, 
    "id": "order-of-accuracy", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "", 
    "course_identifier": "https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "jmartis"
    ], 
    "modification_date": "2016/10/27 12:04:51.807 GMT-4", 
    "always_publish": false, 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1.3 Order of Accuracy", 
            "string"
        ]
    ], 
    "_id": "order-of-accuracy", 
    "courselist_features": [], 
    "_path": "/Plone/courses/aeronautics-and-astronautics/16-90-computational-methods-in-aerospace-engineering-spring-2014/numerical-integration-of-ordinary-differential-equations/order-of-accuracy"
}