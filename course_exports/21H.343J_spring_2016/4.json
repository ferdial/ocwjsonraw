{
    "text": "<p>At our first meeting, the instructors will divide the students into two groups, Group A and Group B.</p> <p>In most weeks, one group will meet with Professors McCants and Ravel to discuss class readings and examine rare print objects from the MIT collections, while the other group will meet with Ken Stone in the <a href=\"http://studentlife.mit.edu/hobbyshop\">MIT Hobby Shop</a> to build our printing press. The following week the two groups will trade places, so that all students in the class will participate equally in our academic and building exercises.</p> <p>(HS) = Sessions in the MIT Hobby Shop</p> <div class=\"maintabletemplate\"><table summary=\"See table caption for summary.\" class=\"tablewidth100\">     <caption class=\"invisible\">Course calendar.</caption>  <!-- BEGIN TABLE HEADER (for MIT OCW Table Template 2.51) -->     <thead>         <tr>             <th scope=\"col\">SES&nbsp;#</th>             <th scope=\"col\">TOPICS</th>             <th scope=\"col\">DUE&nbsp;DATES</th>         </tr>     </thead>     <!-- END TABLE HEADER -->     <tbody>         <tr class=\"row\">             <td colspan=\"3\"><strong>Week 1</strong></td>         </tr>         <tr class=\"alt-row\">             <td>1</td>             <td><p>Introduction.</p>             <p>Split class into Groups A &amp; B.</p></td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td>2</td>             <td>Class visit to <a href=\"http://bostongazette.org/\">The Printing Office of Edes &amp; Gill</a></td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 2</strong></td>         </tr>         <tr class=\"row\">             <td>3</td>             <td>Printing press construction session (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td>4</td>             <td>Printing press construction session (HS) (cont.)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td colspan=\"3\"><strong>Week 3</strong></td>         </tr>         <tr class=\"alt-row\">             <td>5</td>             <td>Paper-Making, Manuscript, and Print</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td>6</td>             <td><p>Manuscripts and Early European Print Materials in the MIT Libraries: Visit from MIT Rare Books Program Manager <a href=\"http://libguides.mit.edu/profiles/skuce\">Stephen Skuce</a></p>             <ol>                 <li>Illuminated European Book of Hours, ca. 1450</li>                 <li>Persian illuminated manuscript, 1495</li>                 <li>The Nuremburg Chronicle, 1493</li>             </ol></td>             <td>Forum post due</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 4</strong></td>         </tr>         <tr class=\"row\">             <td>7</td>             <td>Printing press construction session (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td>8</td>             <td>Printing press construction session (HS) (cont.)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td colspan=\"3\"><strong>Week 5</strong></td>         </tr>         <tr class=\"alt-row\">             <td>9</td>             <td>Religion in Print</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td>10</td>             <td><p>Early Modern Printed Religious Materials in the MIT Libraries: Visit from MIT Rare Books Program Manager <a href=\"http://libguides.mit.edu/profiles/skuce\">Stephen Skuce</a></p>             <ol>                 <li>Leaves from the Gutenberg Bible, 1453</li>                 <li>1507 Canon law Decrees of Gregory IX</li>                 <li>1564 English Book of Martyrs</li>                 <li>1572 German Gospels and Sermons</li>                 <li>Leaf from the King James Bible, 1640s</li>                 <li>1693 Lutheran Bible</li>                 <li>1700 Amsterdam polyglot Bible</li>             </ol></td>             <td>Forum post due</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 6</strong></td>         </tr>         <tr class=\"row\">             <td>11</td>             <td>Printing press construction session (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td>12</td>             <td>Printing press construction session (HS) (cont.)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td colspan=\"3\"><strong>Week 7</strong></td>         </tr>         <tr class=\"alt-row\">             <td>13</td>             <td>The Meanings and Uses of Early European Print&mdash;A Conversation</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td>14</td>             <td>A visit to the <a href=\"http://mitmuseum.mit.edu/hart-nautical-collections-list\">Hart Nautical Collection</a>, MIT Museum</td>             <td>First paper due</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 8</strong></td>         </tr>         <tr class=\"row\">             <td>15</td>             <td>Printing press construction session (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td>16</td>             <td>Printing press construction session (HS) (cont.)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td colspan=\"3\"><strong>Week 9</strong></td>         </tr>         <tr class=\"alt-row\">             <td>17</td>             <td>Print and Natural Philosophy</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td>18</td>             <td><p>Early Modern Natural Philosophy in the MIT Libraries: Visit from MIT Rare Books Program Manager <a href=\"http://libguides.mit.edu/profiles/skuce\">Stephen Skuce</a></p>             <ol>                 <li>1556, Conrad Gessner, <em>Illustrated Zoology</em></li>                 <li>1580s, Ambroise Par\u00e9, <em>Collected Works</em> (Essays on curiosities of the animal world, healing battlefield wounds, human procreation)</li>                 <li>1656 <em>Works of Galileo</em></li>                 <li>1666, <em>Observations of Tycho Brahe</em></li>                 <li>Isaac Newton, <em>Mathematical Principles of Natural Philosophy</em>, 2nd ed., ca. 1715</li>                 <li>1486, 1532, 1626 editions of <em>The Spheres of Sacrobosco,</em> an astronomical textbook (Venetian and Dutch examples)</li>                 <li>1561 &amp; 1621 editions of Agricola, <em>De Re Metallica</em> (metallurgy text, both published in Basel)</li>                 <li>1648, 1651, 1691 versions of an English text entitled <em>Mathematical Recreations</em></li>             </ol></td>             <td>Forum post due</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 10</strong></td>         </tr>         <tr class=\"row\">             <td>19</td>             <td>Printing press construction session (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 11</strong></td>         </tr>         <tr class=\"row\">             <td>20</td>             <td><p>How A Sixteenth-Century Italian Miller Read: The Historian's Interpretation</p></td>             <td>Forum post due</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 12</strong></td>         </tr>         <tr class=\"row\">             <td>21</td>             <td>Experimental printing on completed press (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td>22</td>             <td>Experimental printing on completed press (HS) (cont.)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td colspan=\"3\"><strong>Week 13</strong></td>         </tr>         <tr class=\"alt-row\">             <td>23</td>             <td><p>How A Sixteenth-Century Italian Miller Read: The Documents I</p></td>             <td>&nbsp;</td>         </tr>         <tr class=\"row\">             <td>24</td>             <td><p>How A Sixteenth-Century Italian Miller Read: The Documents II</p></td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td colspan=\"3\"><strong>Week 14</strong></td>         </tr>         <tr class=\"row\">             <td>25</td>             <td>Print Your Own Text I (HS)</td>             <td>&nbsp;</td>         </tr>         <tr class=\"alt-row\">             <td>26</td>             <td>Print Your Own Text II (HS)</td>             <td>Second paper due</td>         </tr>         <!-- TEN ROWS -->     </tbody> </table></div>", 
    "_content_type_license": "text/plain", 
    "_uid": "8370028f43ef4e294624f7e1d8fe7289", 
    "title": "Calendar", 
    "_ac_local_roles": {
        "asingh33": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {}, 
    "constrainTypesMode": 0, 
    "technical_location": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/calendar", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "a1af3ab1ded1c20003d16b99541d7a6b", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2016/07/27 02:49:53.153 GMT-4", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2016/07/27 04:25:01.443 GMT-4", 
    "parent_module": "a1af3ab1ded1c20003d16b99541d7a6b", 
    "is_mathml_document": false, 
    "bottomtext": "", 
    "short_page_title": "Calendar", 
    "section_identifier": null, 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "asingh33", 
                "time": "2016/07/27 02:49:53.166 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/07/27 04:25:01.519 GMT-4"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "asingh33", 
                "time": "2016/08/02 23:33:49.266 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/08/03 02:00:11.166 GMT-4"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/08/09 23:30:14.293 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/08/09 23:32:19.917 GMT-4"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "asingh33", 
                "time": "2016/08/10 07:07:56.326 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "asingh33", 
                "time": "2016/08/10 07:26:01.344 GMT-4"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/09/08 02:50:49.012 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "asingh33", 
                "comments": "", 
                "time": "2016/09/08 02:56:02.548 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "a1af3ab1ded1c20003d16b99541d7a6b"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2016.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/calendar", 
    "is_image_gallery": false, 
    "id": "calendar", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "This section contains a calendar featuring the course topics, hobby shop sessions, field trips, and assignment due dates.", 
    "course_identifier": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "asingh33"
    ], 
    "modification_date": "2017/01/04 16:31:30.329 US/Eastern", 
    "always_publish": false, 
    "_content_type_description": "text/plain", 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "Calendar", 
            "string"
        ]
    ], 
    "_id": "calendar", 
    "courselist_features": [], 
    "_path": "/Plone/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/calendar"
}