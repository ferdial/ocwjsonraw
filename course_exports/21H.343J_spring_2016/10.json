{
    "text": "<p>21H.343J / CC.120J Making Books: The Renaissance and Today<em> involved three curricular components: 1) engagement with key readings; 2) exploration of rare print objects from the MIT collections; and 3) the hands-on experience of building a printing press and making cotton rag paper. In this section, Professor Jeffrey Ravel describes how offering students tangible experiences with archival materials helped ground the readings and provided a foundation for students&rsquo; building of the press and paper-making. He also offers other educators planning courses with similar components advice for getting started.</em></p>   <p>Students in <em>21H.343J / CC.120J Making Books: The Renaissance and Today </em>read important scholarship in the history of the book from Gutenberg to the French Revolution, which they discussed with the instructors and each other during our seminar sessions. The purpose of these readings was to help students to think beyond technological determinism, or the idea that the &quot;invention&quot; of moveable type printing in the 1450s caused an immediate communications revolution.</p>   <div class=\"pullquote\" style=\"float: right; margin: -4px 0 auto 20px; padding-left: 20px; width: 240px; border-right: none; border-left: 1px solid #D5C9BA;\"><p class=\"quote\">Opportunities to interact with archival materials allowed students to closely study the printed objects they had read about in class. It gave them a tangible understanding of objects that book and print historians sometimes talk about in abstract ways.</p> <p class=\"sig\">&mdash; Jeffrey Ravel</p></div> <p>We also had three class sessions during which <a href=\"https://libraries.mit.edu/archives/research/rare-books.html\">the MIT Rare Books</a> Program Manager, <a href=\"http://libguides.mit.edu/profiles/skuce\">Steven Skuce</a>, shared with students manuscripts and early European print materials, Early Modern printed religious materials, and Early Modern natural philosophy printed materials. Another class session involved a visit to the <a href=\"https://councilofamericanmaritimemuseums.org/resources/ship-plans-directory/mit-museum-hart-nautical-collection/\">Hart Nautical Collection</a> at the MIT Museum, which provided students with an opportunity to explore visual material from the period between 1500 and 1800.</p> <p>These opportunities to interact with archival materials allowed students to closely study the printed objects they had read about in class. It gave them a tangible understanding of objects that book and print historians sometimes talk about in abstract ways. Because none of the students had extensive familiarity with the Early Modern materials, these tangible experiences also gave them a physical connection with the past places and periods we were discussing in class. Finally, the opportunity to discuss the relevant scholarship and to handle physical objects made with printing presses was invaluable background for the the process of actually designing and building their own handset printing press and making cotton rag paper.</p>", 
    "_content_type_license": "text/plain", 
    "_uid": "fd1f70add824664c21d52724e20f0f61", 
    "title": "Using Archival Experiences to Ground Readings and Hands-on Learning", 
    "_ac_local_roles": {
        "yunpeng": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "364e72058baf876deff7770f80369e00"
        ]
    }, 
    "constrainTypesMode": 0, 
    "_content_type_section_identifier": "text/plain", 
    "technical_location": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/instructor-insights/using-archival-experiences-to-ground-readings-and-hands-on-learning", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "364e72058baf876deff7770f80369e00", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2016/08/11 11:39:29.268 GMT-4", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2016/08/11 15:20:41.390 GMT-4", 
    "parent_module": "a1af3ab1ded1c20003d16b99541d7a6b", 
    "is_mathml_document": false, 
    "bottomtext": "", 
    "short_page_title": "Using Archival Experiences to Ground Learning", 
    "section_identifier": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/instructor-insights", 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "actor": "yunpeng", 
                "comments": "", 
                "time": "2016/08/11 11:39:29.277 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "comments": "", 
                "actor": "yunpeng", 
                "time": "2016/08/11 15:20:41.420 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "actor": "yunpeng", 
                "comments": "", 
                "time": "2016/08/12 10:43:26.829 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "yunpeng", 
                "comments": "", 
                "time": "2016/08/12 10:44:00.192 GMT-4"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "sehansen", 
                "time": "2016/08/15 16:49:50.766 GMT-4"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "sehansen", 
                "comments": "", 
                "time": "2016/08/15 17:03:40.392 GMT-4"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": true, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 4, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "a1af3ab1ded1c20003d16b99541d7a6b"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2016.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/instructor-insights/using-archival-experiences-to-ground-readings-and-hands-on-learning", 
    "is_image_gallery": false, 
    "id": "using-archival-experiences-to-ground-readings-and-hands-on-learning", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "In this section, Professor Jeffrey Ravel describes how offering students tangible experiences with archival materials helped ground the readings and provided a foundation for students\u2019 building of the press and paper-making. ", 
    "course_identifier": "https://ocw.mit.edu/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "yunpeng"
    ], 
    "modification_date": "2016/09/24 07:27:27.803 GMT-4", 
    "always_publish": false, 
    "_content_type_description": "text/plain", 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "Using Archival Experiences to Ground Readings and Hands-on Learning", 
            "string"
        ]
    ], 
    "_id": "using-archival-experiences-to-ground-readings-and-hands-on-learning", 
    "courselist_features": [], 
    "_path": "/Plone/courses/history/21h-343j-making-books-the-renaissance-and-today-spring-2016/instructor-insights/using-archival-experiences-to-ground-readings-and-hands-on-learning"
}