{
    "text": "<p class=\"sc_nav\">&laquo; <a href=\"./resolveuid/08db2ed910adc41a6bceda6c3bf65e1f\" class=\"sc_prev\">Previous</a> | <a href=\"./resolveuid/962d171611a101d49f865818d41217eb\" class=\"sc_next\">Next</a> &raquo;</p> <p>&nbsp;</p> <h2 class=\"subhead\">Introduction</h2> <p>Given a value &ndash; the price of gas, the pressure in a tank, or your distance from Boston &ndash; how can we describe changes in that value? Differentiation is a valuable technique for answering questions like this.</p> <p><a href=\"./resolveuid/962d171611a101d49f865818d41217eb\"><strong>Part A: Definition and Basic Rules</strong></a></p> <ul class=\"arrow\">     <li><a href=\"./resolveuid/c25a5592cbb582539daea82e3aeeda33\">Session 1: Introduction to Derivatives</a></li>     <li><a href=\"./resolveuid/29ef5f87bae9a94ad04425b8efabe1ac\">Session 2: Examples of Derivatives</a></li>     <li><a href=\"./resolveuid/4eff05d7dd9bd856611cfe4f4cddcb98\">Session 3: Derivative as Rate of Change</a></li>     <li><a href=\"./resolveuid/5878551f3d21546e5d3abcc5b156e7d2\">Session 4: Limits and Continuity</a></li>     <li><a href=\"./resolveuid/d0bc8735c3a450ca5aad8ed8059d679d\">Session 5: Discontinuity</a></li>     <li><a href=\"./resolveuid/a50336604c88798f90662c641ab35802\">Session 6: Calculating Derivatives</a></li>     <li><a href=\"./resolveuid/118fc2aa2f82b0483267f66bf76b6ba7\">Session 7: Derivatives of Sine and Cosine</a></li>     <li><a href=\"./resolveuid/efc851d816c458170838c60287c6388d\">Session 8: Limits of Sine and Cosine</a></li>     <li><a href=\"./resolveuid/d35aadf5c6d2e1543aee74e2dfdda970\">Session 9: Product Rule</a></li>     <li><a href=\"./resolveuid/94a0d0fdde324bb54b34d0ea12a6d94d\">Session 10: Quotient Rule</a></li>     <li><a href=\"./resolveuid/68feb14ba590b3b088c989717ae12492\">Session 11: Chain Rule</a></li>     <li><a href=\"./resolveuid/f0a4bb746fd734de38118b0ff7607e0b\">Session 12: Higher Derivatives</a></li>     <li><a href=\"./resolveuid/93a0806f20ed65401e0e178f23dae5d8\">Problem Set 1</a></li> </ul> <p><a href=\"./resolveuid/ea455a8ae7f0daad0b8d5033112f5f5e\"><strong>Part B: Implicit Differentiation and Inverse Functions</strong></a></p> <ul class=\"arrow\">     <li><a href=\"./resolveuid/796ea10c25d27cfaf1a12bb6262c8bff\">Session 13: Implicit Differentiation</a></li>     <li><a href=\"./resolveuid/331d135eb8244bc35069e7f2d86beb59\">Session 14: Examples of Implicit Differentiation</a></li>     <li><a href=\"./resolveuid/5668bfaf4a4849cdc9fb5962b4a18003\">Session 15: Implicit Differentiation and Inverse Functions</a></li>     <li><a href=\"./resolveuid/a08812a629e76732837d8a39d48fc8fa\">Session 16: The Derivative of a<sup>x</sup></a></li>     <li><a href=\"./resolveuid/625f9141b9dc5a9365acef497e19af09\">Session 17: The Exponential Function, its Derivative, and its Inverse</a></li>     <li><a href=\"./resolveuid/0cf2fa75aeb682dbf14c169c98cd1a15\">Session 18: Derivatives of other Exponential Functions</a></li>     <li><a href=\"./resolveuid/604424927fbe5ea476fd72662493105b\">Session 19: An Interesting Limit Involving <em>e</em></a></li>     <li><a href=\"./resolveuid/da268fb0fd08ed16c1b1988ed700633a\">Session 20: Hyperbolic Trig Functions</a></li>     <li><a href=\"./resolveuid/19961aa5fe0a0ea6be7d663fd98ff8f1\">Problem Set 2</a></li> </ul> <p><a href=\"./resolveuid/ffc4ad3d871527a0ef738ec4809f2038\"><strong>Exam 1</strong></a></p> <ul class=\"arrow\">     <li><a href=\"./resolveuid/0bc7f9cd2fd4599019832c45df40e073\">Session 21: Review for Exam 1 - Computing Derivatives Using Differentiation Rules</a></li>     <li><a href=\"./resolveuid/6d5198747ffe03bdab003afba5203c7d\">Session 22: Materials for Exam 1</a></li> </ul> <p>&nbsp;</p> <p class=\"sc_nav_bottom\">&laquo; <a href=\"./resolveuid/08db2ed910adc41a6bceda6c3bf65e1f\" class=\"sc_prev\">Previous</a> | <a href=\"./resolveuid/962d171611a101d49f865818d41217eb\" class=\"sc_next\">Next</a> &raquo;</p>", 
    "_content_type_license": "text/plain", 
    "_uid": "ff2c27eff0de39810c3e51a37548e568", 
    "title": "1. Differentiation", 
    "_ac_local_roles": {
        "cc_akuma1": [
            "Owner"
        ]
    }, 
    "location": "", 
    "_content_type_course_identifier": "text/plain", 
    "_atbrefs": {
        "isReferencing": [
            "08db2ed910adc41a6bceda6c3bf65e1f", 
            "962d171611a101d49f865818d41217eb"
        ]
    }, 
    "constrainTypesMode": 0, 
    "technical_location": "https://ocw.mit.edu/courses/mathematics/18-01sc-single-variable-calculus-fall-2010/1.-differentiation", 
    "software_requirements": [
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "40.0", 
            "software_name_col": "Chrome", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "8.0", 
            "software_name_col": "Safari", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "38.0", 
            "software_name_col": "Mozilla Firefox", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "9.0", 
            "software_name_col": "Internet Explorer", 
            "maximum_version_col": ""
        }, 
        {
            "software_type_col": "Browser", 
            "minimum_version_col": "13.1", 
            "software_name_col": "Edge", 
            "maximum_version_col": ""
        }
    ], 
    "_content_type_technical_location": "text/plain", 
    "_content_type_rights": "text/html", 
    "_directly_provided": [
        "Products.CMFEditions.interfaces.IVersioned"
    ], 
    "parent_uid": "bfe41979b9593362793fd930b36efa01", 
    "excludeFromNav": false, 
    "_content_type_unique_identifier": "text/plain", 
    "allowDiscussion": false, 
    "metadata_contributor_list": [], 
    "locallyAllowedTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "_content_type_text": "text/html", 
    "ad_type": "HOUSE", 
    "creation_date": "2012/11/07 23:53:02.533 US/Eastern", 
    "aggregationlevel": 3, 
    "duration": "", 
    "immediatelyAddableTypes": [
        "OCWImage", 
        "CourseSection", 
        "OCWExtFile", 
        "OCWFile", 
        "MediaResource"
    ], 
    "subject": [], 
    "_content_type_short_page_title": "text/plain", 
    "effectiveDate": "2012/11/09 03:03:12.448 US/Eastern", 
    "parent_module": "bfe41979b9593362793fd930b36efa01", 
    "is_mathml_document": false, 
    "bottomtext": "", 
    "short_page_title": "1. Differentiation", 
    "section_identifier": null, 
    "related_mit_links": "", 
    "_content_type_ad_type": "text/plain", 
    "_layout": "custom_view", 
    "_workflow_history": {
        "page_workflow": [
            {
                "action": null, 
                "review_state": "waiting_for_authoring", 
                "comments": "", 
                "actor": "cc_akuma1", 
                "time": "2012/11/07 23:53:02.543 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "srani7", 
                "comments": "", 
                "time": "2012/11/09 03:03:22.935 US/Eastern"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "actor": "asingh124", 
                "comments": "", 
                "time": "2012/11/23 04:53:15.598 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "asingh124", 
                "comments": "", 
                "time": "2012/11/23 05:31:46.352 US/Eastern"
            }, 
            {
                "action": "decline", 
                "review_state": "waiting_for_authoring", 
                "actor": "asingh124", 
                "comments": "", 
                "time": "2012/11/23 05:40:04.582 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "abohra", 
                "comments": "", 
                "time": "2012/11/23 06:23:56.948 US/Eastern"
            }, 
            {
                "action": "decline_to_qa", 
                "review_state": "waiting_for_qa", 
                "comments": "", 
                "actor": "Jcplayer", 
                "time": "2012/11/29 11:31:15.744 US/Eastern"
            }, 
            {
                "action": "approve_to_staging", 
                "review_state": "ready_for_staging", 
                "actor": "Jcplayer", 
                "comments": "", 
                "time": "2012/11/29 11:57:17.491 US/Eastern"
            }
        ]
    }, 
    "_classname": "CourseSection", 
    "_content_type_materialtype": "text/plain", 
    "_userdefined_roles": [], 
    "publisher": "MIT OpenCourseWare", 
    "nextPreviousEnabled": false, 
    "language": "", 
    "url_rewrite_patterns": [], 
    "widgets_features": [], 
    "category_features": [], 
    "firstTimeEdit": true, 
    "feature_requirements": [], 
    "list_in_left_nav": false, 
    "short_name_tlp": "", 
    "expirationDate": "None", 
    "_content_type_id": "text/plain", 
    "contributor_list": [
        {}
    ], 
    "is_media_gallery": false, 
    "contributors": [], 
    "_gopip": 2, 
    "_content_type_title": "text/plain", 
    "course_linking": [], 
    "_atrefs": {
        "BelongsTo": [
            "bfe41979b9593362793fd930b36efa01"
        ], 
        "isReferencing": [
            "604424927fbe5ea476fd72662493105b", 
            "d0bc8735c3a450ca5aad8ed8059d679d", 
            "efc851d816c458170838c60287c6388d", 
            "94a0d0fdde324bb54b34d0ea12a6d94d", 
            "625f9141b9dc5a9365acef497e19af09", 
            "4eff05d7dd9bd856611cfe4f4cddcb98", 
            "796ea10c25d27cfaf1a12bb6262c8bff", 
            "d35aadf5c6d2e1543aee74e2dfdda970", 
            "5878551f3d21546e5d3abcc5b156e7d2", 
            "6d5198747ffe03bdab003afba5203c7d", 
            "ea455a8ae7f0daad0b8d5033112f5f5e", 
            "c25a5592cbb582539daea82e3aeeda33", 
            "29ef5f87bae9a94ad04425b8efabe1ac", 
            "68feb14ba590b3b088c989717ae12492", 
            "a08812a629e76732837d8a39d48fc8fa", 
            "331d135eb8244bc35069e7f2d86beb59", 
            "0cf2fa75aeb682dbf14c169c98cd1a15", 
            "19961aa5fe0a0ea6be7d663fd98ff8f1", 
            "962d171611a101d49f865818d41217eb", 
            "5668bfaf4a4849cdc9fb5962b4a18003", 
            "ffc4ad3d871527a0ef738ec4809f2038", 
            "93a0806f20ed65401e0e178f23dae5d8", 
            "0bc7f9cd2fd4599019832c45df40e073", 
            "da268fb0fd08ed16c1b1988ed700633a", 
            "a50336604c88798f90662c641ab35802", 
            "f0a4bb746fd734de38118b0ff7607e0b", 
            "118fc2aa2f82b0483267f66bf76b6ba7", 
            "08db2ed910adc41a6bceda6c3bf65e1f"
        ]
    }, 
    "other_platform_requirements": "", 
    "license": "http://creativecommons.org/licenses/by-nc-sa/3.0/us/", 
    "rights": "This site (c) Massachusetts Institute of Technology 2012.  Content within individual courses is (c) by the individual authors unless otherwise noted. The Massachusetts Institute of Technology is providing this Work (as defined below) under the terms of this Creative Commons public license (\"CCPL\" or \"license\") unless otherwise noted. The Work is protected by copyright and/or other applicable law. Any use of the work other than as authorized under this license is prohibited. By exercising any of the rights to the Work provided here, You (as defined below) accept and agree to be bound by the terms of this license. The Licensor, the Massachusetts Institute of Technology, grants You the rights contained here in consideration of Your acceptance of such terms and conditions.", 
    "hide_Page_title": false, 
    "_defaultpage": "", 
    "tcATmit_subject_tags": [], 
    "features_tracking": [], 
    "ad_champion_text": "", 
    "has_inline_embed": false, 
    "unique_identifier": "https://ocw.mit.edu/courses/mathematics/18-01sc-single-variable-calculus-fall-2010/1.-differentiation", 
    "is_image_gallery": false, 
    "id": "1.-differentiation", 
    "materialtype": "Other", 
    "_type": "CourseSection", 
    "description": "This section includes the unit on differentiation, one of the five major units of the course.", 
    "course_identifier": "https://ocw.mit.edu/courses/mathematics/18-01sc-single-variable-calculus-fall-2010", 
    "_content_type_publisher": "text/plain", 
    "tcATmit_instructional_tags": [], 
    "_content_type": "text/html", 
    "creators": [
        "cc_akuma1"
    ], 
    "modification_date": "2016/09/24 07:48:45.831 GMT-4", 
    "always_publish": false, 
    "_content_type_description": "text/plain", 
    "_owner": null, 
    "_permissions": {
        "Change portal events": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "Modify portal content": {
            "acquire": false, 
            "roles": [
                "Manager", 
                "MetadataEditor"
            ]
        }, 
        "Access contents information": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }, 
        "List folder contents": {
            "acquire": false, 
            "roles": [
                "Editor", 
                "Manager", 
                "MetadataEditor", 
                "Owner"
            ]
        }, 
        "View": {
            "acquire": false, 
            "roles": [
                "Authenticated"
            ]
        }
    }, 
    "_properties": [
        [
            "title", 
            "1. Differentiation", 
            "string"
        ]
    ], 
    "_id": "1.-differentiation", 
    "courselist_features": [], 
    "_path": "/Plone/courses/mathematics/18-01sc-single-variable-calculus-fall-2010/1.-differentiation"
}