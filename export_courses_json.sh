# Fetch all export URLs so that content gets exported
wget --no-check-certificate --directory-prefix=output -i /prod/OCWEngines/export_urls.txt

# Remove output directory as it is used only to run above command
rm -rf output

# Empty the file
echo -n "" > /prod/OCWEngines/export_urls.txt

#Sync /export directory with S3 bucket
cd /export
aws s3 sync . s3://ocw-content-storage/PROD/